//
//  Post.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 12/26/16.
//  Copyright © 2016 The Zero2Launch Team. All rights reserved.
//

import Foundation

class Post {
    var caption: String?
    var photoUrl: String?
    var uid: String?
    var id: String?
    var likeCount: Int?
    var likes: Dictionary<String, Any>?
    var isLiked: Bool?

    var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
}


