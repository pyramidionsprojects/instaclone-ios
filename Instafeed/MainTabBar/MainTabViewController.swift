//
//  MainTabViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 27/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController,UITabBarControllerDelegate {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    var moveIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("changeTabbarItem"), object: nil)


        self.selectedIndex = moveIndex


    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification){
        
//        self.tabBar
        
//        self.tabBarController?.selectedIndex = 4

        self.selectedIndex = 4
        

        
        
//        tabBarController?.selectedViewController = tabBarController?.viewControllers![3]

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        print("Selected item = ",tabBar.items?.index(of: item))
        
        
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
        
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex != 2
        {
            appDelegate.galleryReturn = 0
        }

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
