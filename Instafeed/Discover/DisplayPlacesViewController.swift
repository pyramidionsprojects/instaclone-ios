//
//  DiscoverViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 01/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import CRRefresh
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage

class DisplayPlacesViewController: UIViewController,UIViewControllerPreviewingDelegate
{
    
    @IBOutlet weak var proLabel: UILabel!
    
    var city : String = ""
    var area : String = ""

    var lat : String = "0.0"
    var log : String = "0.0"

    var proPic : String = ""

    
    
    @IBOutlet weak var discoverCollectView: UICollectionView!

    var Total_Page_Count : Int = 0
    var Current_page : Int = 1

    var feedPosts = Array<Any>()
    
    var forceTouchAvailable = false
    
    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(DisplayPlacesViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            
            return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.discoverCollectView.addSubview(self.refreshControl)
        
        
        self.proLabel.text = self.city + ", " + self.area
        
        
        self.loadProfile()

        /*
        
        tableView.cr.addFootRefresh(animator: NormalFootAnimator()) { [weak self] in
            /// start refresh
            /// Do anything you want...
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                /// If common end
                self?.tableView.cr.endLoadingMore()
                /// If no more data
                self?.tableView.cr.noticeNoMoreData()
                /// Reset no more data
                self?.tableView.cr.resetNoMore()
            })
        }

*/
        // Do any additional setup after loading the view.
/*
        discoverCollectView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            /// start refresh
            /// Do anything you want...
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                /// Stop refresh when your job finished, it will reset refresh footer if completion is true
                self?.discoverCollectView.cr.endHeaderRefresh()
            })
        }
        /// manual refresh
        discoverCollectView.cr.beginHeaderRefresh()
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    
    
 
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        
        //        refreshControl.endRefreshing()
        
        
        
//        refreshControl.endRefreshing()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
            {
                self.pullToRefresh()
        })
        
        
//        pullToRefresh()
        
        
    }
    
    
    
    func pullToRefresh()
    {
        Current_page = 1
        
        
        self.feedPosts.removeAll()

        
        //        KRProgressHUD.show(withMessage: "Loading...")
        
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "area":area,
            "city":city,
            "page":Current_page
        ]
        
        print(params)
        let url = BASE_URL + PLACESPOST
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                self.refreshControl.endRefreshing()
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            if(jsonResponse["success"].stringValue == "false" )
                            {
                                
                                self.feedPosts.removeAll()

                                self.discoverCollectView.reloadData()

                            }
                            else
                            {
                                
                                self.lat = jsonResponse["lat"].stringValue
                                self.log = jsonResponse["lon"].stringValue
                                
                                
                                let JSON = response.result.value as! NSDictionary
                                
                                self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any>
                                
                                
                                if self.feedPosts.count > 0
                                {
                                    let feedPost = self.feedPosts[0]as! NSDictionary
                                    
                                    
                                    self.proPic = feedPost["profilePic"] as! String ?? ""
                                    
                                }
                                
                                
                                
                                self.Total_Page_Count =  jsonResponse["pageCount"].intValue
                                
                                
                                self.discoverCollectView.reloadData()
                            }
                            
                        }
                        else
                        {
                            self.feedPosts.removeAll()
                            self.discoverCollectView.reloadData()

                        }
             /*           else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }*/
                    }
                }
                else
                {
                    
                    self.feedPosts.removeAll()
                    self.discoverCollectView.reloadData()

                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                }
        }
    }
    
    
    func loadProfile()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "area":area,
                "city":city,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + PLACESPOST
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                if(jsonResponse["success"].stringValue == "false" )
                                {
                                    
                                }
                                else
                                {
                                    
                                    self.lat = jsonResponse["lat"].stringValue
                                    self.log = jsonResponse["lon"].stringValue

                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any>
                                    
                                    
                                    if self.feedPosts.count > 0
                                    {
                                        let feedPost = self.feedPosts[0]as! NSDictionary

                                        
                                        self.proPic = feedPost["profilePic"] as! String ?? ""
                                        
                                    }
                                    
                                    
                                    
                                    self.Total_Page_Count =  jsonResponse["pageCount"].intValue

                                    
                                    self.discoverCollectView.reloadData()
                                }
                                
                            }
                            else
                            {
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
            }
            
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func UPDATE_FEED_API()
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            KRProgressHUD.show(withMessage: "Loading...")

            
            let params: Parameters = [
                "accessToken": accessToken!,
                "area":area,
                "city":city,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + PLACESPOST
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                
                                if(jsonResponse["success"].stringValue == "false" )
                                {
                                    
                                }
                                else
                                {
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    let newFeed = JSON.object(forKey: "NewsPosts")as! Array<Any>
                                    
                                    
                                    if newFeed.count > 0
                                    {
                                        for var i in 0..<newFeed.count
                                        {
                                            let dict = newFeed[i] as! NSDictionary
                                            self.feedPosts.append(dict)
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                self.feedPosts.removeAll()
                            }
                            
                            self.discoverCollectView.reloadData()
                        }
                        else
                        {
                            self.feedPosts.removeAll()
                            self.discoverCollectView.reloadData()

                        }
                    }
            }
        }
        else
        {
            self.feedPosts.removeAll()
            self.discoverCollectView.reloadData()
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")

        }
    }
    
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard #available(iOS 9.0, *) else {
            return nil
        }
        
        //        let indexPath = collectionview.indexPathForItem(at: collectionview.convert(location, from:view))
        //        if let indexPath = indexPath {
        //            let imageName = img[(indexPath as NSIndexPath).item]
        //            if let cell = collectionview.cellForItem(at: indexPath) {
        //                previewingContext.sourceRect = cell.frame
        //
        //                //                let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //                //                controller.imagenames = imageName
        //                //                return controller
        //            }
        //        }
        
        return nil
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        //        let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //        controller.imagenames = (viewControllerToCommit as! DetailsVC).imagenames
        //
        //        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension DisplayPlacesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.feedPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCollectionViewCell", for: indexPath) as! DiscoverCollectionViewCell
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        var feedContentPosts = Array<Any>()
        
        feedContentPosts = (feedPosts["linkData"]as? Array)!
        
        
        print("feedContentPosts = ",feedContentPosts)
        
        var linkData = NSDictionary()
        
        if feedContentPosts.count > 0
        {
            
            if feedContentPosts.count > 1
            {
//                cell.multipleCount.isHidden = false
            }
            else
            {
//                cell.multipleCount.isHidden = true
            }
            
            linkData = feedContentPosts[0]as! NSDictionary
            
        }
        else
        {
//            cell.multipleCount.isHidden = true
        }
        
        
        print("linkData = ",linkData)
        
        
        let type = linkData["type"]as? String ?? ""
        
        
        print("Type = ",type)
        
        
        if type == "image"
        {
            let linkData = linkData["linkData"]as? String ?? ""
            
            
            cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
            
        }
        else if type == "video"
        {
            let linkData = linkData["thumbnail"]as? String ?? ""
            
            
            cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
        }
        else
        {
            let linkData = linkData["linkData"]as? String ?? ""
            
            cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
        }
        
        
        
        
        
        
        
        
        
//        let imgString = feedPosts["linkData"]as? String ?? ""
        
//        cell.imgView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-thumbnail"))
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        var reusableView : UICollectionReusableView? = nil
        
        // Create header
        if (kind == UICollectionElementKindSectionHeader)
        {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DisplayPlacesCollectionReusableView", for: indexPath) as! DisplayPlacesCollectionReusableView

            
            
            headerView.proName.text = self.city + ", " + self.area

            headerView.profileImg.sd_setImage(with: URL(string: proPic), placeholderImage: UIImage(named: "default-thumbnail"))
        
            
            let mapImgs = "http://maps.google.com/maps/api/staticmap?center=" + lat + "," + log + "&markers=size:large|color:red|" + lat + "," +  log + "&zoom=12&size=400x400&sensor=false";
            
            headerView.mapImg.sd_setImage(with: URL(string: mapImgs), placeholderImage: UIImage(named: "staticmap"))
            
            print("Map = ",mapImgs)
            
            
            headerView.openMap.addTarget(self, action: #selector(DisplayPlacesViewController.openMap(sender:)), for: .touchUpInside)
            
            headerView.openMap.tag = indexPath.row

            
            reusableView = headerView
        }
        
        return reusableView!
        
    }
    
    
    @objc func openMap(sender: UIButton)
    {
//        let url = "http://maps.apple.com/maps?saddr=\(lat),\(log)"
//        UIApplication.shared.openURL(URL(string:url)!)
        
        
        openGoogleMaps()

    }
    
    
    func openGoogleMaps() {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            
            UIApplication.shared.open(URL(string:
                "comgooglemaps://?center=\(lat),\(log)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
            
//            UIApplication.shared.openURL(URL(string:
//                "comgooglemaps://?center=\(lat),\(log)&zoom=14&views=traffic")!)
            
            
        } else {
            print("Can't use comgooglemaps://")
        }
    }
    
    

    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
//        cell.alpha = 0
//        UIView.animate(withDuration: 0.8) {
//            cell.alpha = 1
//        }
        
        
        if collectionView == discoverCollectView
        {
            if forceTouchAvailable == false
            {
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(_:)))
                gesture.minimumPressDuration = 0.5
                cell.addGestureRecognizer(gesture)
            }
        }
        
        
        print("self.feedPosts.count = ",self.feedPosts.count)
        
        print("Total_Page_Count = ",Total_Page_Count)
        
        print("Current_page = ",Current_page)

        
        let lastcell = self.feedPosts.count - 1
        
        if indexPath.row == lastcell
        {
            if Total_Page_Count > Current_page
            {
                Current_page = Current_page + 1

                print("Current Page = ",Current_page)
                
                UPDATE_FEED_API()
            }
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
        
        let like = feedPosts["id"] as! Int
        
        vc.detailPostId = String(describing: like)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func commentBtn(row :Int)
    {
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.feedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String
        
        vc.proPic = profilePic
        
        //        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func longPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = discoverCollectView.indexPath(for: cell)
        {
            
            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
            
            print("FeedPosts = %@",feedPosts);
            
            
            
            var feedContentPosts = Array<Any>()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            
            print("feedContentPosts = ",feedContentPosts)
            
            var linkData = NSDictionary()
            
            if feedContentPosts.count > 0
            {
                
                if feedContentPosts.count > 1
                {
                    //                cell.multipleCount.isHidden = false
                }
                else
                {
                    //                cell.multipleCount.isHidden = true
                }
                
                linkData = feedContentPosts[0]as! NSDictionary
                
            }
            else
            {
                //            cell.multipleCount.isHidden = true
            }
            
            
            print("linkData = ",linkData)
            
            
            let type = linkData["type"]as? String ?? ""
            
            
            print("Type = ",type)
            
            
            var imageName : String = ""

            
            
            if type == "image"
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                
                imageName = linkData
//                cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
                
            }
            else if type == "video"
            {
                let linkData = linkData["thumbnail"]as? String ?? ""
                
  
                imageName = linkData

//                cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            else
            {
                let linkData = linkData["linkData"]as? String ?? ""
  
                imageName = linkData

//                cell.imgView.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
/*            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!*/
            
            
            /*
            if feedContentPosts.count > 1
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    imageName = linkData
                    
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    
                    imageName = linkData
                    
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }
            */
            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            
            
//            imageName = feedPosts["linkData"] as? String ?? ""

            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            
            // you can set different frame for each peek view here
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            
            
            if feedPosts.object(forKey: "user_liked") as! String == "true"
            {
                
                let options = [
                    PeekViewAction(title: "Un Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
            }
            else
            {
                
                let options = [
                    PeekViewAction(title: "Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                        
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
                
                
            }
            
            
            
        }
    }
    
    
    func likeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.feedPosts[row] as! NSDictionary)
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
//                let like = (dict["total_likes"] as! Int - 1)
                
//                dict["total_likes"] = like
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                
//                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            
            self.feedPosts[row] = dict
            
        }
    }
    
    
    func postLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
}

extension DisplayPlacesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3 - 1, height: collectionView.frame.size.width / 3 - 1)
    }
    
    
}

