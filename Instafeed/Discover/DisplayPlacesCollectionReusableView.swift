//
//  DisplayPlacesCollectionReusableView.swift
//  Instafeed
//
//  Created by Pyramidions on 24/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class DisplayPlacesCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var mapImg: UIImageView!
    
    @IBOutlet weak var openMap: UIButton!
    
    @IBOutlet weak var proName: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2
        
        self.profileImg.layer.masksToBounds = true
        
    }
    
}
