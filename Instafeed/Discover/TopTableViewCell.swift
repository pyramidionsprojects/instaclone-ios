//
//  TopTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 03/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class TopTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var proImgView: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    

    override func awakeFromNib()
    {
        //        self.layoutIfNeeded()
        self.proImgView.layer.cornerRadius = self.proImgView.frame.size.width / 2
        self.proImgView.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
