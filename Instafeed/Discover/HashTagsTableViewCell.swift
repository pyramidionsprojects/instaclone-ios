//
//  HashTagsTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 08/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class HashTagsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var hashTagView: UIView!
    
    @IBOutlet weak var hashTagLabel: UILabel!
    @IBOutlet weak var hashCount: UILabel!
    
    
        
    @IBOutlet weak var mentionView: UIView!
    
    @IBOutlet weak var mentionImg: UIImageView!
    
    @IBOutlet weak var mentionNameLbl: UILabel!
    
    @IBOutlet weak var mentionUserNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mentionImg.layer.cornerRadius = self.mentionImg.frame.width / 2
        
        mentionImg.layer.masksToBounds = true
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
