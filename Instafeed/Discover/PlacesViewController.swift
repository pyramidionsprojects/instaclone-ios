//
//  PlacesViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 03/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage

class PlacesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var tableView: UITableView!
    
    var places : String = ""
    
    var placeArray = Array<Any>()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)


    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        
        
    }

    
    @objc func showSpinningWheel(_ notification: NSNotification)
    {
        if let place = notification.userInfo?["text"] as? String
        {
            places = place
            
            print("Places = ",place)
            
            searchHashTags()
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return placeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesTableViewCell", for: indexPath) as! PlacesTableViewCell
        
        let feedPosts = self.placeArray[indexPath.row]as! NSDictionary
        
        cell.areaLbl.text = feedPosts["area"]as? String ?? ""
        cell.cityLbl.text = feedPosts["city"]as? String ?? ""
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let feedPosts = self.placeArray[indexPath.row]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "DisplayPlacesViewController")as! DisplayPlacesViewController
        vc.area = feedPosts["area"]as? String ?? ""
        vc.city = feedPosts["city"]as? String ?? ""

        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func searchHashTags()
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let params: Parameters = [
                "address":places
            ]
            
            print(params)
            let url = BASE_URL + SEARCHPLACES
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                self.placeArray = JSON.object(forKey: "places")as! Array<Any>
                                
                                print("Response = ",JSON)
                                
                                self.tableView.reloadData()
                                
                            }
                            else
                            {
                                self.placeArray.removeAll()
                                
                                self.tableView.reloadData()

                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                    }
            }
        }
        else{
        }
    }

}
