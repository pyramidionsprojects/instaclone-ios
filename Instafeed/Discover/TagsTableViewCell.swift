//
//  TagsTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 03/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class TagsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var hashLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    
    
    
    @IBOutlet weak var proImgView: UIImageView!
    
    
    override func awakeFromNib()
    {
        //        self.layoutIfNeeded()
        self.proImgView.layer.cornerRadius = self.proImgView.frame.size.width / 2
        self.proImgView.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
