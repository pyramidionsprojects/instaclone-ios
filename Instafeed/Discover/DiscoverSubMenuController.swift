//
//  DiscoverSubMenuController.swift
//  Instafeed
//
//  Created by Pyramidions on 01/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class DiscoverSubMenuController: UIViewController, CAPSPageMenuDelegate,UITextFieldDelegate {

    
    @IBOutlet weak var searchTxt: UITextField!
    
    
    @IBOutlet weak var topView: UIView!
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        searchTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        
        
        searchTxt.delegate = self
        
        let stoaryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = stoaryboard.instantiateViewController(withIdentifier: "TopViewController")as! TopViewController
        Dvc.title = "Top"
        controllerArray.append(Dvc)
        
        let pvc = stoaryboard.instantiateViewController(withIdentifier: "PeopleViewController")as! PeopleViewController
        pvc.title = "People"
        controllerArray.append(pvc)
        
        let Controller3 = stoaryboard.instantiateViewController(withIdentifier: "TagsViewController")as! TagsViewController
        Controller3.title = "Tags"
        controllerArray.append(Controller3)
        
        let Controller4 = stoaryboard.instantiateViewController(withIdentifier: "PlacesViewController")as! PlacesViewController
        Controller4.title = "Places"
        controllerArray.append(Controller4)
        
        print(controllerArray)
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        let width = screenSize.width / 4
        
        let parameters: [CAPSPageMenuOption] = [
            //            .scrollMenuBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            //            .viewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            //            .selectionIndicatorColor(UIColor.orange),
            //            .addBottomMenuHairline(false),
            //            .menuItemFont(UIFont(name: "HelveticaNeue", size: 10.0)!),
            //            .menuHeight(50.0),
            ////            .selectionIndicatorHeight(0.0),
            //            .menuItemWidthBasedOnTitleTextWidth(true),
            //            .selectedMenuItemLabelColor(UIColor.orange),
            .menuItemWidth(width),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 15.0)!)
            
        ]
        
        
        let setrame =  topView.bounds
        
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: setrame, pageMenuOptions: parameters)
        
        
        
        
        pageMenu?.delegate = self
        
        self.addChildViewController(pageMenu!)
        self.topView.addSubview(pageMenu!.view)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        

    }
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {        
            let imageDataDict:[String: String] = ["text": textField.text!]
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: imageDataDict)
    }
}
