//
//  PeopleViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 03/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage


class PeopleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    
    @IBOutlet weak var tableView: UITableView!
    var places : String = ""
    
    var placeArray = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func showSpinningWheel(_ notification: NSNotification)
    {
        if let place = notification.userInfo?["text"] as? String
        {
            places = place
            print("Places = ",place)
            searchPeople()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.placeArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleTableViewCell", for: indexPath) as! PeopleTableViewCell
        
        let feedPosts = self.placeArray[indexPath.row]as! NSDictionary
        
        let imgString = feedPosts["profilePic"]as? String ?? ""
        
        cell.proImgView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
        
        cell.selectionStyle = .none

        cell.nameLbl.text = feedPosts["name"]as? String ?? ""

        cell.userNameLbl.text = feedPosts["username"]as? String ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let feedPosts = self.placeArray[indexPath.row]as! NSDictionary

        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = feedPosts["username"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func searchPeople()
    {
        if Reachability.isConnectedToNetwork()
        {
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "name":places
            ]
            
            print(params)
            let url = BASE_URL + SEARCHUSER
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                self.placeArray = JSON.object(forKey: "users")as! Array<Any>
                                
                                print("Response = ",JSON)
                                
                                self.tableView.reloadData()
                                
                            }
                            else
                            {
                                self.placeArray.removeAll()
                                
                                self.tableView.reloadData()
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                    }
            }
        }
        else{
        }
    }


}
