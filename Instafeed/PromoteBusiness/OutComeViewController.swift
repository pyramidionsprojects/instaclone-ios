//
//  OutComeViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 18/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class OutComeViewController: UIViewController
{

    @IBOutlet weak var goalLabel: UILabel!
    
    @IBOutlet weak var websiteURL: UILabel!
    
    @IBOutlet weak var statusImg: UIImageView!
    
    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var secondImg: UIImageView!
    @IBOutlet weak var thirdImg: UIImageView!
    
    @IBOutlet weak var statusView: UIView!
    
    var sharedInstance = BusinessSharedManager.sharedInstance
    
    
    let profileVisit = "Select profile visits if you want to grow your presence on Snipofeed. We'll deliver your promotion to a medium-sized group of people who are likely to visit your profile."
    
    let webTraffic = "This outcome sends people from Snipofeed to your website. We'll deliver your post to a focused group of people who are likely to interact with your promotion."


    
    var flag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        statusView.isHidden = true
        
        sharedInstance.goalType = "1"
        
        
        self.goalLabel.text = profileVisit
        
        flag = true
        
        firstImg.image = #imageLiteral(resourceName: "fill_circle")
        secondImg.image = #imageLiteral(resourceName: "empty_circle")
        thirdImg.image = #imageLiteral(resourceName: "empty_circle")
        
        statusImg.image = #imageLiteral(resourceName: "Group 462")
        statusView.isHidden = false
        
        
        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String
        
        flag = true
        
        if proStatus != nil
        {
            self.profileName.text = "@ " + proStatus
        }
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.websiteURL.text = sharedInstance.websiteUrl

        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        statusView.layer.shadowColor = UIColor.lightGray.cgColor
        statusView.layer.shadowOpacity = 0.5
        statusView.layer.shadowOffset = CGSize.zero
        statusView.layer.shadowRadius = 2
        
    }
    
    
    @IBAction func profileVisitsBtn(_ sender: Any)
    {
        
        sharedInstance.goalType = "1"
        
        
        self.goalLabel.text = profileVisit


        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String
        
        flag = true
        
        if proStatus != nil
        {
            self.profileName.text = "@ " + proStatus
        }
        
        firstImg.image = #imageLiteral(resourceName: "fill_circle")
        secondImg.image = #imageLiteral(resourceName: "empty_circle")
        thirdImg.image = #imageLiteral(resourceName: "empty_circle")
        
        statusImg.image = #imageLiteral(resourceName: "Group 462")
        statusView.isHidden = false
    }
    
    
    @IBAction func webTrafficBtn(_ sender: Any)
    {
     
        
        sharedInstance.goalType = "2"
        
        
        self.goalLabel.text = webTraffic


        
        flag = true

//        self.profileName.text = ""

        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        secondImg.image = #imageLiteral(resourceName: "fill_circle")
        thirdImg.image = #imageLiteral(resourceName: "empty_circle")
        
        statusImg.image = #imageLiteral(resourceName: "Group 463")

        statusView.isHidden = false
    }
    
    
    @IBAction func promotionViews(_ sender: Any)
    {
        
        flag = true

        self.profileName.text = ""

        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        secondImg.image = #imageLiteral(resourceName: "empty_circle")
        thirdImg.image = #imageLiteral(resourceName: "fill_circle")

        statusImg.image = #imageLiteral(resourceName: "Group 464")

        statusView.isHidden = false
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if flag
        {
            
            self.websiteURL.text = sharedInstance.websiteUrl
            
            if sharedInstance.goalType == "2"
            {
                if sharedInstance.websiteUrl != ""
                {
                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let vc = StoaryBoard.instantiateViewController(withIdentifier: "AudienceViewController")as! AudienceViewController
                    
                    self.navigationController?.pushViewController(vc, animated: true)

                }
                else
                {
                    let alertController = UIAlertController(title: "Oops", message: "Enter your website", preferredStyle: .alert)
                    
                    let action1 = UIAlertAction(title: "Ok", style: .default)
                    {
                        (action:UIAlertAction) in
                    }
                    
                    alertController.addAction(action1)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "AudienceViewController")as! AudienceViewController
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Select your goal type", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func editBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "AddURLViewController")as! AddURLViewController
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
}
