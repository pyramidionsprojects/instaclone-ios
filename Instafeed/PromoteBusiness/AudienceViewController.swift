//
//  OutComeViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 18/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class AudienceViewController: UIViewController
{

    @IBOutlet weak var audienceLabel: UILabel!
    
    @IBOutlet weak var statusImg: UIImageView!
    
//    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var secondImg: UIImageView!
    
    //    @IBOutlet weak var secondImg: UIImageView!
//    @IBOutlet weak var thirdImg: UIImageView!
    
    @IBOutlet weak var statusView: UIView!
    
    var sharedInstance = BusinessSharedManager.sharedInstance
    
    var flag = false
    
    
    let autoAudience = "Automatic audience targeting targets people similar to your followers. We also look for other people who have engaged with your organic content and may be interested in more of your posts."
    
    let localAudience = "Our local audience targets people based on their location. We'll show your promotion to people who are similar to your followers and within a certain distance of your business."
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.audienceLabel.text = autoAudience
        
        
        firstImg.image = #imageLiteral(resourceName: "fill_circle")
        secondImg.image = #imageLiteral(resourceName: "greater_1")
        
        sharedInstance.audienceType = "1"
        
        statusView.isHidden = false
        
        flag = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if sharedInstance.currentLocation == ""
        {
            firstImg.image = #imageLiteral(resourceName: "fill_circle")
            secondImg.image = #imageLiteral(resourceName: "greater_1")
            statusView.isHidden = false
            
            sharedInstance.audienceType = "1"
            
            self.audienceLabel.text = autoAudience

        }
        else
        {
            firstImg.image = #imageLiteral(resourceName: "empty_circle")
            secondImg.image = #imageLiteral(resourceName: "fill_circle")
            statusView.isHidden = false
            
            sharedInstance.audienceType = "2"
            
            self.audienceLabel.text = localAudience

        }
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        statusView.layer.shadowColor = UIColor.lightGray.cgColor
        statusView.layer.shadowOpacity = 0.5
        statusView.layer.shadowOffset = CGSize.zero
        statusView.layer.shadowRadius = 2
    }
    
    @IBAction func AutomaticBtn(_ sender: Any)
    {
        
        sharedInstance.audienceType = "1"
        
        self.audienceLabel.text = autoAudience

        
        firstImg.image = #imageLiteral(resourceName: "fill_circle")
        secondImg.image = #imageLiteral(resourceName: "greater_1")

        //        secondImg.image = #imageLiteral(resourceName: "empty_circle")
        //        thirdImg.image = #imageLiteral(resourceName: "empty_circle")
        //
        //        statusImg.image = #imageLiteral(resourceName: "Group 462")
        
        statusView.isHidden = false
    }
    
    
    @IBAction func localBtn(_ sender: Any)
    {
        
        sharedInstance.audienceType = "2"

        flag = true
        
        self.audienceLabel.text = localAudience

        
        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        secondImg.image = #imageLiteral(resourceName: "fill_circle")
        statusView.isHidden = false
        
        sharedInstance.audienceType = "1"
        
        
        //        self.profileName.text = ""
        //
        //        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        //        secondImg.image = #imageLiteral(resourceName: "fill_circle")
        //        thirdImg.image = #imageLiteral(resourceName: "empty_circle")
        //
        //        statusImg.image = #imageLiteral(resourceName: "Group 463")
        
        statusView.isHidden = false
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "AudienceAddViewController")as! AudienceAddViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func manualBtn(_ sender: Any)
    {
        flag = true
        
        //        self.profileName.text = ""
        //
        //        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        //        secondImg.image = #imageLiteral(resourceName: "empty_circle")
        //        thirdImg.image = #imageLiteral(resourceName: "fill_circle")
        //
        //        statusImg.image = #imageLiteral(resourceName: "Group 464")
        
        statusView.isHidden = false
    }
    

    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if flag
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "BudgetDurationViewController")as! BudgetDurationViewController
            
            self.navigationController?.pushViewController(vc, animated: true)

        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Select your Audience type", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
