//
//  OutComeViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 18/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class BudgetDurationViewController: UIViewController
{
    
    var flag = true
    
    @IBOutlet weak var spendLabel: UILabel!
    @IBOutlet weak var estProfileLabel: UILabel!
    
    var productValue = 0
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    
    @IBOutlet weak var proVisitLabel: UILabel!
    
    @IBOutlet weak var proReachLabel: UILabel!
    
    
    @IBOutlet weak var firstReachLabel: UILabel!
    @IBOutlet weak var firstAmountLable: UILabel!
    
    @IBOutlet weak var secondReachLabel: UILabel!
    @IBOutlet weak var secondAmountLabel: UILabel!
    
    @IBOutlet weak var thirdReachLabel: UILabel!
    @IBOutlet weak var thirdAmountLabel: UILabel!
    
    @IBOutlet weak var fourthReachLabel: UILabel!
    @IBOutlet weak var fourthAmountLabel: UILabel!
    
    
    @IBOutlet weak var firstImg: UIImageView!
    
    @IBOutlet weak var secondImg: UIImageView!
    
    @IBOutlet weak var thirdImg: UIImageView!
    
    @IBOutlet weak var fourthImg: UIImageView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sharedInstance.amount = "20"
        
        self.spendLabel.text = "Your Total Spend \nis $20 over 2 days"
        self.estProfileLabel.text = "13 - 25"
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
    
    }
    
   
    
   

    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextContinueBtn(_ sender: Any)
    {
        if flag
        {
            
//            if sharedInstance.paymentSuccess != ""
//            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "ReviewViewController")as! ReviewViewController
                
                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            else
//            {
//                IAPHandler.shared.purchaseMyProduct(index: productValue)
//            }


        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Select your plan type", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if flag
        {
            
//            if sharedInstance.paymentSuccess != ""
//            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "ReviewViewController")as! ReviewViewController
                
                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            else
//            {
//                IAPHandler.shared.purchaseMyProduct(index: productValue)
//            }
            
        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Select your plan type", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func firstBtn(_ sender: Any)
    {
        
        self.spendLabel.text = "Your Total Spend \nis $20 over 2 days"
        self.estProfileLabel.text = "13 - 25"

        
        
        productValue = 0
        
        sharedInstance.typeOfPayment = 0
        
        flag = true
        
        self.firstImg.image = #imageLiteral(resourceName: "planSelect")
        
        self.secondImg.image = #imageLiteral(resourceName: "planDeselect")

        self.thirdImg.image = #imageLiteral(resourceName: "planDeselect")

        self.fourthImg.image = #imageLiteral(resourceName: "planDeselect")

        
        sharedInstance.amount = "20"

        
        firstReachLabel.textColor = UIColor.white
        firstAmountLable.textColor = UIColor.white

        secondReachLabel.textColor = UIColor.black
        secondAmountLabel.textColor = UIColor.black

        thirdReachLabel.textColor = UIColor.black
        thirdAmountLabel.textColor = UIColor.black

        fourthReachLabel.textColor = UIColor.black
        fourthAmountLabel.textColor = UIColor.black

        
        
        
        
    }
    
    
    @IBAction func secondBtn(_ sender: Any)
    {
        
        self.spendLabel.text = "Your Total Spend \nis $50 over 5 days"
        self.estProfileLabel.text = "1000 - 5000"

        
        productValue = 1
        
        sharedInstance.typeOfPayment = 1


        flag = true

        self.firstImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.secondImg.image = #imageLiteral(resourceName: "planSelect")
        
        self.thirdImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.fourthImg.image = #imageLiteral(resourceName: "planDeselect")
        
        sharedInstance.amount = "50"

        
        firstReachLabel.textColor = UIColor.black
        firstAmountLable.textColor = UIColor.black
        
        secondReachLabel.textColor = UIColor.white
        secondAmountLabel.textColor = UIColor.white
        
        thirdReachLabel.textColor = UIColor.black
        thirdAmountLabel.textColor = UIColor.black
        
        fourthReachLabel.textColor = UIColor.black
        fourthAmountLabel.textColor = UIColor.black
    }
    
    
    @IBAction func thirdBtn(_ sender: Any)
    {
     
        
        self.spendLabel.text = "Your Total Spend \nis $70 over 7 days"
        self.estProfileLabel.text = "5000 - 10000"

        
        productValue = 2
        
        sharedInstance.typeOfPayment = 2


        flag = true

        self.firstImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.secondImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.thirdImg.image = #imageLiteral(resourceName: "planSelect")
        
        self.fourthImg.image = #imageLiteral(resourceName: "planDeselect")
        
        sharedInstance.amount = "70"

        
        firstReachLabel.textColor = UIColor.black
        firstAmountLable.textColor = UIColor.black
        
        secondReachLabel.textColor = UIColor.black
        secondAmountLabel.textColor = UIColor.black
        
        thirdReachLabel.textColor = UIColor.white
        thirdAmountLabel.textColor = UIColor.white
        
        fourthReachLabel.textColor = UIColor.black
        fourthAmountLabel.textColor = UIColor.black
    }
    
    
    @IBAction func fourthBtn(_ sender: Any)
    {
        
        self.spendLabel.text = "Your Total Spend \nis $100 over 10 days"
        self.estProfileLabel.text = "10000 - 15000"

        
        productValue = 3

        sharedInstance.typeOfPayment = 3

        
        flag = true

        self.firstImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.secondImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.thirdImg.image = #imageLiteral(resourceName: "planDeselect")
        
        self.fourthImg.image = #imageLiteral(resourceName: "planSelect")
        
        sharedInstance.amount = "100"

        
        firstReachLabel.textColor = UIColor.black
        firstAmountLable.textColor = UIColor.black
        
        secondReachLabel.textColor = UIColor.black
        secondAmountLabel.textColor = UIColor.black
        
        thirdReachLabel.textColor = UIColor.black
        thirdAmountLabel.textColor = UIColor.black
        
        fourthReachLabel.textColor = UIColor.white
        fourthAmountLabel.textColor = UIColor.white
    }
    
    
    
    
}
