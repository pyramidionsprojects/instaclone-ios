//
//  CreatePromotionViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 15/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import KRProgressHUD
import SDWebImage
import Alamofire
import SwiftyJSON
import ActiveLabel
import ASPVideoPlayer


class PreviewPromotionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    var feedPosts = Array<Any>()

    
    @IBOutlet weak var videoPlayerTopView: UIView!
    @IBOutlet weak var videoPlayerMainView: UIView!

    @IBOutlet weak var videoPlayer: ASPVideoPlayer!


    
    var current_page = 1

    @IBOutlet weak var promoteTableView: UITableView!

    
    var sharedInstance = BusinessSharedManager.sharedInstance
    
    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(PreviewPromotionViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            
            return refreshControl
    }()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
     
        promoteTableView.delegate = self
        promoteTableView.dataSource = self
        
      
        loadFeeds()

    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
            {

        })
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)

        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    func loadFeeds()
    {
        

        KRProgressHUD.show(withMessage: "Loading...")
        
        
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":sharedInstance.createPostID
        ]
        
        print(params)
        let url = BASE_URL + VIEWPOST
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            self.feedPosts = JSON.object(forKey: "Postdetails")as! Array<Any>
                            
                            self.promoteTableView.reloadData()
                            
                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
      
        let cell = cell as! PreviewPromoteTableViewCell
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        cell.feedContentPosts  = (feedPosts["linkData"]as? Array)!
        
        
        cell.collectionVIew.reloadData()

    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return feedPosts.count
    }
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreviewPromoteTableViewCell") as! PreviewPromoteTableViewCell
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary

        
        cell.delegate = self

        
        let hashArray = feedPosts["caption_tags"]as? NSArray
        
        
        let imgString = feedPosts["profilePic"]as? String ?? ""
        
        
        cell.proImageView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
        
        
        var feedContentPosts = Array<Any>()
        
        feedContentPosts = (feedPosts["linkData"]as? Array)!
        
        
        print("feedContentPosts = ",feedContentPosts)
        
        var linkData = NSDictionary()
        
        
        linkData = feedContentPosts[0]as! NSDictionary
        
        
        
        let type = linkData["type"]as? String ?? ""
        
        
        
        print("Type = ",type)
        
        /*
        
        if type == "image"
        {
            let linkData = linkData["linkData"]as? String ?? ""
            
//            cell.displayImg.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
            sharedInstance.createPostImage = linkData
            
        }
        else if type == "video"
        {
            let linkData = linkData["thumbnail"]as? String ?? ""
            
//            cell.displayImg.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
            sharedInstance.createPostImage = linkData

        }
        else
        {
            let linkData = linkData["linkData"]as? String ?? ""
            
//            cell.displayImg.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
            sharedInstance.createPostImage = linkData

        }
        
        
        
        if feedPosts.object(forKey: "user_liked") as! String == "true"
        {
//            cell.likeBtn.setImage(UIImage(named: "heart_filled"), for: UIControlState())
            
        }
        else
        {
//            cell.likeBtn.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        
        if feedPosts.object(forKey: "user_pinned") as! String == "true"
        {
//            cell.bookmartBtn.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            
        }
        else
        {
//            cell.bookmartBtn.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
        }
        
        
*/
        
        cell.selectionStyle = .none
        
        
        cell.userNameLabel.text = feedPosts["name"]as? String ?? ""
        
        let currentDate = feedPosts["createdAt"]as? String
        
//        cell.postAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        cell.addressLabel.text = feedPosts["city"]as? String ?? ""
        
        let like = feedPosts["total_likes"] as! Int
        
        cell.likeLabel.text = String(describing: like)
        
        
        let comment = feedPosts["total_comments"] as! Int
        
        
        cell.commentLabel.text = String(describing: comment)
        
        
        let userName = feedPosts["name"]as? String ?? ""
        
        let patterns = "\\s" + userName + "\\b"
        
        
        let customType = ActiveType.custom(pattern: userName) //Looks for "are"
        
        //        let customType = ActiveType.custom(pattern: patterns)
        
        cell.desLabel.enabledTypes.append(customType)
        
        
        cell.desLabel.customColor[customType] = UIColor.black
        
        
        cell.desLabel.configureLinkAttribute = { (type, attributes, isSelected) in
            var atts = attributes
            switch type {
            case customType:
                atts[NSAttributedStringKey.font] = UIFont.boldSystemFont(ofSize: 16)
            default: ()
            }
            
            return atts
        }
        
        
        
        let caption = feedPosts["caption"]as? String ?? ""
        
        
        
        let wholeCaption = " " + userName + " " + caption
        
        
        print("wholeCaption = ",wholeCaption)
        
        print("patterns = ",patterns)
        
        cell.desLabel.text  = wholeCaption
        
        
        //        cell.desLabel.handleMentionTap { self.alert("Mention", message: $0) }
        //        cell.desLabel.handleHashtagTap { self.alert("Hashtag", message: $0) }
        
        /*
        cell.desLabel.handleMentionTap
            {
                self.viewProfile(userName: $0)
        }
        
        
        
        cell.desLabel.handleHashtagTap
            {
                self.alert("Hashtag", message: $0, hash: hashArray!)
        }
        */
        return cell
    }
    
    
   
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func closeVideoPlayer(_ sender: Any)
    {
        videoPlayerTopView.isHidden = true
        videoPlayerMainView.isHidden = true
        
        videoPlayer.videoPlayerControls.pause()
        videoPlayer.videoPlayerControls.stop()
    }
    
}

extension PreviewPromotionViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 3, height: collectionView.frame.size.width / 3)
    }
}


extension PreviewPromotionViewController: PreviewPromoteTableViewCellDelegate
{
    func goToCommentVC()
    {
        
    }
    
    func goToProfileUserVC() {
        
    }
    
    func playVideo(url :String)
    {
        
        
        videoPlayerTopView.isHidden = false
        videoPlayerMainView.isHidden = false
/*
        self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.videoPlayerMainView.transform = CGAffineTransform.identity
        },
                       completion:
            {
                Void in()
                
                let firstVideoURL  = URL(string: url)                 //returns a valid URL
                
                
                self.videoPlayer.videoURLs = [firstVideoURL!]
                
                
                self.videoPlayer.videoPlayerControls.play()
                
                
        })
        */
        
        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        
        
        self.videoPlayer.videoURLs = [firstVideoURL!]
        
        
        self.videoPlayer.videoPlayerControls.play()
        
        
        
        
    }
    
}




