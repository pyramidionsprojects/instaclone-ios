//
//  SetLocationViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 20/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class SetLocationViewController: UIViewController {

    @IBOutlet weak var doneAtn: UIButton!
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    
    @IBOutlet weak var newLocation: UILabel!
    
    @IBOutlet weak var firstImg: UIImageView!
    @IBOutlet weak var secondImg: UIImageView!
    
    @IBOutlet weak var newLocView: UIView!
    @IBOutlet weak var newLocHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sharedInstance.currentLocation = "0"
        
        newLocView.isHidden = true
        newLocHeight.constant = 0

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if sharedInstance.currentLocation == "1"
        {
            newLocView.isHidden = false
            newLocHeight.constant = 50
            firstImg.image = #imageLiteral(resourceName: "empty_circle")
            secondImg.image = #imageLiteral(resourceName: "fill_circle")
            
            newLocation.text = sharedInstance.targetLat+" , "+sharedInstance.targetLon            
        }
        else
        {
            newLocView.isHidden = true
            newLocHeight.constant = 0
            firstImg.image = #imageLiteral(resourceName: "fill_circle")
            secondImg.image = #imageLiteral(resourceName: "empty_circle")
        }        
    }
    
    
    @IBAction func setCurrentBtn(_ sender: Any)
    {
        sharedInstance.currentLocation = "0"

        firstImg.image = #imageLiteral(resourceName: "fill_circle")
        secondImg.image = #imageLiteral(resourceName: "empty_circle")
    }
    
    
    @IBAction func addLocationBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "AddAddressNavViewController")as! AddAddressNavViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    @IBAction func back(_ sender: Any)
    {
        
        sharedInstance.currentLocation = "0"
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func doneBtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func newLocBtn(_ sender: Any)
    {
        sharedInstance.currentLocation = "1"

        firstImg.image = #imageLiteral(resourceName: "empty_circle")
        secondImg.image = #imageLiteral(resourceName: "fill_circle")
    }
    
    
   
}
