//
//  BusinessSharedManager.swift
//  Instafeed
//
//  Created by Pyramidions on 22/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class BusinessSharedManager
{
    static let sharedInstance = BusinessSharedManager()
    
    var createPostID : String = ""
    var createPostImage : String = ""
    var goalType : String = ""
    var websiteUrl: String = ""
    var actionButton: String = ""
    var audienceType: String = ""
    var radius: String = "0"
    var ageMin: String = "0"
    var ageMax: String = "0"
    var gender: String = ""
    var amount: String = ""
    var targetLat: String = ""
    var targetLon: String = ""
    var currentLocation: String = ""
    var paymentSuccess: String = ""
    var typeOfPayment: Int = 0

    func clearData()
    {
        createPostID = ""
        createPostImage = ""
        goalType = ""
        websiteUrl = ""
        actionButton = ""
        audienceType = ""
        radius = "0"
        ageMin = "0"
        ageMax = "0"
        gender = ""
        amount = ""
        targetLat = ""
        targetLon = ""
        currentLocation = ""
        paymentSuccess = ""
        typeOfPayment = 1
    }
    
    
    func print()
    {
        Swift.print("createPostID = ",createPostID)
        Swift.print("createPostImage = ",createPostImage)
    }

}
