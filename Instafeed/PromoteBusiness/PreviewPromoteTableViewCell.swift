//
//  PreviewPromoteTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 23/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

import ActiveLabel


protocol PreviewPromoteTableViewCellDelegate
{
    func goToCommentVC()
    func goToProfileUserVC()
    func playVideo(url :String)
    
}


class PreviewPromoteTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate
{

    @IBOutlet weak var collectionVIew: UICollectionView!
    
    
    var delegate: PreviewPromoteTableViewCellDelegate?
    
    var feedContentPosts = Array<Any>()
    
    @IBOutlet weak var postAgoLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var proImageView: UIImageView!
    
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var desLabel: ActiveLabel!
    
    @IBOutlet weak var displayImg: UIImageView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    @IBOutlet weak var promoteLabel: UILabel!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize(width: collectionVIew.frame.size.width, height: collectionVIew.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        
        self.collectionVIew?.collectionViewLayout = flowLayout
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
    }
    
    @objc func commentImageView_TouchUpInside()
    {
        delegate?.goToCommentVC()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.feedContentPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
        
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        
        
        let type = feedPosts["type"]as? String ?? ""
        
        
        if type == "image"
        {
            cell.playImage.isHidden = true
            
            let linkData = feedPosts["linkData"]as? String ?? ""
            
            cell.instaImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
        }
        else
        {
            cell.playImage.isHidden = false
            
            let linkData = feedPosts["thumbnail"]as? String ?? ""
            
            cell.instaImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
            
        }
        
        
        self.pageControl.numberOfPages = self.feedContentPosts.count
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if let _ = scrollView as? UITableView
        {
            print("tableview")
        }
        else if let _ = scrollView as? UICollectionView
        {
            print("collectionview")
            
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        print("End indexPath = ",indexPath.row)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        print("Display indexPath = ",indexPath.row)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        let linkData = feedPosts["linkData"]as? String ?? ""
        
        let type = feedPosts["type"]as? String ?? ""
        
        
        if type == "video"
        {
            delegate?.playVideo(url: linkData)
            
        }
        
    }
    
    
}


extension PreviewPromoteTableViewCell: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}



extension PreviewPromoteTableViewCell
{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionVIew.delegate = dataSourceDelegate
        collectionVIew.dataSource = dataSourceDelegate
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize(width: collectionVIew.frame.size.width, height: collectionVIew.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        
        self.collectionVIew?.collectionViewLayout = flowLayout
        
        collectionVIew.tag = row
        collectionVIew.setContentOffset(collectionVIew.contentOffset, animated:false) // Stops collection view if it was scrolling.
        self.collectionVIew.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionVIew.contentOffset.x = newValue }
        get { return collectionVIew.contentOffset.x }
    }
    
}
