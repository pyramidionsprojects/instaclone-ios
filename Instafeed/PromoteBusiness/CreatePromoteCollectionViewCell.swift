//
//  CreatePromoteCollectionViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 15/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class CreatePromoteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var feedImage: UIImageView!
    
    @IBOutlet weak var multipleCount: UIImageView!
    
}
