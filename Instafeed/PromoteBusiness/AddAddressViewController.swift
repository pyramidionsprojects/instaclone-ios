//
//  ViewController.swift
//  AutoCompleteAddress
//
//  Created by Agus Cahyono on 2/23/16.
//  Copyright © 2016 Agus Cahyono. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class AddAddressViewController: UIViewController , UISearchBarDelegate ,GMSMapViewDelegate, AddAddressLocateOnTheMap,GMSAutocompleteFetcherDelegate {
    
    var indicator: NVActivityIndicatorView!
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }

    public func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        for prediction in predictions
        {
            if let prediction = prediction as GMSAutocompletePrediction!
            {
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    
    
    @IBOutlet weak var googleMapsContainer: UIView!
    
    
    var oldLatitude : Double = 0.0
    var oldLongitude : Double = 0.0
    var Address: String? = nil
    var Country: String? = nil
    var googleMapsView: GMSMapView!
    var searchResultController: AddAddressSearchResultsController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        searchResultController = AddAddressSearchResultsController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
        
        
        
    }
    

    @IBAction func searchWithAddress(_ sender: AnyObject)
    {
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
       self.present(searchController, animated:true, completion: nil)
    }
    
  
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String)
    {
        
        do
        {
            resultsArray.removeAll()
            
            resultsArray = [String]()
            
            DispatchQueue.main.async { () -> Void in
                
                
                self.oldLatitude = lat
                self.oldLongitude = lon
                
                
                self.sharedInstance.targetLat = String(describing: lat)
                self.sharedInstance.targetLon = String(describing: lon)
                
                self.sharedInstance.currentLocation = "1"

                
                self.dismiss(animated: true, completion: nil)
                
                /*
                
                CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: lon))
                {
                    placemarks, error in
                    
                    var placemark:CLPlacemark
                    
                    placemark = (placemarks?.last)!

                    
                    self.Address =  (placemark.subLocality)
                    self.Country = (placemark.locality)
                    
                    
/*                    let complteAddress = self.Address! + ", " + self.Country!
                    
                    
                    UserDefaults.standard.set(complteAddress, forKey: locationValue)
                    UserDefaults.standard.set(self.Address, forKey: "area")
                    UserDefaults.standard.set(self.Country, forKey: "city")
*/
                    
                    
                    self.dismiss(animated: true, completion: nil)
                    
//                    self.googleMapsView.selectedMarker=marker
                    
                }*/
            }
        }
        catch
        {
            print("Swift try catch is confusing...")
        }
        
        
       
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    

    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: GMSMapViewDelegate
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func back(_ sender: Any)
    {
        sharedInstance.currentLocation = "0"

        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}



