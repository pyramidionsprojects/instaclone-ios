//
//  AudienceAddViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 20/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import RangeSeekSlider
import GoogleMaps
import GooglePlaces



class AudienceAddViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate
{

    
    var sharedInstance = BusinessSharedManager.sharedInstance

    private lazy var locationManager: CLLocationManager =
    {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        return locationManager
    }()

    
    @IBOutlet weak var locationLabel: UILabel!
    

    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var radiusImg: UIImageView!
    
    
    @IBOutlet weak var mapview: GMSMapView!
    
    @IBOutlet weak var maleImg: UIImageView!
    @IBOutlet weak var femaleImg: UIImageView!
    
    @IBOutlet weak var doneAtn: UIButton!
    
    
    var maleFlag = false
    var femaleFlag = false
    
    
    @IBOutlet weak var ageRangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        pinImage.isHidden = true
        radiusImg.isHidden = true

        rangeSlider.delegate = self
        rangeSlider.disableRange = true
        rangeSlider.step = 1.0
        
        rangeSlider.maxValue = 30
        rangeSlider.selectedMaxValue = 1

        ageRangeSlider.delegate = self
        ageRangeSlider.step = 1.0
        
        mapview.delegate = self
        locationManager.delegate = self

        locationManager.requestWhenInUseAuthorization()
        initializeTheLocationManager()
        self.mapview.isMyLocationEnabled = false
        
        sharedInstance.currentLocation = ""

    }
    
    func initializeTheLocationManager()
    {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if sharedInstance.currentLocation == "1"
        {
            locationLabel.text = sharedInstance.targetLat+" , "+sharedInstance.targetLon
            
            let lat = Double(sharedInstance.targetLat)

            let log = Double(sharedInstance.targetLon)
            
            let location = CLLocationCoordinate2D(latitude: lat!, longitude: log!)
            
            mapview.camera = GMSCameraPosition(target: location, zoom: 30, bearing: 0, viewingAngle: 0)

        }
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func maleBtn(_ sender: Any)
    {
        
        if maleFlag
        {
            if femaleFlag
            {
                self.maleImg.image = #imageLiteral(resourceName: "empty_circle")
                
                maleFlag = false
            }
        }
        else
        {
            maleFlag = true
            
            self.maleImg.image = #imageLiteral(resourceName: "fill_circle")
        }
    }
    
    @IBAction func femaleBtn(_ sender: Any)
    {
        if femaleFlag
        {
            if maleFlag
            {
                self.femaleImg.image = #imageLiteral(resourceName: "empty_circle")
                
                femaleFlag = false
            }
        }
        else
        {
            
            femaleFlag = true
            
            self.femaleImg.image = #imageLiteral(resourceName: "fill_circle")
        }

    }
    
    
    @IBAction func locationBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "SetLocationViewController")as! SetLocationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back(_ sender: Any)
    {

        sharedInstance.currentLocation = ""

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtn(_ sender: Any)
    {
        
        sharedInstance.currentLocation = "1"
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location = locations.first
        {
            mapview.camera = GMSCameraPosition(target: location.coordinate, zoom: 30, bearing: 0, viewingAngle: 0)
            
            pinImage.isHidden = false
            radiusImg.isHidden = false
            
            locationLabel.text = String(describing: location.coordinate.latitude)+" , "+String(describing: location.coordinate.longitude)
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)
    {
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
    }
    
}


extension AudienceAddViewController: RangeSeekSliderDelegate
{
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat)
    {
        if slider === rangeSlider
        {
            
            let radius : CGFloat = maxValue - 30

            self.mapview.animate(toZoom: Float(abs(radius)))

            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            
            sharedInstance.radius = String(describing: Int(maxValue))

        }
        else if slider == ageRangeSlider
        {
            print("Age slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            
            sharedInstance.ageMax = String(describing: Int(maxValue))
            sharedInstance.ageMin = String(describing: Int(minValue))

        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider)
    {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider)
    {
        print("did end touches")
    }
}

