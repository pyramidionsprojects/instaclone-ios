//
//  OutComeViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 18/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class AddURLViewController: UIViewController
{

    
    @IBOutlet weak var webURLTxt: UITextField!
    
    @IBOutlet weak var learnMoreImg: UIImageView!
    @IBOutlet weak var shopNowImg: UIImageView!
    @IBOutlet weak var watchMoreImg: UIImageView!
    @IBOutlet weak var contactUsImg: UIImageView!
    @IBOutlet weak var bookNowImg: UIImageView!
    @IBOutlet weak var signUpImg: UIImageView!
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    var flag = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        sharedInstance.actionButton = "Learn More"
        
        webURLTxt.text = sharedInstance.websiteUrl

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
  
        
    }
    
  
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if webURLTxt.hasText
        {
            if flag
            {
                
                if webURLTxt.text?.lowercased().range(of:"http://") != nil
                {
                    sharedInstance.websiteUrl = webURLTxt.text!
                }
                else
                {
                    let simpler = "http://" + webURLTxt.text!
                    sharedInstance.websiteUrl = simpler
                }
                
                sharedInstance.goalType = "2"
                
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                let alertController = UIAlertController(title: "Oops", message: "Select your Audience type", preferredStyle: .alert)

                let action1 = UIAlertAction(title: "Ok", style: .default)
                {
                    (action:UIAlertAction) in
                }

                alertController.addAction(action1)

                self.present(alertController, animated: true, completion: nil)
            }
        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Enter your website", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func learnMore(_ sender: Any)
    {
        
        sharedInstance.actionButton = "Learn More"

        learnMoreImg.image = #imageLiteral(resourceName: "fill_circle")
        shopNowImg.image = #imageLiteral(resourceName: "empty_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        contactUsImg.image = #imageLiteral(resourceName: "empty_circle")
        bookNowImg.image = #imageLiteral(resourceName: "empty_circle")
        signUpImg.image = #imageLiteral(resourceName: "empty_circle")
        

    }
    
    @IBAction func shopNow(_ sender: Any)
    {
        
        sharedInstance.actionButton = "Shop Now"

        learnMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        shopNowImg.image = #imageLiteral(resourceName: "fill_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        contactUsImg.image = #imageLiteral(resourceName: "empty_circle")
        bookNowImg.image = #imageLiteral(resourceName: "empty_circle")
        signUpImg.image = #imageLiteral(resourceName: "empty_circle")
    }
    
    
    @IBAction func watchMore(_ sender: Any)
    {
        
        sharedInstance.actionButton = "Watch More"

        learnMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        shopNowImg.image = #imageLiteral(resourceName: "empty_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "fill_circle")
        contactUsImg.image = #imageLiteral(resourceName: "empty_circle")
        bookNowImg.image = #imageLiteral(resourceName: "empty_circle")
        signUpImg.image = #imageLiteral(resourceName: "empty_circle")
    }
    
    
    @IBAction func contactUs(_ sender: Any)
    {
        
        sharedInstance.actionButton = "Contact Us"

        learnMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        shopNowImg.image = #imageLiteral(resourceName: "empty_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        contactUsImg.image = #imageLiteral(resourceName: "fill_circle")
        bookNowImg.image = #imageLiteral(resourceName: "empty_circle")
        signUpImg.image = #imageLiteral(resourceName: "empty_circle")
    }
    
    
    @IBAction func bookNow(_ sender: Any)
    {
        
        sharedInstance.actionButton = "Book Now"

        learnMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        shopNowImg.image = #imageLiteral(resourceName: "empty_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        contactUsImg.image = #imageLiteral(resourceName: "empty_circle")
        bookNowImg.image = #imageLiteral(resourceName: "fill_circle")
        signUpImg.image = #imageLiteral(resourceName: "empty_circle")
    }
    
    
    @IBAction func signUp(_ sender: Any)
    {
        sharedInstance.actionButton = "Sign Up"

        learnMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        shopNowImg.image = #imageLiteral(resourceName: "empty_circle")
        watchMoreImg.image = #imageLiteral(resourceName: "empty_circle")
        contactUsImg.image = #imageLiteral(resourceName: "empty_circle")
        bookNowImg.image = #imageLiteral(resourceName: "empty_circle")
        signUpImg.image = #imageLiteral(resourceName: "fill_circle")
    }
    
    
    
    
}
