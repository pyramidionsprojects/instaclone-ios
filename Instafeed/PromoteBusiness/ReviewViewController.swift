//
//  OutComeViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 18/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import AWSS3
import AWSCore
import KRProgressHUD
import SDWebImage
import JGProgressHUD


class ReviewViewController: UIViewController
{

   
    @IBOutlet weak var proVisitLAbel: UILabel!
    @IBOutlet weak var proReachLabel: UILabel!
    
    @IBOutlet weak var previewImg: UIImageView!
    
    
    @IBOutlet weak var outcomeLbl: UILabel!
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var actionLbl: UILabel!
    @IBOutlet weak var audienceLbl: UILabel!
    @IBOutlet weak var budgetLbl: UILabel!
    
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    override func viewDidLoad()
    {
        super.viewDidLoad()

        let type = sharedInstance.goalType
        
        if type == "1"
        {
            outcomeLbl.text = "Profile Visits"
        }
        else if type == "2"
        {
            outcomeLbl.text = "Website Traffic"
        }
       
        
        let audienceType = sharedInstance.audienceType
        
        if audienceType == "1"
        {
            audienceLbl.text = "Automatic"
        }
        else if type == "2"
        {
            audienceLbl.text = "Local"
        }
        
        
        if sharedInstance.actionButton == ""
        {
            actionLbl.text = "Visit Instafeed profile"
        }
        else
        {
            actionLbl.text = sharedInstance.actionButton
        }

        budgetLbl.text = "$"+sharedInstance.amount
        
        
        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String
        
        if proStatus != nil
        {
            destinationLbl.text = "@ " + proStatus
        }
        else
        {
            destinationLbl.text = ""
        }
        
                
        previewImg.sd_setImage(with: URL(string: sharedInstance.createPostImage), placeholderImage: UIImage(named: "default-user-image"))

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
       
        IAPHandler.shared.fetchAvailableProducts()
        
        IAPHandler.shared.purchaseStatusBlock =
            {
                [weak self] (type) in
                guard let strongSelf = self else{ return }
                if type == .purchased
                {
                    let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler:
                    {
                        (alert) in
                        
                        
                        self?.paymentSuccess()
                        
                    })
                    alertView.addAction(action)
                    strongSelf.present(alertView, animated: true, completion: nil)
                }
                else if type == .failed
                {
                    let alertView = UIAlertController(title: "", message: type.message(), preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .default, handler:
                    {
                        (alert) in
                        
                        self?.sharedInstance.paymentSuccess = ""
                        
                    })
                    alertView.addAction(action)
                    strongSelf.present(alertView, animated: true, completion: nil)
                }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        

    }
    
    
   
    func paymentSuccess()
    {
        
       sharedInstance.paymentSuccess = "1"
        
       switchBusiness()
        
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if sharedInstance.paymentSuccess == ""
        {
            IAPHandler.shared.purchaseMyProduct(index: sharedInstance.typeOfPayment)
        }
        else
        {
            switchBusiness()
        }
    }
    
    
    @IBAction func previewBtn(_ sender: Any)
    {                
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "PreviewPromotionViewController")as! PreviewPromotionViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func createBtn(_ sender: Any)
    {
        if sharedInstance.paymentSuccess == ""
        {
            IAPHandler.shared.purchaseMyProduct(index: sharedInstance.typeOfPayment)
        }
        else
        {
            switchBusiness()
        }
    }
    
    
    func switchBusiness()
    {
        
        KRProgressHUD.show(withMessage: "Creating Promotion...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let ageMin = Int(sharedInstance.ageMin)
        let ageMax = Int(sharedInstance.ageMax)

        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":sharedInstance.createPostID,
            "goalType":sharedInstance.goalType,
            "audienceType":sharedInstance.audienceType,
            "radius":sharedInstance.radius,
            "ageMin":ageMin!,
            "ageMax":ageMax!,
            "gender":"",
            "targetName":"",
            "targetLat":sharedInstance.targetLat,
            "targetLon":sharedInstance.targetLon,
            "intrest":"",
            "amount":sharedInstance.amount,
            "websiteUrl":sharedInstance.websiteUrl,
            "actionButton":sharedInstance.actionButton
        ]
        
        print(params)
        let url = BASE_URL + PROMOTE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    KRProgressHUD.dismiss()
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
//                            Dvc.moveIndex = 4
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
