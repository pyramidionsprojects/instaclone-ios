//
//  BusinessContinueViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 14/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class BusinessContinueViewController: UIViewController
{

    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var proLabel: UILabel!
    
    var proImgString: String = ""

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.proLabel.text = ""
        
        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String

        self.proLabel.text = "Welcome to Snipofeed Business Tools, " + proStatus
        
        proImage.sd_setImage(with: URL(string: proImgString), placeholderImage: UIImage(named: "default-user-image"))
        
        
        self.proImage.layer.cornerRadius = self.proImage.frame.size.width / 2
        self.proImage.layer.masksToBounds = true
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func continueBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "BusinessReviewViewController")as! BusinessReviewViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(Dvc, animated: true, completion: nil)
    }
    
    @IBAction func backBtn(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil);
    }
    
}
