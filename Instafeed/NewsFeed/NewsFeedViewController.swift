//
//  NewsFeedViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 01/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import AVFoundation
import MMPlayerView
import CRRefresh
import ASPVideoPlayer
import AVFoundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage
import ActiveLabel
import UIEmptyState





class NewsFeedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,commentCountChangeDelegate,UIEmptyStateDelegate, UIEmptyStateDataSource,UICollectionViewDataSource, UICollectionViewDelegate
 {
    
    
    @IBOutlet weak var emptyView: UIView!
    
    
    @IBOutlet weak var storiesCollectionView: UICollectionView!
    
    
    
    var currentTableIndex : Int = 0

    func commentCount()
    {
        var dict = NSMutableDictionary()
        
        
        let indexPath = IndexPath(item: currentTableIndex, section: 0)

        let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell

        
        dict = NSMutableDictionary(dictionary: self.feedPosts[currentTableIndex] as! NSDictionary)
        
        
        print("Dict = ",dict)
        
            
        dict["total_comments"] = (dict["total_comments"] as! Int + 1)
            
            
        let pID = (dict["total_comments"] as! Int)
            
        
        print("Dict = ",pID)

        
        let id = String(describing: pID)
  
        
        cell.commentLabel.text = String(describing: id)
        
        
        self.feedPosts[currentTableIndex] = dict


            
    }
        
        
    
        
    
    

    
//    var posts = Post()
//
//    
//    lazy var mmPlayerLayer: MMPlayerLayer =
//        {
//            let l = MMPlayerLayer()
//            l.cacheType = .memory(count: 5)
//            l.coverFitType = .fitToPlayerView
//            l.videoGravity = AVLayerVideoGravity.resizeAspectFill
//            //        l.replace(cover: CoverA.instantiateFromNib())
//            return l
//    }()
//
    
    
    lazy var refreshControl: UIRefreshControl =
    {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(NewsFeedViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor(hexString: "#0074BB")
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")

        return refreshControl
    }()
    
    
    
    @IBOutlet weak var videoPlayerTopView: UIView!
    @IBOutlet weak var videoPlayerMainView: UIView!

    @IBOutlet weak var videoPlayer: ASPVideoPlayer!
    
    @IBOutlet weak var newsFeedTblView: UITableView!
    
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 1
    
    
    var feedPosts = Array<Any>()
    var storyData = Array<Any>()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.backToBlockUser(_:)), name: NSNotification.Name(rawValue: "backToBlockUser"), object: nil)

        
        
        newsFeedTblView.estimatedRowHeight = 77
        newsFeedTblView.rowHeight = UITableViewAutomaticDimension
        
        
        self.loadFeeds()
        
        
        self.newsFeedTblView.addSubview(self.refreshControl)

        
      /*
        newsFeedTblView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            /// start refresh
            /// Do anything you want...
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute:
            {
                /// Stop refresh when your job finished, it will reset refresh footer if completion is true
//                self?.newsFeedTblView.cr.endHeaderRefresh()
                
                
                self?.pullToloadFeeds()
                
            })
        }
        /// manual refresh
//        newsFeedTblView.cr.beginHeaderRefresh()
        
        */
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        print("View will Layouts")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        print("View Did Layouts")
    }
    
    var emptyStateImageSize: CGSize?
    {
        return CGSize(width: 60, height: 60)
    }
    
    
    public func reloadEmptyState() {
        self.reloadEmptyStateForTableView(self.newsFeedTblView)
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "empty_home")
    }
    
    var emptyStateTitle: NSAttributedString {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 22)]
        return NSAttributedString(string: "Welcome to Snipofeed", attributes: attrs)
    }
    
    var emptyStateButtonTitle: NSAttributedString? {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.white,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]
        return NSAttributedString(string: "Refresh", attributes: attrs)
    }
    
    var emptyStateButtonSize: CGSize? {
        return CGSize(width: 80, height: 30)
    }
    
    
    func emptyStatebuttonWasTapped(button: UIButton)
    {
        loadFeeds()
    }
    
    // MARK: - Empty State Delegate
    
    func emptyStateViewWillShow(view: UIView) {
        guard let emptyView = view as? UIEmptyStateView else { return }
        // Some custom button stuff
        emptyView.button.layer.cornerRadius = 5
        emptyView.button.layer.borderWidth = 1
        emptyView.button.layer.borderColor = UIColor.black.cgColor
        emptyView.button.layer.backgroundColor = UIColor.black.cgColor
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        
//        refreshControl.endRefreshing()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
        {
            self.pullToloadFeeds()
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.post(name: Notification.Name("enableSwipeGesture"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        
        videoPlayerTopView.isHidden = true
        videoPlayerMainView.isHidden = true
        
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
        //        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        //
        //
        //        videoPlayer.videoURLs = [firstVideoURL!]
        
        
        
        
        videoPlayer.videoPlayerControls.pause()
        
        videoPlayer.videoPlayerControls.stop()
        
        
    }
    
    
    
    
    @objc func backToBlockUser(_ notification: NSNotification)
    {
        self.loadFeeds()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func closeVideoPlayer(_ sender: Any)
    {
        videoPlayerTopView.isHidden = true
        videoPlayerMainView.isHidden = true
        
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
//        let firstVideoURL  = URL(string: url)                 //returns a valid URL
//
//
//        videoPlayer.videoURLs = [firstVideoURL!]
        

        
        
        videoPlayer.videoPlayerControls.pause()
        
        videoPlayer.videoPlayerControls.stop()
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: locationValue)
        
        prefs.removeObject(forKey: "area")
        prefs.removeObject(forKey: "city")
        
/*        videoPlayerTopView.isHidden = false
        videoPlayerMainView.isHidden = false
        
        
        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"

        
        let firstVideoURL  = URL(string: videoUrl)                 //returns a valid URL

        
        videoPlayer.videoURLs = [firstVideoURL!]
        
        
        videoPlayer.videoPlayerControls.play()

        */
        
        
    }
        
        
    
   
    
    
    
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 10
//    }
    
    
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.feedPosts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
        
        cell.delegate = self
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        print("FeedPosts = ",feedPosts)
        
        
        let hashArray = feedPosts["caption_tags"]as? NSArray
        

        print("hashArray = ",hashArray!)


        let imgString = feedPosts["profilePic"]as? String ?? ""
        
        
        var arrayCount = Array<Any>()
        
        arrayCount  = (feedPosts["linkData"]as? Array)!

        if arrayCount.count > 1
        {
            cell.pageControl.isHidden = false
        }
        else
        {
            cell.pageControl.isHidden = true
        }
        
        let isPromoted = feedPosts["isPromoted"]as? String ?? ""
        
        if isPromoted == "1"
        {
            let goalType = feedPosts["goalType"]as? String ?? ""
            
            if goalType == "1"
            {
                cell.sponserView.isHidden = true
            }
            else
            {
                cell.sponserView.isHidden = false
                
                cell.sponserLabel.text = feedPosts["actionButton"]as? String ?? ""
            }
            
        }
        else
        {
            cell.sponserView.isHidden = true
        }
        
        
        

        
        cell.sponserBtn.addTarget(self, action: #selector(NewsFeedViewController.sponserBtn(sender:)), for: .touchUpInside)
        
        cell.sponserBtn.tag = indexPath.row
        
        
        cell.proImageView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))


        if feedPosts.object(forKey: "user_liked") as! String == "true"
        {
            cell.likeBtn.setImage(UIImage(named: "heart_filled"), for: UIControlState())

        }
        else
        {
            cell.likeBtn.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        
        if feedPosts.object(forKey: "user_pinned") as! String == "true"
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            
        }
        else
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
        }
        
        
        
        cell.bookmartBtn.addTarget(self, action: #selector(NewsFeedViewController.bookmartAtn(sender:)), for: .touchUpInside)

        cell.bookmartBtn.tag = indexPath.row

        cell.likeBtn.addTarget(self, action: #selector(NewsFeedViewController.likeBtn(sender:)), for: .touchUpInside)
        
        cell.likeBtn.tag = indexPath.row
        
        let cmtStatus = feedPosts["commentingStatus"] as! Int
        
        let commentStatus = String(describing: cmtStatus)
        
        cell.commentBtn.addTarget(self, action: #selector(NewsFeedViewController.commentBtn(_:)), for: .touchUpInside)
        
        cell.commentBtn.tag = indexPath.row

        if commentStatus == "1"
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = true
        }
        else
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = false
        }

        cell.viewProfileBtn.addTarget(self, action: #selector(NewsFeedViewController.viewProfileBtn(_:)), for: .touchUpInside)
        
        cell.viewProfileBtn.tag = indexPath.row
        
        cell.reportBtn.addTarget(self, action: #selector(NewsFeedViewController.reportBtn(_:)), for: .touchUpInside)
        cell.reportBtn.tag = indexPath.row
        
        cell.selectionStyle = .none
        
        
        cell.userNameLabel.text = feedPosts["name"]as? String ?? ""
        
        let currentDate = feedPosts["createdAt"]as? String
        
        cell.postAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        
        if isPromoted == "1"
        {
            cell.addressLabel.text = "Sponsored"
        }
        else
        {
            cell.addressLabel.text = feedPosts["city"]as? String ?? ""
        }
        
        let like = feedPosts["total_likes"] as! Int
        
        cell.likeLabel.text = String(describing: like)

        
        let comment = feedPosts["total_comments"] as! Int

        
        cell.commentLabel.text = String(describing: comment)
        
        
        let userName = feedPosts["name"]as? String ?? ""
        
        let patterns = "\\s" + userName + "\\b"
        
        
        let customType = ActiveType.custom(pattern: userName) //Looks for "are"
        
//        let customType = ActiveType.custom(pattern: patterns)
        
        cell.desLabel.enabledTypes.append(customType)
        
        cell.desLabel.customColor[customType] = UIColor.black
        
        cell.desLabel.configureLinkAttribute =
        {
            (type, attributes, isSelected) in
            var atts = attributes
            switch type
            {
                case customType:
                    atts[NSAttributedStringKey.font] = UIFont.boldSystemFont(ofSize: 16)
                default: ()
            }
            return atts
        }
        
        let caption = feedPosts["caption"]as? String ?? ""
        
        let wholeCaption = " " + userName + " " + caption
        
        print("wholeCaption = ",wholeCaption)
        
        print("patterns = ",patterns)
        
        cell.desLabel.text  = wholeCaption
        
        cell.desLabel.handleMentionTap
        {
            self.viewProfile(userName: $0)
        }

        cell.desLabel.handleHashtagTap
        {
            self.alert("Hashtag", message: $0, hash: hashArray!)
        }

        
        return cell
    }

    
    func viewProfile(userName :String)
    {
        
        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String
        
        if proStatus == userName
        {
            NotificationCenter.default.post(name: Notification.Name("changeTabbarItem"), object: nil)
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
            vc.userName = userName
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
        
//        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        
//        let proStatus = dict["is_my_profile"]as? String ?? ""
        
//        if proStatus == "false"
//        {
        
        
/*
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
            vc.userName = userName
            self.navigationController?.pushViewController(vc, animated: true)*/
//        }
//        else
//        {
//            NotificationCenter.default.post(name: Notification.Name("changeTabbarItem"), object: nil)
//        }
        
    }
    
    
    func alert(_ title: String, message: String, hash: NSArray)
    {
        
        print("Hash Tag = ",hash)
        
        var hashTag : String = "#" + message
        
        var flag : Bool = false
        
        var currentID = 0
        
        for var i in 0..<hash.count
        {
            let people = hash[i] as! NSDictionary
            let tagName = people["tagName"]as? String ?? ""
            
            if tagName == hashTag
            {
                flag = true
                
                currentID = i;
                
                break;
            }
        }
        
        
        if flag
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "HashViewController")as! HashViewController
            
            let people = hash[currentID] as! NSDictionary

            
            let cmtStatus = people["tagId"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            vc.userName = commentStatus
            
            vc.tgName = hashTag

            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "HashViewController")as! HashViewController
            
/*            let people = hash[currentID] as! NSDictionary
            
            
            let cmtStatus = people["tagId"] as! Int
            
            let commentStatus = String(describing: cmtStatus)*/
            
            vc.userName = "0"
            
            vc.tgName = hashTag
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func sponserBtn(sender: UIButton)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let websiteURL = feedPosts["websiteUrl"]as? String ?? ""
        
        let url = URL(string: websiteURL)
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url!)
            
        }
        
    }
    
    
    
    
    @objc func bookmartAtn(sender: SparkButton)
    {
        var dict = NSMutableDictionary()
        
        
        dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
        
        
        print("Dict = %@",dict)
                
        if (dict.object(forKey: "user_pinned") as! String == "true")
        {
            sender.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
            sender.likeBounce(0.6)
            sender.animate()
            
            dict["user_pinned"] = "false"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinRemovePost(postId: id)
            
        }
        else
        {
            sender.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            sender.unLikeBounce(0.4)
            
            dict["user_pinned"] = "true"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinPost(postId: id)
        }
        
/*
        let indexPath = IndexPath(item: sender.tag, section: 0)
        
        
        let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell*/
        
        self.feedPosts[sender.tag] = dict
        
        
/*        let like = (dict["total_likes"] as! Int)
        
        cell.likeLabel.text = String(describing: like)
  */
        
    }
    
    
    @objc func viewProfileBtn(_ sender: AnyObject)
    {
        
//        self.view.setNeedsLayout()
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let proStatus = dict["is_my_profile"]as? String ?? ""

        if proStatus == "false"
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
            vc.userName = dict["username"]as? String ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            NotificationCenter.default.post(name: Notification.Name("changeTabbarItem"), object: nil)
        }
        
    }
    
    @objc func reportBtn(_ sender: AnyObject)
    {
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let proStatus = dict["is_my_profile"]as? String ?? ""
        
        if proStatus == "true"
        {
            
            let cmtStatus = dict["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            
            if commentStatus == "1"
            {
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                
                let deleteAction = UIAlertAction(title: "Delete Post", style: .destructive, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.deletePost(postid: id, currentIndex: sender.tag)
                    
                })
                
                
                let commentAction = UIAlertAction(title: "Turn off comment", style: .destructive, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                    
                    self.turnOffComment(postid: id, currentIndex: sender.tag)
                    
//                    self.deletePost(postid: id, currentIndex: sender.tag)
                    
                })
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                })
                optionMenu.addAction(deleteAction)
                optionMenu.addAction(commentAction)
                optionMenu.addAction(cancelAction)
                self.present(optionMenu, animated: true, completion: nil)
            }
            else
            {
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                
                let deleteAction = UIAlertAction(title: "Delete Post", style: .destructive, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                    self.deletePost(postid: id, currentIndex: sender.tag)
                    
                })

                
                let commentAction = UIAlertAction(title: "Turn on comment", style: .destructive, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
//                    self.deletePost(postid: id, currentIndex: sender.tag)
                    
                    
                    self.turnOnComment(postid: id, currentIndex: sender.tag)
                    
                })

                
                
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    
                })
                optionMenu.addAction(deleteAction)
                optionMenu.addAction(commentAction)
                optionMenu.addAction(cancelAction)
                self.present(optionMenu, animated: true, completion: nil)
            }
            
        }
        else
        {
        
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            
            
            
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let saveAction = UIAlertAction(title: "Report", style: .destructive, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.report(postid: id, currentIndex: sender.tag)

            })
            
            let deleteAction = UIAlertAction(title: "Unfollow", style: .destructive, handler:
            {
                (alert: UIAlertAction!) -> Void in


                let pID = (dict["postedBy"] as! Int)
                
                let id = String(describing: pID)
                
                
                self.unFollowPost(postId: id)

            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in

            })
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
            
        }
        
        
    }
    
   
    func unFollowPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "followerId":postId
        ]
        
        print(params)
        let url = BASE_URL + UNFOLLOW
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            self.loadFeeds()
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    func report(postid :String,currentIndex :Int)
    {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "It's inappropriate", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.newsFeedTblView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let deleteAction = UIAlertAction(title: "It's spam", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in


            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.newsFeedTblView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
                
    }
    
    
    func turnOffComment(postid :String,currentIndex :Int)
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "postId":postid
            ]
            
            print(params)
            let url = BASE_URL + TURNOFFCOMMENTS
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                let indexPath = IndexPath(item: currentIndex, section: 0)
                                
                                let cell = self.newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell
                                
                                
                                var dict = NSMutableDictionary(dictionary: self.feedPosts[currentIndex] as! NSDictionary)
                                
                                
                                dict["commentingStatus"] = 0
                                
                                self.feedPosts[currentIndex] = dict
                                
                                self.newsFeedTblView.reloadRows(at: [indexPath], with: .fade)
                                
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        
                    }
            }
            
            
            
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
    }
    
    func turnOnComment(postid :String,currentIndex :Int)
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "postId":postid
            ]
            
            print(params)
            let url = BASE_URL + TURNONCOMMENTS
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {

                                let indexPath = IndexPath(item: currentIndex, section: 0)
                                
                                let cell = self.newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell
                                
                                
                                var dict = NSMutableDictionary(dictionary: self.feedPosts[currentIndex] as! NSDictionary)
                                
                                
                                dict["commentingStatus"] = 1
                                
                                self.feedPosts[currentIndex] = dict
                                
                                self.newsFeedTblView.reloadRows(at: [indexPath], with: .fade)
                                
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        
                    }
            }
            
            
            
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
    }
    
    func deletePost(postid :String,currentIndex :Int)
    {
            if Reachability.isConnectedToNetwork()
            {
                
                
                
                let accessToken = UserDefaults.standard.string(forKey: "accessToken")
                
                
                let params: Parameters = [
                    "accessToken": accessToken!,
                    "postId":postid
                ]
                
                print(params)
                let url = BASE_URL + DELETEPOST
                
                let Headers: HTTPHeaders = [
                    
                    "Content-Type":"application/x-www-form-urlencoded"
                ]
                
                Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                    .responseJSON
                    {
                        response in
                        
                        
                        if(response.result.isSuccess)
                        {
                            
                            
                            if let json = response.result.value
                            {
                                print("Account =  \(json)")
                                let jsonResponse = JSON(json)
                                
                                if(jsonResponse["error"].stringValue == "false" )
                                {
                                    let indexPath = IndexPath(item: currentIndex, section: 0)
                                    
                                    self.feedPosts.remove(at: currentIndex)
                                    
                                    self.newsFeedTblView.deleteRows(at: [indexPath], with: .fade)

                                    
                                    let contentOffset = self.newsFeedTblView.contentOffset
                                    self.newsFeedTblView.reloadData()
                                    self.newsFeedTblView.layoutIfNeeded()
                                    self.newsFeedTblView.setContentOffset(contentOffset, animated: false)

                                                                        
                                }
                                else
                                {
                                    
                                }
                            }
                        }
                        else
                        {
                            
                        }
                }
                
                
                
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
    }
    
    @objc func commentBtn(_ sender: AnyObject)
    {
       print("Comment Button Tag = ",sender.tag)

        currentTableIndex = sender.tag
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        vc.commentDelegate = self
        
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
            
        let id = String(describing: pID)
            
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""

        vc.proPic = profilePic
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
        videoPlayerTopView.isHidden = false
        videoPlayerMainView.isHidden = false
        
        self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.videoPlayerMainView.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
        */
    }
    
    
    @objc func likeBtn(sender: SparkButton)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            print("Like Button Tag = ",sender.tag)

            
            var dict = NSMutableDictionary()

            
            dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
            
    //        dict = self.feedPosts[sender.tag]as! NSDictionary


            print("Dict = %@",dict)
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                sender.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
                
                dict["user_liked"] = "false"
                
    //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                postUnLike(postId: id)
                
            }
            else
            {
                sender.setImage(UIImage(named: "heart_filled"), for: UIControlState())
                sender.unLikeBounce(0.4)
                
                dict["user_liked"] = "true"
                
    //            let like =  (dict["total_likes"] as! Int + 1)
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                postLike(postId: id)
            }
            
            
            let indexPath = IndexPath(item: sender.tag, section: 0)

             let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell
            
            self.feedPosts[sender.tag] = dict
            
            let like = (dict["total_likes"] as! Int)
            
            cell.likeLabel.text = String(describing: like)
            
            
            
            

            
        }
    }

    
    func pinRemovePost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + REMOVEFROMPINBORAD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func pinPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + ADDTOPINBOARD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
//        guard let tableViewCell = cell as? FeedTableViewCell else { return }
        
//        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
        
        
//         CellAnimator.animate(cell, withDuration: 1, animation: CellAnimator.AnimationType(rawValue: 5)!)
        
        let cell = cell as! FeedTableViewCell
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary

        
        cell.feedContentPosts  = (feedPosts["linkData"]as? Array)!
        
        
        cell.collectionVIew.reloadData()
//        cell.collectionVIew.contentOffset = .zero

        
        
        let lastcell = self.feedPosts.count - 1
        
        if indexPath.row == lastcell
        {
            if Total_Page_Count >= Current_page {
                Current_page = Current_page + 1
                print("CurrentPage\(Current_page)")
                
                UPDATE_FEED_API()
            }
        }
        
    }
    
    
    
    func UPDATE_FEED_API()
    {
        if Reachability.isConnectedToNetwork(){
            
           
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + FEEDS
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                

                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    
                                    let newFeed = JSON.object(forKey: "NewsPosts")as! Array<Any>
                                    
                                    
                                    if newFeed.count > 0
                                    {
                                        for var i in 0..<newFeed.count
                                        {
                                            let dict = newFeed[i] as! NSDictionary
                                            self.feedPosts.append(dict)
                                        }
                                        
                                    }
                                    

                                }
                                else
                                {
                                    self.feedPosts.removeAll()
                                }
                                
                                self.newsFeedTblView.reloadData()
                                
                                
                                
                                
                                
                             
                        /*        self.TVfeed.reloadData()
                                
                                
                                
                                
                                self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any> */
                                
                                
                                self.newsFeedTblView.reloadData()
                            }
                            else
                            {
/*                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                */
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
//                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
            }
            
            
            /*
            
            Alamofire.request(url, method: .post, parameters: parameters, headers: Headers)
                .responseJSON {[unowned self] response in
                    print(response)
                    //                self.stopAnimating()
                    if response.response == nil || response.result.isFailure  {
                        
                        let appearance = ToastView.appearance()
                        appearance.backgroundColor = .lightGray
                        appearance.textColor = .black
                        appearance.font = .boldSystemFont(ofSize: 16)
                        appearance.textInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
                        appearance.bottomOffsetPortrait = 100
                        appearance.cornerRadius = 20
                    }
                    else{
                        if let result = response.result.value
                        {
                            let JSON = result as! NSDictionary
                            print(JSON)
                            let errorString:String = JSON.value(forKey: "error") as! String
                            
                            if errorString == "false"
                            {
                                let success = JSON.value(forKey: "success")as! String
                                if success == "true"{
                                    let NewFeeds = JSON.value(forKey: "NewsPosts")as! Array<Any>
                                    self.Total_Page_Count = JSON.object(forKey: "pageCount")as! Int
                                    print(self.Total_Page_Count)
                                    if NewFeeds.count > 0
                                    {
                                        for var i in 0..<NewFeeds.count
                                        {
                                            let dict = NewFeeds[i] as! Dictionary<String,Any>
                                            self.feedPosts.append(dict)
                                        }
                                        
                                    }
                                    self.TVfeed.reloadData()
                                }
                                else {
                                    let message = JSON.value(forKey: "message")as! String
                                    
                                }
                                
                            }
                            else
                            {
                                let Message = JSON.value(forKey: "message")as! String
                                if Message == "Invalid Access Token" {
                                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SignInVC")as! SignInVC
                                    self.present(Dvc, animated: true, completion: nil)
                                    Toast(text: "This account has been logged into another device", duration: 3).show()
                                }
                                //                            self.stopAnimating()
                            }
                            
                        }
                        
                    }
            }*/
        }
        else{
//            Toast(text: "Check Internet Connenction", duration: 3).show()
        }
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
//        guard let tableViewCell = cell as? FeedTableViewCell else { return }
        
    }
    

    func loadFeeds()
    {
        Current_page = 1
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "page":Current_page
            ]
        
        print(params)
        let url = BASE_URL + FEEDS
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            if (jsonResponse["success"].stringValue == "true" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any>
                                
                                self.Total_Page_Count = JSON["pageCount"] as! Int
                            }
                            else
                            {
                                self.feedPosts.removeAll()
                            }
                            
                            self.newsFeedTblView.reloadData()
                            
                            self.reloadEmptyState()
                            
                            self.loadStoriesFeeds()
                        }
                        else
                        {
                         
                            KRProgressHUD.dismiss()

                            var flag = false
                            
                            if jsonResponse["message"].stringValue == "Invalid Access Token"
                            {
                                flag = true
                            }
                            else
                            {
                                flag = false
                            }
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                if flag
                                {
                                    self.backToLogin()
                                }
                                
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                else
                {
                    KRProgressHUD.dismiss()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    self.newsFeedTblView.reloadData()
                    self.reloadEmptyState()
                    
                }
        }
        
    }
    
    
    func loadStoriesFeeds()
    {
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!
        ]
        
        print(params)
        let url = BASE_URL + SHOWSTORIES
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                if(response.result.isSuccess)
                {
                    if let json = response.result.value
                    {
                        print("loadStoriesFeeds =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
//                            if (jsonResponse["success"].stringValue == "true" )
//                            {
                            
//                                let JSON = response.result.value as! NSDictionary
                            
                                self.storyData = JSON.object(forKey: "storyData")as! Array<Any>
                            
                            
                            
                            
//                            }
//                            else
//                            {
//                                self.storyData.removeAll()
//                            }
                            
                            self.storiesCollectionView.reloadData()
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.storyData.removeAll()
                    self.storiesCollectionView.reloadData()
                }
        }
    }
    
    
    
    func backToLogin()
    {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SplashViewController")as! SplashViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(Dvc, animated: true, completion: nil)
    }

    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func pullToloadFeeds()
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "page":1
        ]
        
        print(params)
        let url = BASE_URL + FEEDS
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
//                self.newsFeedTblView.cr.endHeaderRefresh()
                
                
                
                self.refreshControl.endRefreshing()

                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            if (jsonResponse["success"].stringValue == "true" )
                            {
                            
                                let JSON = response.result.value as! NSDictionary

                                
                                self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any>
                            
                            }
                            else
                            {
                                self.feedPosts.removeAll()
                            }
                            
                            self.newsFeedTblView.reloadData()
                            
                            self.newsFeedTblView.reloadData()
                            self.reloadEmptyState()
                            
                            self.loadStoriesFeeds()

                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                
                                self.feedPosts.removeAll()
                                self.newsFeedTblView.reloadData()
                                self.reloadEmptyState()
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    self.newsFeedTblView.reloadData()
                    self.reloadEmptyState()
                }
        }
    }
    
    
    func reportPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId,
            "reason":"not allowed"
        ]
        
        print(params)
        let url = BASE_URL + REPORT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return storyData.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoriesCollectionViewCell", for: indexPath) as! StoriesCollectionViewCell
        
        let feedPosts = self.storyData[indexPath.row]as! NSDictionary
        
        let imgString = feedPosts["userImage"]as? String ?? ""
        
        cell.storiesImg.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
        
        if indexPath.row == 0
        {
                cell.meImg.isHidden = false
        }
        else
        {
            cell.meImg.isHidden = true
        }
        
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let feedPosts = storyData[indexPath.row]as! NSDictionary
        
        var item = Array<Any>()
        
        item  = (feedPosts["data"]as? Array)!

        print("item = ",item)
        print("item = ",item.count)

        if item.count <= 0
        {            
            NotificationCenter.default.post(name: Notification.Name("MovePageViewController"), object: nil)
        }
        else
        {
            DispatchQueue.main.async
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContentView") as! ContentViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.pages = self.storyData
                vc.currentIndex = indexPath.row
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}

/*

extension NewsFeedViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    
    
    
//    func collectionView(_ collectionView: UICollectionView,
//                        didEndDisplaying cell: UICollectionViewCell,
//                        forItemAt indexPath: IndexPath)
//    {
//
//    }

    

    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        print("indexPath = ",indexPath.row)
        
//        let cell = collectionView.cellForItem(at: indexPath) as! FeedCollectionViewCell

//        cell.playerView.player?.pause()
        
        let cell = cell as? FeedCollectionViewCell
        
        cell?.playerView.player?.pause()

    }

    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        print("indexPath = ",indexPath.row)
        
        
//        let cell = cell as? FeedCollectionViewCell
//        
//        cell?.playerView.player?.play()

        
//        var cell = tableSingleChat.cellForRow(at: indexPath) as? SenderAudioCell
        
//        let cell = collectionView.cellForItem(at: indexPath) as! FeedCollectionViewCell

        
        
        
        
//        cell.playerView.player?.play()
        
        
        
//        UIView.animateWithDuration(0.6, animations: {
//
//            cell.alpha = 1
//
//        })
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
        
        
        var videoUrl = NSURL(string: "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8")

        let avPlayer = AVPlayer(url: videoUrl! as URL)
        
        cell.playerView?.playerLayer.player = avPlayer
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Tag =", collectionView.tag)
        print("ID =", indexPath.row)
        

//        let cell:FeedCollectionViewCell? = collectionView.cellForItem(at: indexPath) as? FeedCollectionViewCell
//
//
//        let fileUrl = URL(string: (cell?.videoUrl)!)
//
//        cell?.player = AVPlayer(url: fileUrl!)
//        cell?.playerLayer = AVPlayerLayer(player: cell?.player)
//        cell?.playerLayer?.frame = (cell?.imgView.frame)!
//        cell?.playerLayer?.frame.size.width = (cell?.imgView.frame.width)!
//
//        cell?.contentView.layer.addSublayer((cell?.playerLayer!)!)
//
//        cell?.player?.play()
        
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView,
//                        didEndDisplaying cell: UICollectionViewCell,
//                        forItemAt indexPath: IndexPath)
//    {
//
//
//
////        let cell:FeedCollectionViewCell? = collectionView.cellForItem(at: indexPath) as? FeedCollectionViewCell
////
////        cell?.player?.pause()
////        cell?.player?.replaceCurrentItem(with: nil)
////
////        cell?.contentView.layer.removeFromSuperlayer()
//
//
//    }

    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let _ = scrollView as? UITableView {
            print("tableview")
        }
        else if let _ = scrollView as? UICollectionView
        {
            print("collectionview")
            
            
        }
    }
    
}

extension NewsFeedViewController: UICollectionViewDelegateFlowLayout
{
    
}
*/

extension NewsFeedViewController: FeedTableViewCellDelegate
{
    func goToCommentVC()
    {
        
    }
    
    func goToProfileUserVC() {

    }
    
    func playVideo(url :String)
    {
        
        videoPlayerTopView.isHidden = false
        videoPlayerMainView.isHidden = false
        
/*        self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.videoPlayerMainView.transform = CGAffineTransform.identity
        },
        completion:
        {
                        Void in()
            
                        let firstVideoURL  = URL(string: url)                 //returns a valid URL
            
            
                        self.videoPlayer.videoURLs = [firstVideoURL!]
            
            
                        self.videoPlayer.videoPlayerControls.play()
            
            
        })*/
        
        
        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        
        
        self.videoPlayer.videoURLs = [firstVideoURL!]
        
        
        self.videoPlayer.videoPlayerControls.play()
        
//        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
    }
    
}


extension NewsFeedViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 5, height: collectionView.frame.size.width / 5)
    }
}

 


