//
//  FeedTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 01/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import AVFoundation
import ActiveLabel


protocol FeedTableViewCellDelegate
{
    func goToCommentVC()
    func goToProfileUserVC()
    func playVideo(url :String)

}

class FeedTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate
{
   
    override func prepareForReuse()
    {
        super.prepareForReuse()
    }
    
    var feedContentPosts = Array<Any>()
    
    

    @IBOutlet weak var yesCommentView: UIView!
    @IBOutlet weak var noCommentView: UIView!
    
    
    @IBOutlet weak var sponserView: UIView!
    @IBOutlet weak var sponserLabel: UILabel!    
    @IBOutlet weak var sponserBtn: UIButton!
    
    
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeBtn: SparkButton!
    @IBOutlet weak var postAgoLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var proImageView: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var bookmartBtn: SparkButton!
    @IBOutlet weak var reportBtn: UIButton!
    
    var delegate: FeedTableViewCellDelegate?
    
    
    
    @IBOutlet weak var viewProfileBtn: UIButton!
    

    
//    @IBOutlet weak var likeImageView: UIImageView!
//    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var collectionVIew: UICollectionView!
    
    @IBOutlet weak var desLabel: ActiveLabel!
    
    @IBOutlet weak var playImage: UIImageView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        /*
        
        let patterns = "\\s" + "is" + "\\b"
        
        
        let customType = ActiveType.custom(pattern: "\\sis\\b") //Looks for "are"
        
        //        let customType = ActiveType.custom(pattern: patterns)
        
        self.desLabel.enabledTypes.append(customType)
        
        
        self.desLabel.customColor[customType] = UIColor.black
        
        
        
        self.desLabel.text  = " This is a post with #multiple #hashtags and a @userhandle. Links are also supported like this one: http://optonaut.co. Now it also supports custom patterns -> are\n\n Let's trim a long link: \nhttps://twitter.com/twicket_app/status/649678392372121601"
        
        
        */

        
        
        
        
        proImageView.layer.cornerRadius = proImageView.frame.size.width / 2
        proImageView.layer.masksToBounds = true
        
        
       
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize(width: collectionVIew.frame.size.width, height: collectionVIew.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        
        self.collectionVIew?.collectionViewLayout = flowLayout
        
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commentImageView_TouchUpInside))
//        commentImageView.addGestureRecognizer(tapGesture)
//        commentImageView.isUserInteractionEnabled = true
        
//        let tapGestureForLikeImageView = UITapGestureRecognizer(target: self, action: #selector(self.likeImageView_TouchUpInside))
//        likeImageView.addGestureRecognizer(tapGestureForLikeImageView)
//        likeImageView.isUserInteractionEnabled = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
//    @objc func likeImageView_TouchUpInside() {
//      
//        //incrementLikes(forRef: postRef)
//        
//        likeImageView.image = UIImage(named: "likeSelected")
//
//    }
//    
    @objc func commentImageView_TouchUpInside()
    {
            delegate?.goToCommentVC()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.feedContentPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
        
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        
        
        let type = feedPosts["type"]as? String ?? ""
        
        
        if type == "image"
        {
            cell.playImage.isHidden = true
            
            let linkData = feedPosts["linkData"]as? String ?? ""
            
            cell.instaImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))

        }
        else
        {
            cell.playImage.isHidden = false
            
            let linkData = feedPosts["thumbnail"]as? String ?? ""
            
            cell.instaImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))

        }
        
        
        self.pageControl.numberOfPages = self.feedContentPosts.count

        
        
//        var videoUrl = NSURL(string: "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8")
//        
//        let avPlayer = AVPlayer(url: videoUrl! as URL)
//        
//        cell.playerView?.playerLayer.player = avPlayer
                
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if let _ = scrollView as? UITableView
        {
            print("tableview")
        }
        else if let _ = scrollView as? UICollectionView
        {
            print("collectionview")
            
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        print("End indexPath = ",indexPath.row)
        
        //        let cell = collectionView.cellForItem(at: indexPath) as! FeedCollectionViewCell
        
        //        cell.playerView.player?.pause()
        
//        let cell = cell as? FeedCollectionViewCell
////
//        cell?.playerView.player?.pause()
        
        
//        guard let videoCell = cell as? FeedCollectionViewCell else { return };
//
//        videoCell.playerView.player?.pause();
//        videoCell.playerView.player = nil;
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        print("Display indexPath = ",indexPath.row)

        
//        let cell = cell as? FeedCollectionViewCell
//
//        cell?.playerView.player?.play()
        
        
        
//        guard let videoCell = (cell as? FeedCollectionViewCell) else { return };
//        let visibleCells = collectionView.visibleCells;
//        let minIndex = visibleCells.startIndex;
//
////        print("collectionVIew.visibleCells.index(of: cell) = ",collectionView.visibleCells.index(of: cell))
//
//        if collectionView.visibleCells.index(of: cell) == minIndex
//        {
//            videoCell.playerView.player?.play();
//        }
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        let linkData = feedPosts["linkData"]as? String ?? ""
        
        let type = feedPosts["type"]as? String ?? ""
        
        
        if type == "video"
        {
            delegate?.playVideo(url: linkData)

        }
        
    }
    
    
    
    

}


extension FeedTableViewCell: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}



extension FeedTableViewCell
{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionVIew.delegate = dataSourceDelegate
        collectionVIew.dataSource = dataSourceDelegate
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize(width: collectionVIew.frame.size.width, height: collectionVIew.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        
        self.collectionVIew?.collectionViewLayout = flowLayout
        
        collectionVIew.tag = row
        collectionVIew.setContentOffset(collectionVIew.contentOffset, animated:false) // Stops collection view if it was scrolling.
        self.collectionVIew.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionVIew.contentOffset.x = newValue }
        get { return collectionVIew.contentOffset.x }
    }
    
}



