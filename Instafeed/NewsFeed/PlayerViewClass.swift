//
//  PlayerViewClass.swift
//  Instafeed
//
//  Created by Pyramidions on 03/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayerViewClass: UIView
{
    override static var layerClass: AnyClass
    {
        return AVPlayerLayer.self;
    }
    
    var playerLayer: AVPlayerLayer
    {
        return layer as! AVPlayerLayer;
    }
    
    var player: AVPlayer?
    {
        get
        {
            return playerLayer.player;
        }
        
        set
        {
            playerLayer.player = newValue;
        }
    }
}
