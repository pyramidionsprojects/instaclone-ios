//
//  AppDelegate.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import AWSS3
import AWSCore
import Firebase
import UserNotifications
import GoogleMaps
import GooglePlaces
import SocketIO
import SwiftyJSON


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate
{

    var window: UIWindow?
    var galleryReturn : Int = 0
    var pageControlHandle : Int = 0
    var socket : SocketIOClient!
    var manager : SocketManager!

    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        
//        GMSServices.provideAPIKey("AIzaSyD0xYGzVb49K2b_Ar0cVpC5OcYep8y0gKY")
//
//        GMSPlacesClient.provideAPIKey("AIzaSyD0xYGzVb49K2b_Ar0cVpC5OcYep8y0gKY")
        
        IAPHandler.shared.fetchAvailableProducts()

        
       
        
        
        
//        Messaging.messaging().isAutoInitEnabled = true

        
        GMSServices.provideAPIKey("AIzaSyB6XpezvzSPSSR5-A3GFayqfdLU-kSicXw")
        
        GMSPlacesClient.provideAPIKey("AIzaSyB6XpezvzSPSSR5-A3GFayqfdLU-kSicXw")

        
//        navigationController?.navigationBar.barTintColor = UIColor.white

        UINavigationBar.appearance().barTintColor = UIColor.white

        
        IQKeyboardManager.shared.enable = true
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        UITabBar.appearance().tintColor = .black

        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.APNortheast2,
                                                               identityPoolId:AWS_S3_POOL_ID)
        
        let configuration = AWSServiceConfiguration(region:.APNortheast2, credentialsProvider:credentialsProvider)
        
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration

        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
        
        let characterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString = deviceToken.description.trimmingCharacters(in: characterSet).replacingOccurrences(of: " ", with: "");
        
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    
    
    
    
  
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        let isLogIn = UserDefaults.standard.bool(forKey: isLoggedIn)
        if(isLogIn)
        {
            
            
            manager = SocketManager(socketURL: URL(string: BASE_URL)!, config: [.log(false), .compress, .forcePolling(true)])
            socket = manager.defaultSocket
            if(socket != nil)
            {
                
                
                var uid : String = ""
                
                
                if (UserDefaults.standard.object(forKey: userID) != nil)
                {
                    uid = UserDefaults.standard.object(forKey: userID) as! String
                }

                
                socket.on(clientEvent: .connect) {data, ack in
                    print("socket connected")
//                    let senderId = UserDefaults.standard.string(forKey: "userId")!
                    self.socket.emit("get_online", ["userid":uid])
                    self.socket.emit("isOnline", ["userid":uid])
                    
                }
                
                socket.on("recievemessage") {data, ack in
                    let response = JSON(data)
                    print("Recieve Message = ",response)
                    let dataDict:[String: Any] = ["data": data]
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageReceived"), object: self, userInfo: dataDict)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
                    
                }
                
            }
            socket.connect()
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        let isLogIn = UserDefaults.standard.bool(forKey: isLoggedIn)
        if(isLogIn)
        {
            
            var uid : String = ""
            
            if (UserDefaults.standard.object(forKey: userID) != nil)
            {
                uid = UserDefaults.standard.object(forKey: userID) as! String
                
                socket.emit("isOffline", ["userid":uid])

            }

//            let uid = UserDefaults.standard.string(forKey: "userId")
//            socket.emit("isOffline", ["userid":uid])
        }
        
        print("applicationWillTerminate")
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()

    }
    
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

   
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [START receive_message]
/*    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
       
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    */
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {

        // custom code to handle push while app is in the foreground

        print("Handle push from foreground = ",notification.request.content.userInfo)
        
        completionHandler(.alert)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
       
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
       
    }

}


