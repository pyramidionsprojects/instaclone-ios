//
//  ViewController.swift
//  AutoCompleteAddress
//
//  Created by Agus Cahyono on 2/23/16.
//  Copyright © 2016 Agus Cahyono. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class ViewController: UIViewController , UISearchBarDelegate ,GMSMapViewDelegate, LocateOnTheMap,GMSAutocompleteFetcherDelegate {
   
    
    var indicator: NVActivityIndicatorView!

    public func didFailAutocompleteWithError(_ error: Error) {
        //        resultText?.text = error.localizedDescription
    }

    public func didAutocomplete(with predictions: [GMSAutocompletePrediction])
    {
        for prediction in predictions {
            
            if let prediction = prediction as GMSAutocompletePrediction!{
                self.resultsArray.append(prediction.attributedFullText.string)
            }
        }
        self.searchResultController.reloadDataWithArray(self.resultsArray)
        print(resultsArray)
    }
    
    
    @IBOutlet weak var googleMapsContainer: UIView!
    
    
    var oldLatitude : Double = 0.0
    var oldLongitude : Double = 0.0
    var Address: String? = nil
    var Country: String? = nil
    var googleMapsView: GMSMapView!
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var gmsFetcher: GMSAutocompleteFetcher!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        //self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
        //self.view.addSubview(self.googleMapsView)
        
        //self.googleMapsView.delegate  = self;
        
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        
        
//        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
//        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
//        indicator = NVActivityIndicatorView(frame: frame, type: .ballSpinFadeLoader, color: redColor, padding: 0)
//        self.view.addSubview(indicator)
//        self.view.bringSubview(toFront: indicator)
        
        
    }
    

    @IBAction func searchWithAddress(_ sender: AnyObject)
    {
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
       self.present(searchController, animated:true, completion: nil)
    }
    
    
    func locateWithName(title: String)
    {
        let complteAddress = title
        
        
        UserDefaults.standard.set(complteAddress, forKey: locationValue)
        UserDefaults.standard.set("", forKey: "area")
        UserDefaults.standard.set("", forKey: "city")
        
        
        
        self.dismiss(animated: true, completion: nil)

    }
  
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String)
    {
        
        do
        {
            resultsArray.removeAll()
            
            resultsArray = [String]()
            
            DispatchQueue.main.async { () -> Void in
                
//                let position = CLLocationCoordinate2DMake(lat, lon)
//                let marker = GMSMarker(position: position)
//
//                let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
//                self.googleMapsView.camera = camera
                
                
                self.oldLatitude = lat
                self.oldLongitude = lon
                
//                marker.map = self.googleMapsView
                
                CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: lon))
                {
                    placemarks, error in
                    
                    var placemark:CLPlacemark
                    
                    placemark = (placemarks?.last)!
                    
                    

                    
                    
//                    marker.appearAnimation = kGMSMarkerAnimationPop
//                    marker.icon = UIImage(named: "map-pin")
//                    marker.isFlat = true
                    
                    
                    if ((placemark.subLocality) != nil)
                    {
                        self.Address =  (placemark.subLocality)
                    }
                    else
                    {
                         self.Address = ""
                    }

                    if ((placemark.locality) != nil)
                    {
                        self.Country = (placemark.locality)
                    }
                    else
                    {
                        self.Country = ""
                    }

                    
                    
                    let complteAddress = self.Address! + ", " + self.Country!
                    
                    
                    UserDefaults.standard.set(complteAddress, forKey: locationValue)
                    UserDefaults.standard.set(self.Address, forKey: "area")
                    UserDefaults.standard.set(self.Country, forKey: "city")
                
                    
                    
                    self.dismiss(animated: true, completion: nil)
                    
                    
                    
//                    self.googleMapsView.selectedMarker=marker
                    
                }
            }
        }
        catch
        {
            print("Swift try catch is confusing...")
        }
        
        
       
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.resultsArray.removeAll()
        gmsFetcher?.sourceTextHasChanged(searchText)
    }
    
/*    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        self.googleMapsView.clear()
        
        plotMarker(AtCoordinate: CLLocationCoordinate2D.init(latitude: coordinate.latitude, longitude: coordinate.longitude),onMapView: mapView)
    }

    private func plotMarker(AtCoordinate coordinate : CLLocationCoordinate2D, onMapView vwMap : GMSMapView) -> Void
    {
        
//        let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 10)
//        self.googleMapsView.camera = camera
        
        
        
        oldLatitude = coordinate.latitude
        oldLongitude = coordinate.longitude
        
 
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
        {
            placemarks, error in
            
            var placemark:CLPlacemark
            
                placemark = (placemarks?.last)!
//                var currentLocation = "\(placemark.locality),\(placemark.country)"
//                print("\(currentLocation.description)")
            
            
            let marker = GMSMarker(position: coordinate)
            marker.map = vwMap
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.icon = UIImage(named: "map-pin")
            marker.isFlat = true
            
            self.Address =  (placemark.locality)
            self.Country = (placemark.country)
            
//            marker.title = "Address : \(String(describing: self.Country))"
            //        marker.snippet = "My Location"
            self.googleMapsView.selectedMarker=marker
            
        }
        
//        geocoder.reverseGeocodeLocation(coordinate, completionHandler: {(_ placemarks: [Any]?, _ error: Error?) -> Void in
//            if error == nil && (placemarks?.count ?? 0) > 0 {
//                placemark = placemarks?.last
//                var currentLocation = "\(placemark.locality),\(placemark.country)"
//                print("\(currentLocation.description)")
//                locationManager.stopUpdatingLocation()
//            }
//        })
     
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView?
    {
//        let index:Int! = Int(marker.accessibilityLabel!)
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)![0] as! CustomInfoWindow
        
        customInfoWindow.mainView.layer.cornerRadius = 10
        customInfoWindow.mainView.layer.masksToBounds = true
        
        
        customInfoWindow.cityName.text = self.Country
        
        customInfoWindow.address.text = self.Address
        
        
//        customInfoWindow.architectLbl.text = self.Country
//        customInfoWindow.completedYearLbl.text = "Text"
        return customInfoWindow
        
        

    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker)
    {
        
        self.setLocation()
        
    }
    
    
    func setLocation()
    {
        
        self.indicator.startAnimating()
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        print(accessToken!)
        
       
        let params: Parameters = [
            "accessToken": accessToken!,
            "newLattitude":String(oldLatitude),
            "newLongitude":String(oldLongitude),
            "cityname":self.Address ?? "",
            "address":self.Country ?? "",
        ]
        
        print(params)
        let url = "\(Constants.baseURL)locationChange"
        Alamofire.request(url,method: .post, parameters:params).spin().responseJSON { response in
            
            if(response.result.isSuccess)
            {
                self.indicator.stopAnimating()
                
                
                if let json = response.result.value
                {
                    print("SET SETTINGS JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "false" )
                    {
//                        self.showAlert(title: "Success", msg: jsonResponse["message"].stringValue)
                        
                        self.navigationController?.popViewController(animated: true)

                        
                    }
                    else
                    {
//                        self.showAlert(title: "Oops", msg: jsonResponse["message"].stringValue)
                        
                        
                        let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                        
                        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
//                            print("You've pressed default");
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        
                       
                        alertController.addAction(action1)

                        self.present(alertController, animated: true, completion: nil)
                        
                        
                    }
                }
            }
            else
            {
                self.indicator.stopAnimating()

                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
            }
        }
    }
*/
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: GMSMapViewDelegate
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}



