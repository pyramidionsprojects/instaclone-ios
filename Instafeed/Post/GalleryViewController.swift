//
//  GalleryViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 27/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
import Photos
import ARSLineProgress
import AWSS3
import AWSCore
import Alamofire
import SwiftyJSON


class GalleryViewController: UIViewController {

    
    var selectedItems = [YPMediaItem]()

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?

    let imageView = UIImageView()
    var ProfileImgURL : String = ""

    var selectedAssets: [PHAsset] = []
    
    let button = UIButton()
    var imageURL : [URL] = []

    var imageCount : Int = -1

    var i = 1;
    
    var imageUploadCount = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearAllFilesFromTempDirectory()
        
    }
    
    func clearAllFilesFromTempDirectory()
    {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let diskCacheStorageBaseUrl = myDocuments.appendingPathComponent("insta")
        guard let filePaths = try? fileManager.contentsOfDirectory(at: diskCacheStorageBaseUrl, includingPropertiesForKeys: nil, options: []) else { return }
        for filePath in filePaths
        {
            try? fileManager.removeItem(at: filePath)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if appDelegate.galleryReturn == 1
        {
            
        }
        else
        {
           self.showPicker()
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        appDelegate.galleryReturn = 1
        
        ProfileImgURL = ""
        
        selectedAssets = []
        
        imageURL = []
        
        imageCount = -1
        
    }
    
    
    func showPicker()
    {
        
        // Configuration
        var config = YPImagePickerConfiguration()
        
        // Uncomment and play around with the configuration 👨‍🔬 🚀
        
        //        /// Set this to true if you want to force the  library output to be a squared image. Defaults to false
        //        config.onlySquareFromLibrary = true
        //
        //        /// Set this to true if you want to force the camera output to be a squared image. Defaults to true
        //        config.onlySquareImagesFromCamera = false
        //
        //        /// Ex: cappedTo:1024 will make sure images from the library will be
        //        /// resized to fit in a 1024x1024 box. Defaults to original image size.
        //        config.libraryTargetImageSize = .cappedTo(size: 1024)
        //
        //        /// Choose what media types are available in the library. Defaults to `.photo`
        config.libraryMediaType = .photoAndVideo
        //
        //        /// Enables selecting the front camera by default, useful for avatars. Defaults to false
        //        config.usesFrontCamera = true
        //
        //        /// Adds a Filter step in the photo taking process. Defaults to true
        //        config.showsFilters = false
        
        /// Manage filters by yourself
        //        config.filters = [YPFilterDescriptor(name: "Normal", filterName: ""),
        //                          YPFilterDescriptor(name: "Mono", filterName: "CIPhotoEffectMono")]
//        config.filters.remove(at: 1)
//        config.filters.insert(YPFilterDescriptor(name: "Blur", filterName: "CIBoxBlur"), at: 1)
        //
        //        /// Enables you to opt out from saving new (or old but filtered) images to the
        //        /// user's photo library. Defaults to true.
        config.shouldSaveNewPicturesToAlbum = false
        //
        //        /// Choose the videoCompression.  Defaults to AVAssetExportPresetHighestQuality
        //        config.videoCompression = AVAssetExportPreset640x480
        //
        //        /// Defines the name of the album when saving pictures in the user's photo library.
        //        /// In general that would be your App name. Defaults to "DefaultYPImagePickerAlbumName"
        //        config.albumName = "ThisIsMyAlbum"
        //
        //        /// Defines which screen is shown at launch. Video mode will only work if `showsVideo = true`.
        //        /// Default value is `.photo`
        config.startOnScreen = .library
        //
        //        /// Defines which screens are shown at launch, and their order.
        //        /// Default value is `[.library, .photo]`
        config.screens = [.library, .photo, .video]
        
        //
        //        /// Defines the time limit for recording videos.
        //        /// Default is 30 seconds.
        //        config.videoRecordingTimeLimit = 5.0
        //
        //        /// Defines the time limit for videos from the library.
        //        /// Defaults to 60 seconds.
        config.videoFromLibraryTimeLimit = 500.0
        //
        //        /// Adds a Crop step in the photo taking process, after filters. Defaults to .none
//        config.showsCrop = .rectangle(ratio: (16/9))
        //
        //        /// Defines the overlay view for the camera.
        //        /// Defaults to UIView().
        //        let overlayView = UIView()
        //        overlayView.backgroundColor = .red
        //        overlayView.alpha = 0.3
        //        config.overlayView = overlayView
        
        /// Customize wordings
        config.wordings.libraryTitle = "Gallery"
        
        /// Defines if the status bar should be hidden when showing the picker. Default is true
        config.hidesStatusBar = false
        
        config.maxNumberOfItems = 5
        
        config.showsFilters = true

        config.shouldSaveNewPicturesToAlbum = true

        // Here we use a per picker configuration. Configuration is always shared.
        // That means than when you create one picker with configuration, than you can create other picker with just
        // let picker = YPImagePicker() and the configuration will be the same as the first picker.
        let picker = YPImagePicker(configuration: config)
        
        /// Change configuration directly
        //        YPImagePickerConfiguration.shared.wordings.libraryTitle = "Gallery2"
        
        // Single Photo implementation.
        picker.didFinishPicking { items, cancelled in
            
            
            if cancelled
            {
                print("Picker was canceled")
                
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(Dvc, animated: true, completion: nil)
                
                
            }
            else
            {
            
            
            self.selectedItems = items
//            self.selectedImageV.image = items.singlePhoto?.image
            
            picker.dismiss(animated: true, completion: nil)

            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PostViewController")as! PostViewController
            
            Dvc.selectedItems = self.selectedItems

            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(Dvc, animated: true, completion: nil)
            
            
            }
            
        }
        
        // Single Video implementation.
        picker.didFinishPicking { items, cancelled in
            
            
            if cancelled
            {
                print("Picker was canceled")
                
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(Dvc, animated: true, completion: nil)
                
            }
            else
            {
            
            
            
            self.selectedItems = items
//            self.selectedImageV.image = items.singleVideo?.thumbnail
            
            picker.dismiss(animated: true, completion: nil)

            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PostViewController")as! PostViewController
            
            Dvc.selectedItems = self.selectedItems

            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(Dvc, animated: true, completion: nil)
            
            }
            
        }
        
        // Multiple implementation
        picker.didFinishPicking { items, cancelled in
            
            if cancelled
            {
                print("Picker was canceled")
                
                
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(Dvc, animated: true, completion: nil)
                
            }
            else
            {
            
            _ = items.map { print("🧀 \($0)") }
            
            self.selectedItems = items
/*            if let firstItem = items.first {
                switch firstItem {
                case .photo(let photo):
                    self.selectedImageV.image = photo.image
                case .video(let video):
                    self.selectedImageV.image = video.thumbnail
                }
            }*/
            picker.dismiss(animated: true, completion: nil)
            
            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PostViewController")as! PostViewController
            
           
            Dvc.selectedItems = self.selectedItems
            
            
            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(Dvc, animated: true, completion: nil)
            
            }
        }
        
        present(picker, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    
    private func fusumaDidFinishInMultipleMode()
    {
        print("self.selectedAssets.count = ",self.selectedAssets.count)
        
        
        
        
        
        for asset in self.selectedAssets
        {
            
            
            let serialQueue = DispatchQueue(label: "serialQueue")
            serialQueue.async {
                
            asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions())
            {
                (eidtingInput, info) in
                if let input = eidtingInput, let imgURL = input.fullSizeImageURL
                {
                    // imgURL
                    
                    
                    
                    print("imgURL = ",imgURL)
                    
                    self.imageURL.append(imgURL)
                    
                    
                    if self.i == self.selectedAssets.count
                    {
                        print("self.imageURL.count = ",self.imageURL.count)
                        
                        for var n in 0..<(self.imageURL.count)
                        {
                            self.uploadImageAWS(url: self.imageURL[n])
                        }
                    }
                    else
                    {
                        self.i = self.i + 1
                    }
                    
                    
//                    self.uploadImageAWS(url: imgURL)
                    
//                    ARSLineProgress.show()
                    

                    
                }
            }
            }
            
            
        }
        
        
        
    }
    
    
    
    func uploadImageAWS(url: URL)
    {
        
        let imgsData = NSData(contentsOf: url)

        let chosenImage : UIImage = UIImage(data: imgsData! as Data)!
        
        
        let imgData = UIImageJPEGRepresentation(chosenImage, 0.5)!

        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
                }
                
                if let _ = task.result
                {
                    
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    
                    if self.imageUploadCount == self.i
                    {
                        
                    }
                    else
                    {
                        self.imageUploadCount = self.imageUploadCount + 1
                    }
                    
                    
//                    ARSLineProgress.hide()

                    
                    print("ProfileImgURL = ",self.ProfileImgURL)
                    
                }
                
                return nil
        }
        
    }
    
    
    func uploadCameraImage(img: UIImage)
    {
      
            let fileManager = FileManager.default
        
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyyMMddHHmmssSSS"
            var strFileName = formatter.string(from: Date())
            strFileName = strFileName + ".jpg"
        
            let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(strFileName)")
        
            let imageData = UIImageJPEGRepresentation(img, 0.99)
        
            fileManager.createFile(atPath: path as String, contents: imageData, attributes: nil)
            
            let fileUrl = NSURL(fileURLWithPath: path)
            var uploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest?.bucket = BUCKET_NAME
            uploadRequest?.key = strFileName
            uploadRequest?.contentType = "image/jpeg"
            uploadRequest?.body = fileUrl as URL!
//            uploadRequest?.serverSideEncryption = AWSS3ServerSideEncryption.awsKms
            uploadRequest?.uploadProgress = { (bytesSent, totalBytesSent, totalBytesExpectedToSend) -> Void in
                DispatchQueue.main.async(execute:
                    {
                        
                        print("totalBytesExpectedToSend = ",totalBytesExpectedToSend)
                        
//                    self.amountUploaded = totalBytesSent // To show the updating data status in label.
//                    self.fileSize = totalBytesExpectedToSend
                })
            }
            
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(block: { (task: AWSTask) -> Any? in
            
            if let error = task.error
            {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil 
            {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent((uploadRequest?.bucket)!).appendingPathComponent((uploadRequest?.key)!)
                print("Uploaded to:\(String(describing: publicURL!))")
            }
            return nil
        })

        
    }
    /*{
        
        let imgData = UIImageJPEGRepresentation(img, 0.5)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                        
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
                }
                
                if let _ = task.result
                {
                    
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    
//                    ARSLineProgress.hide()
                    
                    
                    print("Single ProfileImgURL = ",self.ProfileImgURL)
                    
                    
                    //                        self.ProfileImgURL = ""
                    //
                    //                        self.indicator.stopAnimating()
                    //
                    //                        self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    //
                    //                        print("Send Image = ",self.ProfileImgURL)
                    //
                    //                        self.uploadImage()
                }
                
                return nil
        }
        
    }*/*/
    
}
