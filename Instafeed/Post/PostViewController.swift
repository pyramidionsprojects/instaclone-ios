//
//  PostViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 30/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Photos
import KMPlaceholderTextView
import ARSLineProgress
import AWSS3
import AWSCore
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import CoreLocation
import YPImagePicker


class PostViewController: UIViewController,NVActivityIndicatorViewable,UITextViewDelegate,CLLocationManagerDelegate
{
    var addTagType = ""
    
    var selectedItems = [YPMediaItem]()

    
    var newHashTag = Array<Any>()
    
    @IBOutlet weak var hashTopView: UIView!
    @IBOutlet weak var hashTagTableView: UITableView!
    @IBOutlet weak var hasgTagView: UIView!
    
    @IBOutlet weak var locationAboveView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    var lat : Double = 0.0
    var lang : Double = 0.0
    
    var area : String = ""
    var city : String = ""
    
    
    var groupHashTags = Array<Any>()

    
    var commentingStatus = "1"
    
    
    var sendData = Array<Any>()
    
    var hashTagsValues = Array<Any>()
    
    var captionTags = Array<Any>()

    var indicator: NVActivityIndicatorView!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var postTxtView: KMPlaceholderTextView!
    
    var getAssets: [PHAsset] = []

    var videoValues : [Data] = []
    var videoSingle = Data()

    
    var photoCamera = UIImage()
    var typeSource : Int = 0
    
    var ProfileImgURL : String = ""
    
    var selectedAssets: [PHAsset] = []
    
    var imageValues : [UIImage] = []
    
    var imageURL : [URL] = []
    
    var imageCount : Int = -1

    var i = 1;
    
    var imageUploadCount = 1
    
    
    var linkData : [String] = []
    
    
    var globalVal : Int = 0

    
    let locationManager = CLLocationManager()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.hashTopView.isHidden = true
        self.hasgTagView.isHidden = true
        

        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()
        
/*        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        */
        
        
        
        
//        clearAllFilesFromTempDirectory()
        
        hashTagTableView.separatorStyle = UITableViewCellSeparatorStyle.none

        
        postTxtView.delegate = self
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        
        
        let screenSize: CGSize = UIScreen.main.bounds.size
        
        let targetSize = CGSize(width: screenSize.width, height: screenSize.height)
        
        
        let options = PHImageRequestOptions()
        options.resizeMode = PHImageRequestOptionsResizeMode.exact
        options.deliveryMode = .highQualityFormat
        options.isNetworkAccessAllowed = true
        options.resizeMode = .exact
        
        
        
        let isLoc = UserDefaults.standard.object(forKey: locationValue)
        
        if isLoc == nil
        {
            locationAboveView.isHidden = true;
        }
        else
        {
            locationAboveView.isHidden = false
        }
        
        if let firstItem = self.selectedItems.first
        {
            switch firstItem
            {
                case .photo(let photo):
                
                    self.imgView.image = photo.image

                
                case .video(let video):
                
                    self.imgView.image = video.thumbnail

            }
        }
        
        
        
        
    }
    
    
    
    func clearAllFilesFromTempDirectory()
    {
            let fileManager = FileManager.default
            let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
            let diskCacheStorageBaseUrl = myDocuments.appendingPathComponent("insta")
            guard let filePaths = try? fileManager.contentsOfDirectory(at: diskCacheStorageBaseUrl, includingPropertiesForKeys: nil, options: []) else { return }
            for filePath in filePaths
            {
                    try? fileManager.removeItem(at: filePath)
            }
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        
//        hasgTagView.layer.cornerRadius = 10
        
        // border
        hasgTagView.layer.borderWidth = 1.0
        hasgTagView.layer.borderColor = UIColor.gray.cgColor
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    
    

    
   
        
        
        
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let isLoc = UserDefaults.standard.object(forKey: locationValue)
        
        if isLoc == nil
        {
            locationAboveView.isHidden = true;
        }
        else
        {
            locationAboveView.isHidden = false
            
            self.locationLabel.text = UserDefaults.standard.object(forKey: locationValue) as? String
            
            
            self.area = (UserDefaults.standard.object(forKey: "area") as? String)!

            self.city = (UserDefaults.standard.object(forKey: "city") as? String)!
            
        }
        
    }

    
    
    
   
        
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func back(_ sender: Any)
    {
        
        self.appDelegate.galleryReturn = 0
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(Dvc, animated: true, completion: nil)
        
    }
    
    
    
    func saveFiles()
    {
        
        
        if self.selectedItems.count == globalVal
        {
            self.postImage()
        }
        else
        {
            let firstItem = self.selectedItems[globalVal]
            
            switch firstItem
            {
            case .photo(let photo):
                
                let myDict:NSDictionary = ["type" : "image","data":"","thumbnail":""]
                
                sendData.append(myDict)
                
                
                self.uploadImage(chosenImage: photo.image)
                
                break
                
            case .video(let video):
                
                
                let myDict:NSDictionary = ["type" : "video","data":"","thumbnail":""]
                
                sendData.append(myDict)
                
                self.uploadVideo(url: video.url, image: video.thumbnail)
                
            }
        }
        
        
        
        
        
    }
    
    
    
    
    
    
   

    func uploadVideo(url :URL , image :UIImage)
    {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var filename = formatter.string(from: Date())
        filename = filename + ".mp4"
        
        let transferManager = AWSS3TransferManager.default()

        let uploadingFileURL = url//URL(fileURLWithPath: String(describing: fileURL))
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        
        uploadRequest?.bucket = BUCKET_NAME
        uploadRequest?.key = filename
        uploadRequest?.body = uploadingFileURL
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as? NSError
            {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                        case .cancelled, .paused:
                            break
                        default:
                            print("Error uploading: \(uploadRequest?.key) Error: \(error)")
                        
                        KRProgressHUD.dismiss()
                        
                        transferManager.cancelAll()
                        
                        
                        
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key) Error: \(error)")
                    
                    KRProgressHUD.dismiss()
                    
                    transferManager.cancelAll()

                }
                return nil
            }
            
            let uploadOutput = task.result
            
            
            let url = IMAGES_BUCKET + (uploadRequest?.key)!
            
            var dict = NSMutableDictionary()
            
            dict = NSMutableDictionary(dictionary: self.sendData[self.globalVal] as! NSDictionary)
            
            dict["data"] = url
            
            
            self.sendData[self.globalVal] = dict
            
            
            self.uploadThumbnail(chosenImage: image)

            
            return nil
        })
        
    }
    
    
    func uploadThumbnail(chosenImage: UIImage)
    {
        
        
        let imgData = UIImageJPEGRepresentation(chosenImage, 1)!
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
                }
                
                if let _ = task.result
                {
                    
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    
                    self.linkData.append(self.ProfileImgURL)
                    
                    var dict = NSMutableDictionary()
                    
                    
                    dict = NSMutableDictionary(dictionary: self.sendData[self.globalVal] as! NSDictionary)
                    
                    
                    dict["thumbnail"] = self.ProfileImgURL
                    
                    
                    self.sendData[self.globalVal] = dict
                    
                    
                    print("Thumbnail URL = ",self.ProfileImgURL)
                    
                    
                    self.globalVal = self.globalVal + 1
                    
                    self.saveFiles()
                    
                }
                
                return nil
        }
        
    }
    
   
    func uploadImage(chosenImage: UIImage)
    {
        
        
        let imgData = UIImageJPEGRepresentation(chosenImage, 1)!
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
                }
                
                if let _ = task.result
                {
                    
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    
                    self.linkData.append(self.ProfileImgURL)
                    
                    var dict = NSMutableDictionary()
                    
                    
                    dict = NSMutableDictionary(dictionary: self.sendData[self.globalVal] as! NSDictionary)
                    
                    
                    dict["data"] = self.ProfileImgURL
                    
                    
                    self.sendData[self.globalVal] = dict
                    
                    
                    print("ProfileImgURL = ",self.ProfileImgURL)
                    
                    
                    self.globalVal = self.globalVal + 1
                    
                    self.saveFiles()
                    
                }
                
                return nil
        }
        
    }
    
    
    
    
    @IBAction func postAtn(_ sender: Any)
    {
        
        if postTxtView.text != ""
        {

                KRProgressHUD.show(withMessage: "Posting...")

            
            
                sendData.removeAll()
                
                
                self.saveFiles()
            
                print(" = ",groupHashTags)
                
                let textviewtext = postTxtView.text
                
                let hash = textviewtext?.components(separatedBy: " ")
                
                
                var hashAll = [String]()
                
                for h in hash!
                {
                    if h.range(of:"#") != nil
                    {
                        print("exists")
                        hashAll.append(h)
                    }
                    else
                    {
                    }

                }
                
                
                let skippedArray = (hashAll as AnyObject as! NSArray).mutableCopy()

            
                print(" Before skippedArray = ",skippedArray)
                print(" Before hashAll = ",hashAll)

            
            
                for i in 0..<groupHashTags.count
                {
                    
                    for j in 0..<hashAll.count
                    {
                        if groupHashTags[i] as! String == hashAll[j]
                        {
                            (skippedArray as AnyObject).remove(groupHashTags[i])
                            break
                        }
                    }
                    
                }
                
               print(" skippedArray = ",skippedArray)
            
                var tempHash = Array<Any>()
                
                tempHash = skippedArray as! Array
                
                for ar in tempHash
                {
                    
                    
                    print("New tag = ",ar)
                    
                    let myDict:NSDictionary = ["new" : ar]
                    
                    newHashTag.append(myDict)
                    
                }

                

                print("newHashTag = ",newHashTag)
                
  
                
//            }
            
            
            
        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message:"Enter write a caption", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    
    
   
    
    
    
    
    func postImage()
    {
        
        
        print("Send Array = ",self.sendData)
        
        let data = try? JSONSerialization.data(withJSONObject: self.sendData, options: [])
        let linkDataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")

        
        var captionTagsString : NSString = "[]"
        
        
        
        print("self.captionTags = ",self.captionTags)
        
        let data1 = try? JSONSerialization.data(withJSONObject: self.captionTags, options: [])
        captionTagsString = NSString(data: data1!, encoding: String.Encoding.utf8.rawValue)!
        
        
        var newHash : NSString = "[]"

        if self.newHashTag.count == 0
        {
            
        }
        else
        {
            let data2 = try? JSONSerialization.data(withJSONObject: self.newHashTag, options: [])
            newHash = NSString(data: data2!, encoding: String.Encoding.utf8.rawValue)!
        }
        
            
        let params: Parameters = [
                "accessToken": accessToken!,
                "contentType":"image",
                "hashTag":"[]",
                "linkData":linkDataString!,
                "tagUsers":"[]",
                "caption":postTxtView.text!,
                "captionTags":captionTagsString,
                "commentingStatus":commentingStatus,
                "latitude":String(format:"%f", lat),
                "longitude":String(format:"%f", lang),
                "area":self.area,
                "city":self.city,
                "newHashTag":newHash
            ]
            
            print(params)
            let url = BASE_URL + NEWPOST
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                self.appDelegate.galleryReturn = 0
                                
                                
                                
                                let prefs = UserDefaults.standard
                                prefs.removeObject(forKey: locationValue)

                                prefs.removeObject(forKey: "area")
                                prefs.removeObject(forKey: "city")


                                
                                
                                self.locationAboveView.isHidden = true
                                
                                
                                
                                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                self.present(Dvc, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                
                                //                            self.indicator.stopAnimating()
                                
                                let prefs = UserDefaults.standard
                                prefs.removeObject(forKey: locationValue)
                                
                                prefs.removeObject(forKey: "area")
                                prefs.removeObject(forKey: "city")

                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        //                    self.indicator.stopAnimating()
                        
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
            }
        
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
   
    
    
    @IBAction func addLocation(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "NavViewController")as! NavViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func clearLocation(_ sender: Any)
    {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: locationValue)
        
        self.locationAboveView.isHidden = true
        
        prefs.removeObject(forKey: "area")
        prefs.removeObject(forKey: "city")
    }
    
   
    
    func textViewDidChangeSelection(_ textView: UITextView)
    {
        let str = textView.text
        if str != ""
        {
//            let lastChar = str![(str?.endIndex)!]
            let lastChar = str![(str?.index((str?.endIndex)!, offsetBy: -1))!]   // "e"
            
            if lastChar == "#"
            {
//                self.hashTopView.isHidden = false
//
//                self.hasgTagView.isHidden = false
                
                
                
                UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hashTopView.isHidden = false
                })
                
                
                UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hasgTagView.isHidden = false
                })
                
            }
        }
        else
        {
            self.hasgTagView.isHidden = true
        }
        
        let textviewtext = textView.text

        let hash = textviewtext?.components(separatedBy: "#")
        
        let mentions = textviewtext?.components(separatedBy: "@")

        let space = textviewtext?.components(separatedBy: " ")

        
        print("Last = ",(hash?.last!)!)
        print("mentions = ",(mentions?.last!)!)
        print("space = ",(space?.last!)!)

        
        
        if hash?.last!.range(of:"#") != nil
        {
            print("Hash = ",(hash?.last!)!)
            
        }
        else if mentions?.last!.range(of:"@") != nil
        {
            print("Mention = ",(mentions?.last!)!)
            
//            self.hasgTagView.isHidden = false

        }
        else
        {
            print("Space = ",(space?.last!)!)
            
            
            
            let string : String = (space?.last!)!
            
            if string.range(of:"#") != nil
            {
                
                addTagType = "hash"
                
                self.searchHashTags(hash: string)
                
                
                hashTagTableView.reloadData()

//                self.hashTopView.isHidden = false
//
//                self.hasgTagView.isHidden = false
                
                
                UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hashTopView.isHidden = false
                })
                
                
                UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hasgTagView.isHidden = false
                })
                


            }
            else if string.range(of:"@") != nil
            {
                
                addTagType = "mention"
                
                let outputString =  String(string.characters.dropFirst())

                
                self.searchUserTags(hash: outputString)
                
                
                hashTagTableView.reloadData()
                
//                self.hashTopView.isHidden = false
//
//                self.hasgTagView.isHidden = false
                
                
                UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hashTopView.isHidden = false
                })
                
                
                UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hasgTagView.isHidden = false
                })
                
                
            }
            else
            {
                addTagType = ""
                
                UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hashTopView.isHidden = true
                })


                UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                    {
                        self.hasgTagView.isHidden = true
                })
                
            }
            

        }
        
        
        //.componentsSeparatedByString("#")
//        arrSearched = arrValues.filter({$0.lowercaseString.containsString(new.last!.stringByReplacingOccurrencesOfString("#", withString: "").lowercaseString)})
//        hashTagTableView.reloadData()
    }
    
    
    func searchHashTags(hash :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let params: Parameters = [
                "tagName":hash
            ]
            
            print(params)
            let url = BASE_URL + HASHTAGSEARCH
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                self.hashTagsValues = JSON.object(forKey: "hashTags")as! Array<Any>

                                print("Hash Response = ",JSON)
                                
                                
                                self.hashTagTableView.reloadData()
                                
                            }
                            else
                            {
                             
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                    }
            }
        }
        else{
        }
    }
    
    
    
    func searchUserTags(hash :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")

            let params: Parameters = [
                "accessToken":accessToken!,
                "tagName":hash
            ]
            
            print(params)
            let url = BASE_URL + TAGUSERSEARCH
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                self.hashTagsValues = JSON.object(forKey: "tagusers")as! Array<Any>
                                
                                print("Hash Response = ",JSON)
                                
                                self.hashTagTableView.reloadData()

                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                    }
            }
        }
        else
        {
            
        }
    }
    
    
    @IBAction func turnOffComBtn(_ sender: UISwitch)
    {
        if sender.isOn
        {
            commentingStatus = "1"
        }
        else
        {
            commentingStatus = "0"
        }

    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude
        lang = locValue.longitude
        self.getLocationAddress()
        
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
        
    }
    
    
    func getLocationAddress()
    {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: lang))
        {
            placemarks, error in
            var placemark:CLPlacemark
            placemark = (placemarks?.last)!
//            let currentLocation = "\(String(describing: placemark.locality!)),\(String(describing: placemark.country!))"
            
            self.area = (String(describing: placemark.subLocality!))
            self.city = (String(describing: placemark.locality!))
        }
    }
    
    
}


extension PostViewController: UITableViewDataSource,UITableViewDelegate
{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if hashTagsValues.count == 0
        {
            
            UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                {
                    self.hashTopView.isHidden = true
            })
            
            
            UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                {
                    self.hasgTagView.isHidden = true
            })
            
            
        }
       
        
        return hashTagsValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HashTagsTableViewCell", for: indexPath) as! HashTagsTableViewCell
        
        
        cell.selectionStyle = .none

        let hashTags = self.hashTagsValues[indexPath.row]as! NSDictionary
        
        
        if addTagType == "hash"
        {
            cell.hashTagView.isHidden = false
            cell.mentionView.isHidden = true
            
            let tagname = hashTags["tagName"]as? String ?? ""
            let count = hashTags["post_count"]as? Int

            cell.hashCount.text = String(describing: count!) + " " + "Post"
            
            cell.hashTagLabel.text = tagname            
        }
        else if addTagType == "mention"
        {
            cell.hashTagView.isHidden = true
            cell.mentionView.isHidden = false

            cell.mentionNameLbl.text = hashTags["name"]as? String ?? ""
            cell.mentionUserNameLbl.text = hashTags["username"]as? String ?? ""
        }

        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 100.0;//Choose your custom row height
//    }

    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let hashTags = self.hashTagsValues[indexPath.row]as! NSDictionary
        
        
        if addTagType == "hash"
        {
        
            let tagname = hashTags["tagName"]as? String ?? ""
            
            
//            let tag = "#" + tagname

            
            let outputString =  String(tagname.characters.dropFirst())

//            self.hashTopView.isHidden = true
//
//            self.hasgTagView.isHidden = true
            
            UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
            {
                    self.hashTopView.isHidden = true
            })
            
            
            UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
            {
                    self.hasgTagView.isHidden = true
            })
            
            
            let textViewTmp = postTxtView.text
            
            let space = textViewTmp?.components(separatedBy: "#")
            
            var strNewText = space
            
            if strNewText!.count > 1
            {
                strNewText![(strNewText?.count)! - 1] = outputString
            }
            
            let strFinalText = strNewText?.joined(separator: "#")
            
            print("strFinalText = ",strFinalText!)
            
            
            groupHashTags.append(tagname)
            
            
            postTxtView.text = strFinalText! + " "
            
            let id = hashTags["id"] as! Int
            
            let ids = String(describing: id)
            
            let dict = ["id" : ids, "type" : "hash"]
            
            self.captionTags.append(dict)
            
            
        }
        else if addTagType == "mention"
        {
            var tagname = hashTags["username"]as? String ?? ""
            
            tagname = "@" + tagname
            
//            let outputString =  String(tagname.characters.dropFirst())
            
            
            
            UIView.transition(with: self.hashTopView, duration: 0.5, options: .transitionCrossDissolve, animations:
                {
                    self.hashTopView.isHidden = true
            })
            
            
            UIView.transition(with: self.hasgTagView, duration: 0.5, options: .transitionCrossDissolve, animations:
                {
                    self.hasgTagView.isHidden = true
            })
            
            
//            self.hashTopView.isHidden = true
//
//            self.hasgTagView.isHidden = true
            
            let textViewTmp = postTxtView.text
            
            let space = textViewTmp?.components(separatedBy: "@")
            
            var strNewText = space
            
            if strNewText!.count > 1
            {
                strNewText![(strNewText?.count)! - 1] = tagname
            }
            
            let strFinalText = strNewText?.joined(separator: "@")
            
            
            groupHashTags.append(tagname)

            
            print("strFinalText = ",strFinalText!)
            
            postTxtView.text = strFinalText! + " "
            
//            let id = hashTags["id"]as? String ?? ""

            let id = hashTags["id"] as! Int
            
            let ids = String(describing: id)

            
            let dict = ["id" : ids, "type" : "user"]
            
            self.captionTags.append(dict)

            
        }
        
        self.hashTagsValues.removeAll()
        
    }

    
}

