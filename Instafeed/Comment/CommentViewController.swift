//
//  CommentViewController.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/1/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage
import Toast_Swift

protocol commentCountChangeDelegate
{
    func commentCount()
}

class CommentViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintToBottom: NSLayoutConstraint!
    @IBOutlet weak var proImage: UIImageView!
    
    var commentDelegate: commentCountChangeDelegate?

    var proPic : String = ""
    
    var postID : String = ""
    
    var maintainStatus : String = ""

    var commentID : String = ""
    
    var showComments = Array<Any>()

    //    var postId: String!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        title = "Comment"
//        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 77
        tableView.rowHeight = UITableViewAutomaticDimension
        empty()
        handleTextField()
//        loadComments()
        
//      self.enumerateFonts()
        
        self.proImage.sd_setImage(with: URL(string: proPic), placeholderImage: UIImage(named: "default-user-image"))

        loadComments()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func enumerateFonts()
    {
        for fontFamily in UIFont.familyNames
        {
            print("Font family name = \(fontFamily as String)")
            for fontName in UIFont.fontNames(forFamilyName: fontFamily as String)
            {
                print("- Font name = \(fontName)")
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        print(notification)
        let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        UIView.animate(withDuration: 0.3) {
            self.constraintToBottom.constant = keyboardFrame!.height
            self.view.layoutIfNeeded()

        }
    }
    @objc func keyboardWillHide(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            self.constraintToBottom.constant = 0
            self.view.layoutIfNeeded()
            
        }
    }
    
    
    func handleTextField() {
        commentTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControlEvents.editingChanged)
    }
    
    @objc func textFieldDidChange() {
        if let commentText = commentTextField.text, !commentText.isEmpty {
            sendButton.setTitleColor(UIColor.black, for: UIControlState.normal)
            sendButton.isEnabled = true
            return
        }
        sendButton.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
        sendButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.post(name: Notification.Name("disableSwipeGesture"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    @IBAction func sendButton_TouchUpInside(_ sender: Any)
    {
        
        if self.commentTextField.text != ""
        {
            if Reachability.isConnectedToNetwork()
            {
                
                if maintainStatus == ""
                {
                    
                    commentDelegate?.commentCount()
                    postComments()
                }
                else
                {
                        maintainStatus = ""
                        postReplyComments()
                }
                
                self.empty()
                self.view.endEditing(true)

            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
        }
        else
        {
            
        }
        
        
//        postComments()
    }
    
    func empty()
    {
        self.commentTextField.text = ""
        self.sendButton.isEnabled = false
        sendButton.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Comment_ProfileSegue"
        {
//            let profileVC = segue.destination as! ProfileUserViewController
//            let userId = sender  as! String
//            profileVC.userId = userId
        }
    }
    
    
    
    func postReplyComments()
    {
        
        KRProgressHUD.show(withMessage: "Posting...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "commentId":commentID,
            "replyData":self.commentTextField.text!
        ]
        
        print(params)
        let url = BASE_URL + REPLYES
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            self.loadComments()
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    func postComments()
    {
        
        KRProgressHUD.show(withMessage: "Posting...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "Postsid":postID,
            "comments":self.commentTextField.text!
        ]
        
        print(params)
        let url = BASE_URL + COMMENT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            self.loadComments()
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    func loadComments()
    {
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters =
        [
            "accessToken": accessToken!,
            "postId":postID
        ]
        
        print(params)
        let url = BASE_URL + SHOWCOMMENTS
        
//        let url = "http://139.59.43.169:3013/showComments"
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                if(response.result.isSuccess)
                {

                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            self.showComments = JSON.object(forKey: "PostComments")as! Array<Any>
                            
                            print("showComments = ",self.showComments)
                            
                            self.tableView.reloadData()
                            
                            let scrollPoint = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
                            self.tableView.setContentOffset(scrollPoint, animated: true)

                            DispatchQueue.main.async
                            {
                                let indexPath = IndexPath(row: self.showComments.count-1, section: 0)
                                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                            }
                            
                        }
                        else
                        {
                            self.view.makeToast(jsonResponse["message"].stringValue)
                            
/*                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)*/
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func viewProfileBtn(_ sender: AnyObject)
    {
        let dict = self.showComments[sender.tag] as! NSDictionary
        
        
        let proName : String = dict["username"]as? String ?? ""
        

        var proStatus = "true"
        
        
        if (UserDefaults.standard.object(forKey: username) != nil)
        {
            let pro : String = UserDefaults.standard.object(forKey: username) as! String
            
            if proName == pro
            {
                proStatus = "true"
            }
            else
            {
                proStatus = "false"
            }
            
            if proStatus == "false"
            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
                vc.userName = dict["username"]as? String ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                NotificationCenter.default.post(name: Notification.Name("changeTabbarItem"), object: nil)
            }
        }
    }
    
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
        
        let rect2 = UIScreen.main.bounds.size // Returns CGSize

        let feedPosts = self.showComments[indexPath.row]as! NSDictionary
        
        print("Height feedPosts = ",feedPosts)
        
        var feedContentPosts = Array<Any>()
        
        feedContentPosts  = (feedPosts["comment_replyes"]as? Array)!
        
        let comment = feedPosts["comment"]as? String ?? ""
        
        var height :CGFloat = 0.0

        var sizes :CGFloat = 0.0

     
        for i in (0..<feedContentPosts.count)
        {
            
            let feedPosts = feedContentPosts[i] as! NSDictionary

            
            let comment = feedPosts["reply"]as? String ?? ""

            
            let contentNSString = comment as String
            
            
           height = contentNSString.height(withConstrainedWidth: rect2.width * 0.6, font: UIFont(name: "Poppins-Medium", size: 15.0)!) + 62
            
            
            print("feedContentPosts = ",height)

        }
 

         if feedContentPosts.count > 0
         {
            
            let contentNSString = comment as String
            
            sizes = contentNSString.height(withConstrainedWidth: rect2.width * 0.7, font: UIFont(name: "Poppins-Medium", size: 15.0)!) + 52

            let size :CGFloat = height * (CGFloat(feedContentPosts.count)) + sizes
            
            print("Height = ",height)
            print("Sizes = ",sizes)
            print("Size = ",size)

            return size
         }
         else
         {
            return UITableViewAutomaticDimension
         }
        
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return showComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
        
        
        let showComment = self.showComments[indexPath.row]as! NSDictionary
        
        
        let pID = (showComment["commentId"] as! Int)
        
        let id = String(describing: pID)
        
        cell.commentId = id
        
        cell.postid = postID
        
        
        let imgString = showComment["profilePic"]as? String ?? ""
        
        cell.profileImageView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
        
        if showComment.object(forKey: "user_liked_status") as! String == "true"
        {
            cell.likeButton.setImage(UIImage(named: "heart_filled"), for: UIControlState())
            
        }
        else
        {
            cell.likeButton.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        
        cell.likeButton.addTarget(self, action: #selector(CommentViewController.likeBtn(sender:)), for: .touchUpInside)
        
        cell.likeButton.tag = indexPath.row
        
        cell.selectionStyle = .none
        
        cell.viewProfileBtn.addTarget(self, action: #selector(CommentViewController.viewProfileBtn(_:)), for: .touchUpInside)
        
        cell.viewProfileBtn.tag = indexPath.row

        
//        cell.replyBtn.addTarget(self, action: #selector(CommentViewController.replysBtn(_:)), for: .touchUpInside)
//
//        cell.replyBtn.tag = indexPath.row

        
        cell.replyBtn.addTarget(self, action: #selector(CommentViewController.replysBtn(sender:)), for: .touchUpInside)
        
        cell.replyBtn.tag = indexPath.row
        
        let likeCnt = showComment["comment_likes"]as! Int
        
        if likeCnt == 0
        {
            cell.likeCount.text = ""
        }
        else
        {
            cell.likeCount.text = String(describing: likeCnt) + " Like"
        }
        
        let name = showComment["name"]as? String ?? ""
        
        let comment = showComment["comment"]as? String ?? ""
        
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: name, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Medium", size: 15.0)!])
        
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:name.count))
        
        myMutableString.append(NSAttributedString(string: " "))
        myMutableString.append(NSAttributedString(string: comment))
        
        cell.commentLabel.attributedText = myMutableString
        
        
        let currentDate = showComment["commentedAt"]as? String
        
        cell.commentAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let cell = cell as! CommentTableViewCell
        
        let feedPosts = self.showComments[indexPath.row]as! NSDictionary
        
        cell.feedContentPosts  = (feedPosts["comment_replyes"]as? Array)!
        
        cell.collectionVIew.reloadData()
    }
    
    
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    

    @objc func replysBtn(sender: AnyObject)
    {
        let showComment = self.showComments[sender.tag]as! NSDictionary
        
        let pID = (showComment["commentId"] as! Int)
        
        let id = String(describing: pID)
        
        commentID = id
        
        maintainStatus = "1"
        
        self.commentTextField.becomeFirstResponder()
    }
    
    @objc func likeBtn(sender: SparkButton)
    {
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            print("Like Button Tag = ",sender.tag)
            
            var dict = NSMutableDictionary()
            
            dict = NSMutableDictionary(dictionary: self.showComments[sender.tag] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            print("Dict = %@",dict)
            
            if (dict.object(forKey: "user_liked_status") as! String == "true")
            {
                sender.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
                
                dict["user_liked_status"] = "false"
                
                let pID = (dict["commentId"] as! Int)
                
                let id = String(describing: pID)
                
                self.commentUnLike(postId: id)
                
                
                dict["comment_likes"] = (dict["comment_likes"] as! Int - 1)
                
                //                postUnLike(postId: id)
                
                self.showComments[sender.tag] = dict
                
            }
            else
            {
                sender.setImage(UIImage(named: "heart_filled"), for: UIControlState())
                sender.unLikeBounce(0.4)
                
                dict["user_liked_status"] = "true"
                
                
                let pID = (dict["commentId"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                self.commentLike(postId: id)
                
                dict["comment_likes"] = (dict["comment_likes"] as! Int + 1)
                
                self.showComments[sender.tag] = dict
                
                //                postLike(postId: id)
            }
            
            
            let indexPath = IndexPath(item: sender.tag, section: 0)
            
            let cell = tableView.cellForRow(at: indexPath) as! CommentTableViewCell
            
            let likeCnt = dict["comment_likes"] as! Int
            
            if likeCnt == 0
            {
                cell.likeCount.text = ""
            }
            else
            {
                cell.likeCount.text = String(describing: likeCnt) + " Like"
            }
                        
        }
    }
    
    
    func commentLike(postId :String)
    {
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "commentId":postId
        ]
        
        print(params)
        let url = BASE_URL + COMMENTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func commentUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "commentId":postId
        ]
        
        print(params)
        let url = BASE_URL + COMMENTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }

}

extension CommentViewController: CommentTableViewCellDelegate
{
    func commentReply(replyID: String) {
        
    }
    
    func goToProfileUserVC(userId: String) {
//        performSegue(withIdentifier: "Comment_ProfileSegue", sender: userId)
    }
}
