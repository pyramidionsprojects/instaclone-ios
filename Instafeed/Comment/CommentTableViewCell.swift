//
//  CommentTableViewCell.swift
//  InstagramClone
//
//  Created by The Zero2Launch Team on 1/1/17.
//  Copyright © 2017 The Zero2Launch Team. All rights reserved.
//

import UIKit
//import KILabel
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage



protocol CommentTableViewCellDelegate {
    func goToProfileUserVC(userId: String)
    func commentReply(replyID: String)
}

class CommentTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{

    @IBOutlet weak var collectionVIew: UICollectionView!

    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentAgoLabel: UILabel!
    @IBOutlet weak var likeButton: SparkButton!
    
    @IBOutlet weak var likeCount: UILabel!
    
    @IBOutlet weak var replyBtn: UIButton!
    
    var commentId : String = "";
    var postid : String = "";

    @IBOutlet weak var viewProfileBtn: UIButton!
    
    var feedContentPosts = Array<Any>()
    
    var delegate: CommentTableViewCellDelegate?

    override func prepareForReuse()
    {
        super.prepareForReuse()
//        profileImageView.image = UIImage(named: "placeholderImg")
    }

    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       
        
        
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
        
        
        
        collectionVIew.delegate = self
        collectionVIew.dataSource = self
        
    }
    
    
    /*
    
    @objc func commentImageView_TouchUpInside()
    {
        delegate?.goToCommentVC()
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.feedContentPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommentCollectionViewCell", for: indexPath) as! CommentCollectionViewCell
        
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        let pro = feedPosts["profilePic"]as? String ?? ""
        
        
        cell.profileImageView.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-thumbnail"))

        
        let name = feedPosts["name"]as? String ?? ""
        
        if feedPosts.object(forKey: "status") as! String == "true"
        {
            cell.likeButton.setImage(UIImage(named: "heart_filled"), for: UIControlState())
        }
        else
        {
            cell.likeButton.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        let likeCnt = feedPosts["reply_count"]as! Int
        
        if likeCnt == 0
        {
            cell.likeCount.text = ""
        }
        else
        {
            cell.likeCount.text = String(describing: likeCnt) + " Like"
        }
        
        
        cell.likeButton.addTarget(self, action: #selector(CommentTableViewCell.likeBtn(sender:)), for: .touchUpInside)
        
        cell.likeButton.tag = indexPath.row

        
        cell.replyBtn.addTarget(self, action: #selector(CommentTableViewCell.replyBtn(sender:)), for: .touchUpInside)
        
        cell.replyBtn.tag = indexPath.row

        
        let comment = feedPosts["reply"]as? String ?? ""
        
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: name, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Medium", size: 15.0)!])
        
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:name.count))
        
        myMutableString.append(NSAttributedString(string: " "))
        myMutableString.append(NSAttributedString(string: comment))
        
        cell.commentLabel.attributedText = myMutableString
        
        
        let currentDate = feedPosts["repliedAt"]as? String
        
        cell.commentAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        return cell
    }
    
    
    
    
    @objc func replyBtn(sender: AnyObject)
    {
        replyBtn.sendActions(for: .touchUpInside)        
    }
    
    @objc func likeBtn(sender: SparkButton)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            
        }
        else
        {
            print("Like Button Tag = ",sender.tag)
            
            var dict = NSMutableDictionary()
            
            dict = NSMutableDictionary(dictionary: self.feedContentPosts[sender.tag] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            print("Dict = %@",dict)
            
            if (dict.object(forKey: "status") as! String == "true")
            {
                
                sender.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
                
                sender.likeBounce(0.6)
                
                sender.animate()
                
                dict["status"] = "false"
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                self.commentUnLike(postId: id)
                
                dict["reply_count"] = (dict["reply_count"] as! Int - 1)
                
                //                postUnLike(postId: id)
                
                self.feedContentPosts[sender.tag] = dict
            }
            else
            {
                sender.setImage(UIImage(named: "heart_filled"), for: UIControlState())
                sender.unLikeBounce(0.4)
                
                dict["status"] = "true"
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                self.commentLike(postId: id)
                
                dict["reply_count"] = (dict["reply_count"] as! Int + 1)
                
                self.feedContentPosts[sender.tag] = dict
                
                //                postLike(postId: id)
            }
            
            let indexPath = IndexPath(item: sender.tag, section: 0)
            
            let cell = collectionVIew.cellForItem(at: indexPath) as! CommentCollectionViewCell
            
            let likeCnt = dict["reply_count"] as! Int
            
            if likeCnt == 0
            {
                cell.likeCount.text = ""
            }
            else
            {
                cell.likeCount.text = String(describing: likeCnt) + " Like"
            }
        }
    }
    
    
    func commentLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "commentId":commentId,
            "postId":postid,
            "replyId":postId
        ]
        
        print(params)
        let url = BASE_URL + LIKEREPLY
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func commentUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "commentId":commentId,
            "postId":postid,
            "replyId":postId
        ]
        
        print(params)
        let url = BASE_URL + REPLYUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
   
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        

        let rect2 = UIScreen.main.bounds.size

        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
       
        var height :CGFloat = 0.0
        
        let comment = feedPosts["reply"]as? String ?? ""
        
        
        let contentNSString = comment as String
        
        
        height = contentNSString.height(withConstrainedWidth: rect2.width * 0.6, font: UIFont(name: "Poppins-Medium", size: 15.0)!)
        
        
        print("height = ",height)
        
        return CGSize(width: collectionView.frame.size.width, height: height + 52)

        
    }
        
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
}



extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat
    {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
