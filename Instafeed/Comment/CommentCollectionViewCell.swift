//
//  CommentCollectionViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 01/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class CommentCollectionViewCell: UICollectionViewCell
{ 
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentAgoLabel: UILabel!
    @IBOutlet weak var likeButton: SparkButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var viewProfileBtn: UIButton!
    @IBOutlet weak var replyBtn: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.layer.masksToBounds = true
    }
    
}
