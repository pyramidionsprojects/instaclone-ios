//
//  EmailViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class EmailViewController: UIViewController
{
    
    @IBOutlet weak var centerAlignEmail: NSLayoutConstraint!
    
    @IBOutlet weak var centerAlignNext: NSLayoutConstraint!
    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var nextView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        self.nextView.layer.borderWidth = 1
        self.nextView.layer.borderColor = UIColor(red:85/255, green:166/255, blue:218/255, alpha: 1).cgColor
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        centerAlignNext.constant += view.bounds.width
        centerAlignEmail.constant += view.bounds.width
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations:
        {
            self.centerAlignEmail.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.6, delay: 0.3, options: .curveEaseOut, animations:
        {
            self.centerAlignNext.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }

    @IBAction func nextBtn(_ sender: Any)
    {
        if self.isValidEmail(testStr: self.emailTxt.text!)
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "FullNameViewController")as! FullNameViewController
            Dvc.emailId = emailTxt.text!
            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            present(Dvc, animated: true, completion: nil)
        }
        else
        {
            let alertController = UIAlertController(title: "Oops", message: "Invalid Email ID", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    

    @IBAction func clearEmailBtn(_ sender: Any)
    {
        emailTxt.text = ""
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
