//
//  FullNameViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import Alamofire
import SwiftyJSON
import KRProgressHUD


class FullNameViewController: UIViewController,NVActivityIndicatorViewable
{

    var indicator: NVActivityIndicatorView!

    @IBOutlet weak var userNameTxt: UITextField!
    
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var fullNameTxt: UITextField!
    @IBOutlet weak var pwdTxt: UITextField!
    var emailId : String = ""
    
    @IBOutlet weak var centerAlignUserName: NSLayoutConstraint!
    @IBOutlet weak var centerAlignName: NSLayoutConstraint!
    @IBOutlet weak var centerAlignPwd: NSLayoutConstraint!
    @IBOutlet weak var centerAlignNext: NSLayoutConstraint!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        
        self.nextView.layer.borderWidth = 1
        self.nextView.layer.borderColor = UIColor(red:85/255, green:166/255, blue:218/255, alpha: 1).cgColor        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        centerAlignUserName.constant += view.bounds.width
        centerAlignName.constant += view.bounds.width
        centerAlignPwd.constant += view.bounds.width
        centerAlignNext.constant += view.bounds.width
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {
            self.centerAlignUserName.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)

        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations: {
            self.centerAlignName.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
            self.centerAlignPwd.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            self.centerAlignNext.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)

    }
    


    @IBAction func nextBtn(_ sender: Any)
    {
        if userNameTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your user name", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else if fullNameTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your name", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else if pwdTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your password", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
/*
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ChangeUserNameViewController")as! ChangeUserNameViewController
//            Dvc.email = emailId
//            Dvc.userName = fullNameTxt.text!
//            Dvc.password = pwdTxt.text!
            
            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            present(Dvc, animated: true, completion: nil)*/
            
            
            checkName()

        }
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func checkName()
    {
        //        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Sign Up...")
        
        let params: Parameters = [
            "username": userNameTxt.text!
            ]
        
        print(params)
        let url = BASE_URL + CHECKNAME1
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    //                    self.indicator.stopAnimating()
                    
//                    KRProgressHUD.dismiss()
                    
                    
                    if let json = response.result.value
                    {
                        print("SignUp Response =  \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            self.SignUpAPI()

                        }
                        else
                        {
                            
                            KRProgressHUD.dismiss()
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    func SignUpAPI()
    {
//        self.indicator.startAnimating()
        
        let params: Parameters = [
            "name": fullNameTxt.text!,
            "email":emailId,
            "password":pwdTxt.text!,
            "username":userNameTxt.text!
            ]
        
        print(params)
        let url = BASE_URL + CREATE_ACCOTNT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
//                    self.indicator.stopAnimating()
  
                    KRProgressHUD.dismiss()

                    if let json = response.result.value
                    {
                        print("SignUp Response =  \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            //                        self.showAlert(title: "Success", msg: jsonResponse["message"].stringValue)
                            
                            //                        self.navigationController?.popViewController(animated: true)
                            
                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")
                            
                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")
                            
                            UserDefaults.standard.set(jsonResponse["username"].stringValue, forKey: username)
                            
                            UserDefaults.standard.set(jsonResponse["name"].stringValue, forKey: name)

                            let alertController = UIAlertController(title: "Success", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                
                                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ChangeUserNameViewController")as! ChangeUserNameViewController
                                    Dvc.userName = jsonResponse["username"].stringValue
                                    Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                    self.present(Dvc, animated: true, completion: nil)
                                
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            //                        self.showAlert(title: "Oops", msg: jsonResponse["message"].stringValue)
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    

}
