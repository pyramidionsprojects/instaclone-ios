//
//  ChangeUserNameViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import Alamofire
import SwiftyJSON
import Firebase


class ChangeUserNameViewController: UIViewController,NVActivityIndicatorViewable,CLLocationManagerDelegate
{

    let locationManager = CLLocationManager()

    var userName : String = ""
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    
    @IBOutlet weak var centerAlignNext: NSLayoutConstraint!
    @IBOutlet weak var changeNameLabel: UILabel!
    
    var lat : String = ""
    var log : String = ""

    var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        welcomeLabel.text = "Welcome to Snipofeed, \n" + userName
        
        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.welcomeLabel.alpha = 0.0
        self.desLabel.alpha = 0.0
        self.changeNameLabel.alpha = 0.0
        
        centerAlignNext.constant += view.bounds.width
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {
            
            self.welcomeLabel.alpha = 1.0

        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.desLabel.alpha = 1.0

        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
            
            self.centerAlignNext.constant -= self.view.bounds.width
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            
            self.changeNameLabel.alpha = 1.0

        }, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
//        lat = locValue.latitude
//        lang = locValue.longitude
        
        lat = String(locValue.latitude)
        log = String(locValue.longitude)
        
        UserDefaults.standard.set(lat, forKey: currentLat)
        UserDefaults.standard.set(log, forKey: currentLong)
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func nextBtn(_ sender: Any)
    {
//        SignUpAPI()
        updateToken()
    }
    
    
    func updateToken()
    {
        var refreshedToken : String? = InstanceID.instanceID().token()
        
        if refreshedToken == nil
        {
            refreshedToken = ""
        }
        
        if UserDefaults.standard.object(forKey: "currentLat") != nil
        {
            lat =  UserDefaults.standard.object(forKey: currentLat) as! String
            log =  UserDefaults.standard.object(forKey: currentLong) as! String
        }
        else
        {
            lat = ""
            log = ""
        }

        var accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        if accessToken == nil
        {
            accessToken = ""
        }
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "deviceToken":refreshedToken!,
            "appVersion":"0.1",
            "os":"iOS",
            "localLat":lat,
            "locallong":log
            ]
        
        print(params)
        let url = BASE_URL + DEVICE_TOKEN
        
        let Headers: HTTPHeaders =
        [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    self.indicator.stopAnimating()
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            UserDefaults.standard.set(true, forKey: isLoggedIn)

                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "AddProfileImageViewController")as! AddProfileImageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                    }
                }
                else
                {
                    self.indicator.stopAnimating()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func changeUserNameBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ChangeUserViewController")as! ChangeUserViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
}
