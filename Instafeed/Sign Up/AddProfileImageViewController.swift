//
//  AddProfileImageViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 26/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import AWSS3
import AWSCore
import KRProgressHUD

class AddProfileImageViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate,NVActivityIndicatorViewable
{

    
    
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var addProfileLabel: UILabel!
    @IBOutlet weak var profileLabelDes: UILabel!
    
    @IBOutlet weak var centerAlignPhoto: NSLayoutConstraint!
    @IBOutlet weak var centerAlignSkip: NSLayoutConstraint!
    
    var cameBack : Int = 0
    
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var ProfileImgURL : String = ""
    var indicator: NVActivityIndicatorView!
    
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        imagePicker.delegate = self
        
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        
        if cameBack == 0
        {
            self.profileImgView.alpha = 0.0
            self.addProfileLabel.alpha = 0.0
            self.profileLabelDes.alpha = 0.0
            
            centerAlignPhoto.constant += view.bounds.width
            centerAlignSkip.constant += view.bounds.width
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if cameBack == 0
        {
        
            UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {

                self.profileImgView.alpha = 1

            }, completion: nil)
            
            UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations: {
                
                self.addProfileLabel.alpha = 1

            }, completion: nil)
            
            UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
                
                self.profileLabelDes.alpha = 1.0

            }, completion: nil)
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
                
                self.centerAlignPhoto.constant -= self.view.bounds.width
                
                self.view.layoutIfNeeded()


            }, completion: nil)
            
            UIView.animate(withDuration: 0.5, delay: 0.6, options: .curveEaseOut, animations: {
                
                self.centerAlignSkip.constant -= self.view.bounds.width
                
                self.view.layoutIfNeeded()


            }, completion: nil)
        }
        
    }
    
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        profileImgView.layer.masksToBounds=true
        profileImgView.layer.borderWidth=3
        profileImgView.layer.borderColor = UIColor.black.cgColor
        profileImgView.layer.cornerRadius=profileImgView.bounds.width/2

    }
    
    
    @IBAction func addPhoto(_ sender: Any)
    {
        
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel)
        {
            action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Choose Gallery", style: .default)
        {
            action -> Void in

                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .photoLibrary
                
                
                /*
                 The sourceType property wants a value of the enum named        UIImagePickerControllerSourceType, which gives 3 options:
                 
                 UIImagePickerControllerSourceType.PhotoLibrary
                 UIImagePickerControllerSourceType.Camera
                 UIImagePickerControllerSourceType.SavedPhotosAlbum
                 
                 */
            self.present(self.imagePicker, animated: true, completion: nil)


        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Choose Camera", style: .default) { action -> Void in

            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        
        cameBack = 1;
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.profileImgView.image = pickedImage

            ProfileImgURL = ""

            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            let imgData = UIImageJPEGRepresentation(chosenImage, 0.5)!
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyyMMddHHmmssSSS"
            var strFileName = formatter.string(from: Date())
            strFileName = strFileName + ".png"
            
            
//            self.indicator.startAnimating()
            
            
            KRProgressHUD.show(withMessage: "Loading...")


            let expression = AWSS3TransferUtilityUploadExpression()
            expression.progressBlock =
                {
                    (task, progress) in DispatchQueue.main.async(execute:
                        {
                            print(progress)
                    })
            }
            
            completionHandler =
                { (task, error) -> Void in
                    DispatchQueue.main.async(execute:
                        {
                    })
            }
            
            let  transferUtility = AWSS3TransferUtility.default()
            
            transferUtility.uploadData(imgData,
                                       bucket: BUCKET_NAME,
                                       key: strFileName,
                                       contentType: "image/png",
                                       expression: expression,
                                       completionHandler: completionHandler).continueWith
                {
                    (task) -> AnyObject! in
                    if let error = task.error
                    {
                        print("Error: \(error.localizedDescription)")
                        KRProgressHUD.dismiss()
                    }
                    
                    if let _ = task.result
                    {
                        self.ProfileImgURL = ""
                        
                        self.indicator.stopAnimating()
                        
                        self.ProfileImgURL = IMAGES_BUCKET + strFileName

                        print("Send Image = ",self.ProfileImgURL)
                        
                        self.uploadImage()
                    }
                    
                    return nil
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        cameBack = 1;
        dismiss(animated: true, completion:nil)
    }
    
    
    @IBAction func skipBtn(_ sender: Any)
    {
        UserDefaults.standard.set(true, forKey: isLoggedIn)

        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(Dvc, animated: true, completion: nil)
        
    }
    
    func uploadImage()
    {
//        self.indicator.startAnimating()
        
//        KRProgressHUD.dismiss()

        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "profilePic":self.ProfileImgURL,
            ]
        
        print(params)
        let url = BASE_URL + PROFILEPICUPLOAD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
//                    self.indicator.stopAnimating()
                      
                    KRProgressHUD.dismiss()

                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            UserDefaults.standard.set(true, forKey: isLoggedIn)
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)

                            
                        }
                        else
                        {
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
