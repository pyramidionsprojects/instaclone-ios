//
//  ChangeUserViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import Alamofire
import SwiftyJSON
import CoreLocation
import KRProgressHUD



class ChangeUserViewController: UIViewController,NVActivityIndicatorViewable
{
    
    @IBOutlet weak var changeUserNameLabel: UILabel!
    @IBOutlet weak var centerAlignName: NSLayoutConstraint!
    @IBOutlet weak var centerAlignNext: NSLayoutConstraint!
    
    @IBOutlet weak var usernameTxt: UITextField!
    var indicator: NVActivityIndicatorView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
        
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.changeUserNameLabel.alpha = 0.0
        
        centerAlignName.constant += view.bounds.width
        centerAlignNext.constant += view.bounds.width
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {
            
            self.changeUserNameLabel.alpha = 1.0
            
        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations: {
            
            self.centerAlignName.constant -= self.view.bounds.width
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
            
            self.centerAlignNext.constant -= self.view.bounds.width
            
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if usernameTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your name", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            updateUserName()
        }
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    func updateUserName()
    {
//        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Loading...")

        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")

        
        let params: Parameters = [
            "accessToken": accessToken!,
            "username":self.usernameTxt.text!,
        ]
        
        print(params)
        let url = BASE_URL + CHECKNAME
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
//                    self.indicator.stopAnimating()

                    
                    KRProgressHUD.dismiss()

                    
                    if let json = response.result.value
                    {
                        print("Change User Name Response =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "AddProfileImageViewController")as! AddProfileImageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    

}
