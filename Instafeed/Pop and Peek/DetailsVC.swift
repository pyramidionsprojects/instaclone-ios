//
//  DetailsVC.swift
//  3DpopAndpeek
//
//  Created by Apple on 23/05/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

    
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var imagename: UIImageView!
    var imagenames : String = ""
    var proPic : String = ""
    var proName : String = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        profilePic.layer.cornerRadius = self.profilePic.frame.width / 2
        
        profilePic.layer.masksToBounds = true
        
        profileName.text! = proName
        
        
        title = "3D Touch"
//        imagename.image = UIImage(named: imagenames)
        
        
        imagename.sd_setImage(with: URL(string: imagenames), placeholderImage: UIImage(named: "default-thumbnail"))

        
        
        profilePic.sd_setImage(with: URL(string: imagenames), placeholderImage: UIImage(named: "default-thumbnail"))

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var previewActionItems : [UIPreviewActionItem]
    {
        let dummyAction = UIPreviewAction(title: "3D Touch is Awesome!", style: .default, handler: { action, previewViewController in
            print("Action selected!")
        })
        
        return [dummyAction]
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
