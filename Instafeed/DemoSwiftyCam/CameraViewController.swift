/*Copyright (c) 2016, Andrew Walz.

Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */


import UIKit
import AVFoundation
import iOSPhotoEditor
import Photos
import AVFoundation
import MobileCoreServices
import AudioToolbox
import ARSLineProgress
import AWSS3
import AWSCore
import Alamofire
import SwiftyJSON
import KRProgressHUD


class CameraViewController: SwiftyCamViewController, SwiftyCamViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate
{
    
//    var identifier: UIBackgroundTaskIdentifier = 0

    @IBOutlet weak var bgImgView: UIImageView!
    @IBOutlet weak var dotImgView: UIImageView!
    @IBOutlet weak var typeText: UITextView!
    
    @IBOutlet weak var placeholderLbl: UILabel!
    
    @IBOutlet weak var typeTextView: UIView!
    
    
    var dotCount = 1
    
    var ProfileImgURL : String = ""
    
    @IBOutlet weak var collectView: UIView!
    @IBOutlet weak var arrowView: UIView!
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?

    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var captureButton    : SwiftyRecordButton!
    @IBOutlet weak var flipCameraButton : UIButton!
    @IBOutlet weak var flashButton      : UIButton!
    
    
    let myStaticArray = [
        "Type",
        "Camera",
        "Free-Hand"
    ]

    
    var status = 1
    
	override func viewDidLoad()
    {
		super.viewDidLoad()
        
        shouldPrompToAppSettings = true
		cameraDelegate = self
		maximumVideoDuration = 10.0
        shouldUseDeviceOrientation = true
        allowAutoRotate = true
        audioEnabled = true
        
        typeTextView.isHidden = true
        
        typeText.delegate = self
        
        dotImgView.layer.cornerRadius = self.dotImgView.frame.height / 2.0;
        dotImgView.layer.masksToBounds = true
        
        dotImgView.layer.borderColor = UIColor.white.cgColor
        dotImgView.layer.borderWidth = 1
        
        

        
        
        // disable capture button until session starts
        captureButton.buttonEnabled = false
        
        self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        scrollToNearestVisibleCollectionViewCells(i: 1)
	}
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func scrollToNearestVisibleCollectionViewCell()
    {
        let visibleCenterPositionOfScrollView = Float(collectionView.contentOffset.x + (self.collectionView!.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<collectionView.visibleCells.count
        {
            let cell = collectionView.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
            
            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance
            {
                closestDistance = distance
                closestCellIndex = collectionView.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1
        {
            
            if closestCellIndex == 0
            {
                status = 0
                captureButton.isVideo = false
                typeTextView.isHidden = false
            }
            else if closestCellIndex == 1
            {
                status = 1
                captureButton.isVideo = false
                typeTextView.isHidden = true
            }
            else if closestCellIndex == 2
            {
                status = 2
                captureButton.isVideo = true
                typeTextView.isHidden = true
            }
            
            self.collectionView!.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }

    func scrollToNearestVisibleCollectionViewCells(i :Int)
    {
//        let visibleCenterPositionOfScrollView = Float(collectionView.contentOffset.x + (self.collectionView!.bounds.size.width / 2))
//        var closestCellIndex = -1
//        var closestDistance: Float = .greatestFiniteMagnitude
////        for i in 0..<collectionView.visibleCells.count
////        {
//            let cell = collectionView.visibleCells[i]
//            let cellWidth = cell.bounds.size.width
//            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
//
//            // Now calculate closest cell
//            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
//            if distance < closestDistance {
//                closestDistance = distance
//                closestCellIndex = collectionView.indexPath(for: cell)!.row
//            }
////        }
//        if closestCellIndex != -1
//        {
            self.collectionView!.scrollToItem(at: IndexPath(row: i, section: 0), at: .centeredHorizontally, animated: true)
//        }
    }
    
    
	override var prefersStatusBarHidden: Bool
    {
		return true
	}
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        typeTextView.isHidden = true
        
        self.bgImgView.image = #imageLiteral(resourceName: "I1")
        self.dotImgView.image = #imageLiteral(resourceName: "I1")
        
        scrollToNearestVisibleCollectionViewCells(i: 1)
    }
    

	override func viewDidAppear(_ animated: Bool)
    {
		super.viewDidAppear(animated)
        captureButton.delegate = self
	}
    
    func swiftyCamSessionDidStartRunning(_ swiftyCam: SwiftyCamViewController)
    {
        print("Session did start running")
        captureButton.buttonEnabled = true
    }
    
    func swiftyCamSessionDidStopRunning(_ swiftyCam: SwiftyCamViewController)
    {
        print("Session did stop running")
        captureButton.buttonEnabled = false
    }
    

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage)
    {
        
        let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
        photoEditor.photoEditorDelegate = self
        photoEditor.image = photo
        //Colors for drawing and Text, If not set default values will be used
        //photoEditor.colors = [.red, .blue, .green]
        
        //Stickers that the user will choose from to add on the image
        for i in 0...10
        {
            photoEditor.stickers.append(UIImage(named: i.description )!)
        }

        //To hide controls - array of enum control
        //photoEditor.hiddenControls = [.crop, .draw, .share]

        present(photoEditor, animated: true, completion: nil)
        
//        let newVC = PhotoViewController(image: photo)
//        self.present(newVC, animated: true, completion: nil)
	}

    
	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection)
    {
		print("Did Begin Recording")
        
        self.collectView.isHidden = true
        self.arrowView.isHidden = true
        
		captureButton.growButton()
        hideButtons()
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection)
    {
        self.collectView.isHidden = false
        self.arrowView.isHidden = false
		print("Did finish Recording")
		captureButton.shrinkButton()
        showButtons()
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL)
    {
        print("URL = ",url)
		let newVC = VideoViewController(videoURL: url)
		self.present(newVC, animated: true, completion: nil)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint)
    {
        print("Did focus at point: \(point)")
        focusAnimationAt(point)
	}
    
    func swiftyCamDidFailToConfigure(_ swiftyCam: SwiftyCamViewController)
    {
        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat)
    {
        print("Zoom level did change. Level: \(zoom)")
		print(zoom)
	}

	func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection)
    {
        print("Camera did change to \(camera.rawValue)")
		print(camera)
	}
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFailToRecordVideo error: Error)
    {
        print(error)
    }

    @IBAction func cameraSwitchTapped(_ sender: Any)
    {

        switchCamera()
        
        /*
        
        if captureButton.isVideo
        {
            
            
            
            if captureButton.buttonEnabled
            {
                
                switchCamera()

                captureButton.buttonEnabled = false
                

            }
            else
            {
                switchCamera()

                captureButton.buttonEnabled = true
                

            }
            
        }
        else
        {
            switchCamera()
        }*/
    }
    
    @IBAction func toggleFlashTapped(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            imagePicker.sourceType = .photoLibrary;
            //imagePicker.sourceType = .savedPhotosAlbum
            // imagePicker.mediaTypes = [kUTTypeMovie as NSString as String]
            imagePicker.mediaTypes = ["public.image", "public.movie"]
            //imagePicker.videoMaximumDuration = TimeInterval(10.0)
            imagePicker.allowsEditing = false
            
/*            self.identifier = UIBackgroundTaskInvalid

            self.identifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                print("Background handler called. Not running background tasks anymore.")
                UIApplication.shared.endBackgroundTask(self.identifier)
                self.identifier = UIBackgroundTaskInvalid
            })
*/
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        dismiss(animated: true, completion: nil)
        
        guard info[UIImagePickerControllerMediaType] != nil else
        {
            return
        }
        
        let mediaType = info[UIImagePickerControllerMediaType] as! CFString
        switch mediaType
        {
        case kUTTypeImage:
            
            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
            photoEditor.photoEditorDelegate = self
            photoEditor.image = image
            //Colors for drawing and Text, If not set default values will be used
            //photoEditor.colors = [.red, .blue, .green]
            
            //Stickers that the user will choose from to add on the image
            for i in 0...20
            {
                photoEditor.stickers.append(UIImage(named: i.description )!)
            }
            
            //To hide controls - array of enum control
            //photoEditor.hiddenControls = [.crop, .draw, .share]
            
            present(photoEditor, animated: true, completion: nil)
           
            break
        case kUTTypeMovie:
            
            let manager = FileManager.default
            guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            else
            {
                return
            }
            
            guard let mediaType = info[UIImagePickerControllerMediaType] as? String
            else
            {
                return
            }
            
            guard let url = info[UIImagePickerControllerMediaURL] as? NSURL else
            {
                return
            }
            
            let newVC = VideoViewController(videoURL: url as URL)
            self.present(newVC, animated: true, completion: nil)

            if mediaType == kUTTypeMovie as String || mediaType == kUTTypeVideo as String
            {
                
            }
            
            break
        
        default:
            break
        }
    }

    /*{
/*        flashEnabled = !flashEnabled
        toggleFlashAnimation()*/
        
        let imagePicker = OpalImagePickerController()
        
        imagePicker.imagePickerDelegate = self
        
        imagePicker.selectionTintColor = UIColor.white.withAlphaComponent(0.7)
        
        imagePicker.selectionImageTintColor = UIColor.black
        
//        imagePicker.selectionImage = UIImage(named: "x_image")
        
        imagePicker.statusBarPreference = UIStatusBarStyle.lightContent
        
        imagePicker.maximumSelectionsAllowed = 5
        
//        imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        
        let configuration = OpalImagePickerConfiguration()

        configuration.maximumSelectionsAllowedMessage = NSLocalizedString("You cannot select that many images!", comment: "")

        imagePicker.configuration = configuration
        
        present(imagePicker, animated: true, completion: nil)

/*        let imagePicker = OpalImagePickerController()
        presentOpalImagePickerController(imagePicker, animated: true,
                                         select:
        {
            (assets) in
            
            print("Assets = ",assets)
                                            //Select Assets
        },
        cancel:
        {
            //Cancel
        })*/
        
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingImages images: [UIImage])
    {
        print("Images = ",images)
//        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset])
    {
        print("Assets = ",assets)
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel(_ picker: OpalImagePickerController)
    {
//        self.dismiss(animated: true, completion: nil)
    }
    */
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return myStaticArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraCollectionViewCell", for: indexPath) as! CameraCollectionViewCell
            
            cell.nameLbl.text = self.myStaticArray[indexPath.row]
            
            //            cell.suggestionLbl.sizeToFit()
            
            return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        snapToCenter()
        
//        scrollToNearestVisibleCollectionViewCell()

        scrollToNearestVisibleCollectionViewCells(i: indexPath.row)
        
        
        if indexPath.row == 0
        {
            status = 0
            
            captureButton.isVideo = false
            
            typeTextView.isHidden = false

            
        }
        else if indexPath.row == 1
        {
            status = 1
            
            captureButton.isVideo = false
            
            typeTextView.isHidden = true


        }
        else if indexPath.row == 2
        {
            status = 2
            
            captureButton.isVideo = true
            
            typeTextView.isHidden = true

        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        
    }
    
    
    func postImage(type :String,storyDescription :String,storyURL :String,bgimage :String)
    {
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "storyURL":storyURL,
            "storyDescription":storyDescription,
            "bgimage":bgimage,
            "type":type
        ]
        
        print(params)
        let url = BASE_URL + CREATESTORIES
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                if(response.result.isSuccess)
                {
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func uploadImage(chosenImage: UIImage)
    {
        
        KRProgressHUD.show(withMessage: "Uploading...")
        
        let imgData = UIImageJPEGRepresentation(chosenImage, 1)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
                    
                    KRProgressHUD.dismiss()
                }
                
                if let _ = task.result
                {
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    self.postImage(type: "image", storyDescription: "", storyURL: self.ProfileImgURL, bgimage: "")
                }
                return nil
        }
    }
    

    func postType(type :String,storyDescription :String,storyURL :String,bgimage :String)
    {
        KRProgressHUD.show(withMessage: "Uploading...")

        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "storyURL":storyURL,
            "storyDescription":storyDescription,
            "bgimage":bgimage,
            "type":type
        ]
        
        print(params)
        let url = BASE_URL + CREATESTORIES
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                if(response.result.isSuccess)
                {
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    

    @IBAction func changeTheme(_ sender: Any)
    {
        dotCount = dotCount + 1
        
        if dotCount == 10
        {
            dotCount = 1
        }
        
        let count = "I" + String(dotCount)

        let image = UIImage(named:count)

        self.bgImgView.image = image
        self.dotImgView.image = image
    }
    
    @IBAction func doneBtn(_ sender: Any)
    {
        let count = String(dotCount)
        
        self.postType(type: "text", storyDescription: self.typeText.text, storyURL: "", bgimage: count)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("print1")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("print2")
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            placeholderLbl.isHidden = false
        }
        else
        {
            placeholderLbl.isHidden = true
        }
    }

    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if textView.text.count > 0
        {
            placeholderLbl.isHidden = true
        }
        else
        {
            placeholderLbl.isHidden = false
        }
        return true
    }

    
}



// UI Animations
extension CameraViewController
{
    fileprivate func hideButtons()
    {
        UIView.animate(withDuration: 0.25)
        {
            self.flashButton.alpha = 0.0
            self.flipCameraButton.alpha = 0.0
        }
    }
    
    fileprivate func showButtons() {
        UIView.animate(withDuration: 0.25) {
            self.flashButton.alpha = 1.0
            self.flipCameraButton.alpha = 1.0
        }
    }
    

    fileprivate func focusAnimationAt(_ point: CGPoint)
    {
        let focusView = UIImageView(image: #imageLiteral(resourceName: "focus"))
        focusView.center = point
        focusView.alpha = 0.0
        view.addSubview(focusView)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations:
        {
            focusView.alpha = 1.0
            focusView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
        })
        {
            (success) in
            UIView.animate(withDuration: 0.15, delay: 0.5, options: .curveEaseInOut, animations:
            {
                focusView.alpha = 0.0
                focusView.transform = CGAffineTransform(translationX: 0.6, y: 0.6)
            }) { (success) in
                focusView.removeFromSuperview()
            }
        }
    }
    
    fileprivate func toggleFlashAnimation()
    {
        if flashEnabled == true
        {
            flashButton.setImage(#imageLiteral(resourceName: "flash"), for: UIControlState())
        }
        else
        {
            flashButton.setImage(#imageLiteral(resourceName: "flashOutline"), for: UIControlState())
        }
    }
}


extension CameraViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
            let name :String = self.myStaticArray[indexPath.row]

            let width: CGFloat = NSMutableAttributedString(string: name, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Medium", size: 20.0)!]).size().width

            return CGSize(width: width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        let inset: CGFloat = collectionView.frame.width * 0.5
        return UIEdgeInsetsMake(0, inset, 0, inset)
    }
}

extension CameraViewController: UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
//        snapToCenter()
        scrollToNearestVisibleCollectionViewCell()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if !decelerate
        {
            scrollToNearestVisibleCollectionViewCell()
//            snapToCenter()
        }
    }
}

extension CameraViewController: PhotoEditorDelegate
{
    
    func doneEditing(image: UIImage)
    {
//        imageView.image = image
        
        session.stopRunning()
        
        self.uploadImage(chosenImage: image)
    }
    
    func canceledEditing()
    {
        print("Canceled")
    }
    
}
