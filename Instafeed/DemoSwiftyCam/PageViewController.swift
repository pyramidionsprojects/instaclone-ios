//
//  PageViewController.swift
//  Snipofeed
//
//  Created by Pyramidions on 02/07/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController,UIScrollViewDelegate
{
    
    fileprivate lazy var pages: [UIViewController] =
    {
        return [
            self.getViewController(withIdentifier: "CameraViewController"),
            self.getViewController(withIdentifier: "MainTabViewController"),
            self.getViewController(withIdentifier: "ChatListNav")
            ]
    }()
    
    
    var currentPage = 1
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate   = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.movePageViewController(_:)), name: NSNotification.Name(rawValue: "MovePageViewController"), object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.backPageViewController(_:)), name: NSNotification.Name(rawValue: "BackPageViewController"), object: nil)


        NotificationCenter.default.addObserver(self, selector: #selector(self.disableSwipe(_:)), name: NSNotification.Name(rawValue: "disableSwipeGesture"), object: nil)


        NotificationCenter.default.addObserver(self, selector: #selector(self.enableSwipe(_:)), name: NSNotification.Name(rawValue: "enableSwipeGesture"), object: nil)

        

//        for subview in self.view.subviews {
//            if let scrollView = subview as? UIScrollView {
//                scrollView.delegate = self
//                break;
//            }
//        }

     
        
        
        
        
//        if let firstVC = pages.first
//        {
            setViewControllers([pages[1]], direction: .forward, animated: true, completion: nil)
//        }
    }
    
    
    @objc func enableSwipe(_ notification: NSNotification)
    {
        enableSwipeGesture()
    }

    @objc func disableSwipe(_ notification: NSNotification)
    {
        disableSwipeGesture()
    }
    
    @objc func movePageViewController(_ notification: NSNotification)
    {
        setViewControllers([pages[0]], direction: .reverse, animated: true, completion: nil)
    }
    
    @objc func backPageViewController(_ notification: NSNotification)
    {
        setViewControllers([pages[1]], direction: .reverse, animated: true, completion: nil)
    }
    
    
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView)
//    {
//        if (currentPage == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width)
//        {
//            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0);
//        }
//        else if (currentPage == pages.count && scrollView.contentOffset.x > scrollView.bounds.size.width)
//        {
//            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0);
//        }
//    }
//
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
//    {
//        if (currentPage == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width)
//        {
//            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0);
//        }
//        else if (currentPage == pages.count && scrollView.contentOffset.x >= scrollView.bounds.size.width)
//        {
//            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0);
//        }
//    }
    
}



extension PageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        
        guard let viewControllerIndex = pages.index(of: viewController)
        else
        {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        
        currentPage = previousIndex
        
        guard previousIndex >= 0
        else
        {
            return nil
        }
        
        guard pages.count > previousIndex
        else
        {
            return nil
        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        currentPage = nextIndex

        
        guard nextIndex < pages.count else
        {
            return nil
        }
        
        guard pages.count > nextIndex else
        {
            return nil
        }
        
        return pages[nextIndex]
        
        
        
    }
}

extension PageViewController: UIPageViewControllerDelegate { }


extension UIPageViewController {
    
    func enableSwipeGesture() {
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = true
            }
        }
    }
    
    func disableSwipeGesture() {
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}
