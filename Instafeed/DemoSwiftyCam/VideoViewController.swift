/*Copyright (c) 2016, Andrew Walz.
 
 Redistribution and use in source and binary forms, with or without modification,are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

import UIKit
import AVFoundation
import AVKit
import ARSLineProgress
import AWSS3
import AWSCore
import Alamofire
import SwiftyJSON
import KRProgressHUD



class VideoViewController: UIViewController
{
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    private var videoURL: URL
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    var ProfileImgURL : String = ""
    
    init(videoURL: URL)
    {
        self.videoURL = videoURL
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray
        player = AVPlayer(url: videoURL)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else
        {
            return
        }
        
        playerController!.showsPlaybackControls = false
        
        playerController!.player = player!
        self.addChildViewController(playerController!)
        self.view.addSubview(playerController!.view)
        playerController!.view.frame = view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
        
        let cancelButton = UIButton(frame: CGRect(x: 15.0, y: 15.0, width: 20.0, height: 20.0))
        cancelButton.setImage(#imageLiteral(resourceName: "cancel-2"), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        view.addSubview(cancelButton)
        
        
        let doneButton = UIButton(frame: CGRect(x: self.view.frame.size.width - 35.0, y: 15.0, width: 20.0, height: 20.0))
        doneButton.setImage(#imageLiteral(resourceName: "left-arrow (1)"), for: UIControlState())
        doneButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        view.addSubview(doneButton)
        
        // Allow background audio to continue to play

        do
        {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        }
        catch let error as NSError
        {
            print(error)
        }
        
        do
        {
            try AVAudioSession.sharedInstance().setActive(true)
        }
        catch let error as NSError
        {
            print(error)
        }
        
        print("videoURL = ",videoURL);
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        player?.play()
    }
    
    @objc func cancel()
    {
        dismiss(animated: true, completion: nil)
    }

    @objc func done()
    {
        
        stopPlayer()
        
        self.uploadVideo(url: self.videoURL)
    }
    
    
    func stopPlayer() {
        if let play = player {
            print("stopped")
            play.pause()
            player = nil
            print("player deallocated")
        } else {
            print("player was already deallocated")
        }
    }

    
    
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification)
    {
        if self.player != nil
        {
            self.player!.seek(to: kCMTimeZero)
            self.player!.play()
        }
    }
    
    func uploadVideo(url :URL)
    {
        KRProgressHUD.show(withMessage: "Uploading...")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var filename = formatter.string(from: Date())
        filename = filename + ".mp4"
        
        let transferManager = AWSS3TransferManager.default()
        
        let uploadingFileURL = url//URL(fileURLWithPath: String(describing: fileURL))
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        
        uploadRequest?.bucket = BUCKET_NAME
        uploadRequest?.key = filename
        uploadRequest?.body = uploadingFileURL
        
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as? NSError
            {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code)
                {
                    switch code
                    {
                        case .cancelled, .paused:
                            break
                        default:
                            print("Error uploading: \(uploadRequest?.key) Error: \(error)")
                        
                            KRProgressHUD.dismiss()
                        
                            transferManager.cancelAll()
                    }
                }
                else
                {
                    print("Error uploading: \(uploadRequest?.key) Error: \(error)")
                    
                    KRProgressHUD.dismiss()
                    
                    transferManager.cancelAll()
                    
                }
                return nil
            }
            else
            {
                let uploadOutput = task.result
                let url = IMAGES_BUCKET + (uploadRequest?.key)!
                
                self.ProfileImgURL = url
                
                self.postImage()
            }
            
            return nil
        })
    }
    
    func postImage()
    {
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "storyURL":ProfileImgURL,
            "storyDescription":"",
            "bgimage":"",
            "type":"video"
        ]
        
        print(params)
        let url = BASE_URL + CREATESTORIES
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                if(response.result.isSuccess)
                {
                    
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
