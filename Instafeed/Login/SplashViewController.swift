//
//  SplashViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 27/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool)
    {
        
        super.viewWillAppear(animated)
        
        let isLogIn = UserDefaults.standard.bool(forKey: isLoggedIn)
        if(isLogIn)
        {
            //            self.updateDeviceToken()
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(Dvc, animated: true, completion: nil)
            
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(Dvc, animated: true, completion: nil)
            
        }
    }
    

}
