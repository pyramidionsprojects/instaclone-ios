//
//  FacebookLoginViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import CoreLocation
import Firebase


class FacebookLoginViewController: UIViewController, FBSDKLoginButtonDelegate,NVActivityIndicatorViewable ,CLLocationManagerDelegate
{
    
    
    let locationManager = CLLocationManager()

    
    @IBOutlet weak var centerAlignFb: NSLayoutConstraint!    
    @IBOutlet weak var fbDisplayLAbel: UILabel!
    @IBOutlet weak var orImgView: UIImageView!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var signInLabel: UILabel!
    

    @IBOutlet weak var fbView: UIView!
    var dict : [String : AnyObject]!
    var email : String = ""
    var name : String = ""
    var profilePic : String = ""
    var socialToken : String = ""
    var isNewUser : Bool = false
    
    
    var indicator: NVActivityIndicatorView!

    
    @IBOutlet weak var loginButton: FBSDKLoginButton!
    
    var lat : String = ""
    var log : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        
        configureFacebook()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.fbDisplayLAbel.alpha = 0.0
        self.orImgView.alpha = 0.0
        self.signUpLabel.alpha = 0.0
        self.signInLabel.alpha = 0.0
        
        centerAlignFb.constant += view.bounds.width
       
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        lat = String(locValue.latitude)
        log = String(locValue.longitude)
        
        UserDefaults.standard.set(lat, forKey: currentLat)
        UserDefaults.standard.set(log, forKey: currentLong)
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil

        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations: {
            self.centerAlignFb.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {

            self.fbDisplayLAbel.alpha = 1.0

        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            
            self.orImgView.alpha = 1.0

        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            
            self.signUpLabel.alpha = 1.0

        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.6, options: .curveEaseOut, animations: {
            
            self.signInLabel.alpha = 1.0

        }, completion: nil)
        
    
    }
    
    
    @IBAction func signInBtn(_ sender: Any)
    {
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "EmailViewController")as! EmailViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func signUpBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    
    //    MARK: FBSDKLoginButtonDelegate Methods
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                //                print(Parameters())
                if (error == nil)
                {
                    
                    
                    let LoginData = result as! NSDictionary
                    print("FACEBOOK DATA\(LoginData)")

                    self.name = LoginData.value(forKey: "name")as! String
                    self.socialToken = LoginData.value(forKey: "id")as! String
                    
                     if LoginData["email"] == nil
                     {
                        self.email = ""
                     }
                     else
                     {
                        self.email = LoginData.value(forKey: "email")as! String
                     }

                     let profileImg = LoginData.value(forKey: "picture") as! NSDictionary
                     let data = profileImg["data"]! as! NSDictionary

                     if let ProfileImage = data["url"]! as? String
                     {
                        self.profilePic = data["url"]! as! String
                     }
                     else
                     {
                        self.profilePic = ""
                     }
                    
                    self.socialToken = FBSDKAccessToken.current().tokenString
                    
                    self.loginButtonDidLogOut(loginButton)

                    
                    self.SignIn()
                    
                }
            })
        }
        
        
        
    }

            
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
            
            

    
    func configureFacebook()
    {
        loginButton.readPermissions = ["public_profile", "email", "user_friends"];
        loginButton.delegate = self
        loginButton.loginBehavior = FBSDKLoginBehavior.web
    }
    
    func SignIn()
    {
//        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Sign In...")

        let params: Parameters = [
            "email": email,
            "name":name,
            "profilePic":profilePic,
            "socialToken":socialToken
            ]
        
        print(params)
        let url = BASE_URL + SOCIAL_LOGIN
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")
                            
                            self.isNewUser = jsonResponse["isNewUser"].boolValue
                            
                            
                            self.updateToken()
                            
                        }
                        else
                        {
                            
                            self.indicator.stopAnimating()
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateToken()
    {
        var refreshedToken : String? = InstanceID.instanceID().token()
        
        if refreshedToken == nil
        {
            refreshedToken = ""
        }
        
        var accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        if accessToken == nil
        {
            accessToken = ""
        }
        
        if UserDefaults.standard.object(forKey: "currentLat") != nil
        {
            lat =  UserDefaults.standard.object(forKey: currentLat) as! String
            log =  UserDefaults.standard.object(forKey: currentLong) as! String
        }
        else
        {
            lat = ""
            log = ""
        }

        let params: Parameters = [
            "accessToken": accessToken!,
            "deviceToken":refreshedToken!,
            "appVersion":"0.1",
            "os":"iOS",
            "localLat":lat,
            "locallong":log
        ]
        
        print(params)
        let url = BASE_URL + DEVICE_TOKEN
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    
                    if let json = response.result.value
                    {
                        print("Update Token =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")
                            
                            let accessToken = UserDefaults.standard.string(forKey: "accessToken")

                            
                            print("accessToken = ",accessToken)
                            
                            
                            let alertController = UIAlertController(title: "Success", message: "Login Successful", preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                if self.isNewUser
                                {
                                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ChangeUserViewController")as! ChangeUserViewController
                                    Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                    self.present(Dvc, animated: true, completion: nil)
                                }
                                else
                                {
                                    
                                    UserDefaults.standard.set(true, forKey: isLoggedIn)

                                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                    let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                                    Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                    self.present(Dvc, animated: true, completion: nil)
                                    
                                }
                                
                            }
                                                        
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }

}
