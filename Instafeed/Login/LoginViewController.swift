//
//  LoginViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 25/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import Firebase
import KRProgressHUD
import CoreLocation

class LoginViewController: UIViewController,NVActivityIndicatorViewable,CLLocationManagerDelegate
{

    let locationManager = CLLocationManager()

    var lat : String = ""
    var log : String = ""

    @IBOutlet weak var centerAlignUser: NSLayoutConstraint!
    @IBOutlet weak var centerAlignPassword: NSLayoutConstraint!
    @IBOutlet weak var centerAlignLogin: NSLayoutConstraint!
    
    
    @IBOutlet weak var forgotLabel: UILabel!
    @IBOutlet weak var orImagView: UIImageView!
    @IBOutlet weak var signUpLabel: UILabel!
    @IBOutlet weak var logInFbLabel: UILabel!
    @IBOutlet weak var fbImgView: UIImageView!
    
    
    
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var pwdTxt: UITextField!
    @IBOutlet weak var loginView: UIView!
    var indicator: NVActivityIndicatorView!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        //        lat = locValue.latitude
        //        lang = locValue.longitude
        
        lat = String(locValue.latitude)
        log = String(locValue.longitude)
        
        UserDefaults.standard.set(lat, forKey: currentLat)
        UserDefaults.standard.set(log, forKey: currentLong)
        
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil

        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        forgotLabel.alpha = 0.0
        orImagView.alpha = 0.0
        signUpLabel.alpha = 0.0
        logInFbLabel.alpha = 0.0
        fbImgView.alpha = 0.0
        
        centerAlignUser.constant += view.bounds.width
        centerAlignPassword.constant += view.bounds.width
        centerAlignLogin.constant += view.bounds.width
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations:
        {
            self.centerAlignUser.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        },
        completion:
        {
            (finished: Bool) in

        })
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations: {
            self.centerAlignPassword.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            self.centerAlignLogin.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)

        UIView.animate(withDuration: 0.5, delay: 0.7, options: .curveEaseOut, animations:
        {
            self.forgotLabel.alpha = 1.0

        }, completion: nil)
       
        UIView.animate(withDuration: 0.5, delay: 0.8, options: .curveEaseOut, animations:
        {
                self.orImagView.alpha = 1.0

        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.9, options: .curveEaseOut, animations:
        {
                self.logInFbLabel.alpha = 1.0
                self.fbImgView.alpha = 1.0

        }, completion: nil)
        
        
        UIView.animate(withDuration: 0.5, delay: 1.0, options: .curveEaseOut, animations: {

            self.signUpLabel.alpha = 1.0
            
        }, completion: nil)
        
    }
    
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        self.loginView.layer.borderWidth = 1
        self.loginView.layer.borderColor = UIColor(red:85/255, green:166/255, blue:218/255, alpha: 1).cgColor
    }
    
    @IBAction func loginBtn(_ sender: Any)
    {
        
        if userNameTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your name", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in

            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else if pwdTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter your password", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            SignIn()
        }
    }
    
    @IBAction func fbLoginBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "FacebookLoginViewController")as! FacebookLoginViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    @IBAction func signUpBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "EmailViewController")as! EmailViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    func SignIn()
    {
//        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Sign In...")
        
        let params: Parameters =
        [
            "email": userNameTxt.text!,
            "password":pwdTxt.text!,
        ]
        
        print(params)

        let url = BASE_URL + LOG_IN
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {

                    if let json = response.result.value
                    {
                        print("SIGN IN =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                           
                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")

                            UserDefaults.standard.set(jsonResponse["username"].stringValue, forKey: username)

                            UserDefaults.standard.set(jsonResponse["name"].stringValue, forKey: name)

                            let pro : String = UserDefaults.standard.object(forKey: username) as! String
                            
                            
                            
                            let id = jsonResponse["id"].stringValue
                            
                            UserDefaults.standard.set(id, forKey: userID)


                            print("USER ID = ",id)

                            
                            print("Username = ",pro)
                            
                            self.updateToken()
                            
                        }
                        else
                        {
                            
//                            self.indicator.stopAnimating()

                            
                            KRProgressHUD.dismiss()
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()

                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateToken()
    {
        
        
        var refreshedToken : String? = InstanceID.instanceID().token()

        
        if refreshedToken == nil
        {
            refreshedToken = ""
        }
            
                        
        
        var accessToken = UserDefaults.standard.string(forKey: "accessToken")

        if accessToken == nil
        {
            accessToken = ""
        }
        
        
        if UserDefaults.standard.object(forKey: "currentLat") != nil
        {
            lat =  UserDefaults.standard.object(forKey: currentLat) as! String
            log =  UserDefaults.standard.object(forKey: currentLong) as! String
        }
        else
        {
            lat = ""
            log = ""
        }
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "deviceToken":refreshedToken!,
            "appVersion":"0.1",
            "os":"iOS",
            "localLat":lat,
            "locallong":log
        ]
        
        
        print(params)
        let url = BASE_URL + DEVICE_TOKEN
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
//                    self.indicator.stopAnimating()
  
                    KRProgressHUD.dismiss()

                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
//                            UserDefaults.standard.set(jsonResponse["accessToken"].stringValue, forKey: "accessToken")
                  
                            
                            UserDefaults.standard.set(true, forKey: isLoggedIn)

                            
                            let alertController = UIAlertController(title: "Success", message: "Login Successful", preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                
                                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "PageViewController")as! PageViewController
                                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                                self.present(Dvc, animated: true, completion: nil)
                                
                                
                                
                                
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            KRProgressHUD.dismiss()
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    @IBAction func forgotPwd(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ForgotEmailViewController")as! ForgotEmailViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
}
