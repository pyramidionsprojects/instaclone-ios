//
//  ChangePasswordViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 15/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import AWSS3
import AWSCore
import KRProgressHUD
import SDWebImage
import JGProgressHUD
import McPicker

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var currentPwdTxt: UITextField!
    
    @IBOutlet weak var newPwdTxt: UITextField!
    
    @IBOutlet weak var confirmPwdTxt: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func doneBtn(_ sender: Any)
    {
        if currentPwdTxt.text! == ""
        {
            self.showAlert(title: "Oops", msg: "Enter current password")
        }
        else if newPwdTxt.text! == ""
        {
            self.showAlert(title: "Oops", msg: "Enter new password")
        }
        else if confirmPwdTxt.text! == ""
        {
            self.showAlert(title: "Oops", msg: "Enter confirm password")
        }
        else if confirmPwdTxt.text! != newPwdTxt.text!
        {
            self.showAlert(title: "Oops", msg: "Password not match")
        }
        else
        {
            self.updatePassword()
        }
    }
    
    
    @IBAction func cancelBtn(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func updatePassword()
    {
        
        KRProgressHUD.show(withMessage: "Updating...")
        
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "password":currentPwdTxt.text!,
            "newPassword":self.confirmPwdTxt.text!,
        ]
        
        print(params)
        let url = BASE_URL + EDITPASSWORD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            
                            self.dismiss(animated: true, completion: nil)
                            
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}
