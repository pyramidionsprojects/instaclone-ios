//
//  OptionsViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 14/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import AWSS3
import AWSCore
import KRProgressHUD
import SDWebImage
import JGProgressHUD


class OptionsViewController: UIViewController
{

    @IBOutlet weak var privateSwitchBtn: UISwitch!
    
    @IBOutlet weak var switchBusAccount: UIButton!
    
    var proStrImage: String = ""
    

    
    var privacySettings : String = "disabled"

    var businessStatus : String = "disabled"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        if privacySettings == "disabled"
        {
            privateSwitchBtn.isOn = false
        }
        else
        {
            privateSwitchBtn.isOn = true
        }
        
        
        if businessStatus == "disabled"
        {
            switchBusAccount.setTitle("Switch Business Account",for: .normal)
        }
        else
        {
            switchBusAccount.setTitle("Switch Personal Account",for: .normal)
        }
        
        
        
   
    }
    
    
    @IBAction func privateSwitch(_ sender: UISwitch)
    {
        if sender.isOn
        {
            self.updatePrivacy(state: "enabled")
        }
        else
        {
            self.updatePrivacy(state: "disabled")
        }
    }
    
    
    @IBAction func switchBusAtn(_ sender: Any)
    {
        if Reachability.isConnectedToNetwork()
        {
            if businessStatus == "disabled"
            {
                
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "BusinessContinueViewController")as! BusinessContinueViewController
                Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                Dvc.proImgString = self.proStrImage
                present(Dvc, animated: true, completion: nil)
                
            }
            else
            {
                //            self.updatePrivacy(state: "disabled")
                
                self.switchPersonal()
                
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Check your internet connection", preferredStyle: UIAlertControllerStyle.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    
    func switchPersonal()
    {
        
        KRProgressHUD.show(withMessage: "Updating...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "status":"disabled",
            "businessCategory":"",
            "businessContactNumber":"",
            "businessContactAddress":"",
            "businessWebsite":""
        ]
        
        print(params)
        let url = BASE_URL + SWICTHTOBUSINESSUSER
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    KRProgressHUD.dismiss()
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            self.businessStatus = "disabled"
                            
                            self.switchBusAccount.setTitle("Switch Business Account",for: .normal)
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
  
    
    
    
    func updatePrivacy(state :String)
    {
        
        KRProgressHUD.show(withMessage: "Updating...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "status":state
        ]
        
        print(params)
        let url = BASE_URL + PRIVACYSETTING
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    KRProgressHUD.dismiss()
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            if state == "enabled"
                            {
                                self.privateSwitchBtn.isOn = true
                            }
                            else
                            {
                                self.privateSwitchBtn.isOn = false
                            }
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    
    
    
    @objc func backAction(sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
        
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
//        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back_1")
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back_1")
        
        
        


    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
//        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
   
    @IBAction func changePasswordBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController")as! ChangePasswordViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    @IBAction func logOutBtn(_ sender: Any)
    {
        self.logout()
    }
    
    

    
    func logout()
    {
        
        KRProgressHUD.show(withMessage: "Logout...")
        
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!
            ]
        
        print(params)
        let url = BASE_URL + LOGOUT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    KRProgressHUD.dismiss()
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()

                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SplashViewController")as! SplashViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            let domain = Bundle.main.bundleIdentifier!
                            UserDefaults.standard.removePersistentDomain(forName: domain)
                            UserDefaults.standard.synchronize()
                            
                            
                            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SplashViewController")as! SplashViewController
                            Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(Dvc, animated: true, completion: nil)
                            
                            
/*                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)*/
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
/*                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)*/
                    
                    
                    let domain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: domain)
                    UserDefaults.standard.synchronize()
                    
                    
                    let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                    let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SplashViewController")as! SplashViewController
                    Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    self.present(Dvc, animated: true, completion: nil)
                    
                    
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    
    

}
