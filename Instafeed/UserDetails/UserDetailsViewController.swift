//
//  UserDetailsViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 15/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import KRProgressHUD
import SDWebImage
import Alamofire
import SwiftyJSON
import ActiveLabel
import ASPVideoPlayer

class UserDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,commentCountChangeDelegate,UIViewControllerPreviewingDelegate
{
    
    
    var currentTableIndex : Int = 0

    var forceTouchAvailable = false

    var followerID : String = ""

    
    var userID : String = ""

    var receiverID : String = ""

    
    @IBOutlet weak var privateView: UIView!
    
    @IBOutlet weak var publicView: UIView!
    
    
    
    @IBOutlet weak var videoPlayerTopView: UIView!
    @IBOutlet weak var videoPlayerMainView: UIView!
    
    @IBOutlet weak var videoPlayer: ASPVideoPlayer!

    
    
    
    func commentCount()
    {
        var dict = NSMutableDictionary()
        
        
        let indexPath = IndexPath(item: currentTableIndex, section: 0)
        
        let cell = listTableView.cellForRow(at: indexPath) as! FeedTableViewCell
        
        
        dict = NSMutableDictionary(dictionary: self.feedPosts[currentTableIndex] as! NSDictionary)
        
        
        print("Dict = ",dict)
        
        
        dict["total_comments"] = (dict["total_comments"] as! Int + 1)
        
        
        let pID = (dict["total_comments"] as! Int)
        
        
        print("Dict = ",pID)
        
        
        let id = String(describing: pID)
        
        
        cell.commentLabel.text = String(describing: id)
        
        
        self.feedPosts[currentTableIndex] = dict
        
    }
    
    
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var friendsView: UIView!
    
    var menuStatus = 0

    var userName : String = ""

    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 1
    
    var toBackComment : Int = 0

    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var listTableViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var gridCollectionView: UICollectionView!
    @IBOutlet weak var gridCollectHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var taggedCollectionView: UICollectionView!
    @IBOutlet weak var taggedCollectHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var gridLine: UIView!
    @IBOutlet weak var tblLine: UIView!
    @IBOutlet weak var favLine: UIView!
    
    
    @IBOutlet weak var userTaggedView: UIView!
    @IBOutlet weak var gridMainView: UIView!
    @IBOutlet weak var listMainView: UIView!
    
    
    @IBOutlet weak var navProView: UIView!
    @IBOutlet weak var navProImage: UIImageView!
    @IBOutlet weak var navUserName: UILabel!
    
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    
    @IBOutlet weak var bioLbl: UILabel!
    
    @IBOutlet weak var proImage: UIImageView!
    
    
    var feedPosts = Array<Any>()
    var userTaggedPosts = Array<Any>()

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        gridLine.isHidden = false
        tblLine.isHidden = true
        favLine.isHidden = true
        
        privateView.isHidden = true
        
        
        self.navUserName.text = ""
        
        self.userNameLbl.text = ""
        
        self.postCount.text =  "0"
        
        self.followersLbl.text = "0"
        
        self.followingLbl.text = "0"
        
        self.bioLbl.text = " - "
        
        listTableView.isScrollEnabled = false
        
        menuStatus = 0
        
        self.gridMainView.isHidden = false
        self.listMainView.isHidden = true
        self.userTaggedView.isHidden = true
        
        listTableView.estimatedRowHeight = 77
        listTableView.rowHeight = UITableViewAutomaticDimension
//        self.scrollView.addSubview(self.refreshControl)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        //        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1700)
        
        if toBackComment == 1
        {
            toBackComment = 0
        }
        else
        {
//            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
            
            
            let retVal = 126
            
            let size : CGFloat = CGFloat(retVal)
            
            self.gridCollectHeight.constant = size
            
            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size + 350)
            
            
            
            menuStatus = 0
            
            
            gridLine.isHidden = false
            tblLine.isHidden = true
            favLine.isHidden = true
            
            self.gridMainView.isHidden = false
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = true
            
            self.loadProfile()
            
        }
        
    }
    
    @IBAction func closeVideoPlayer(_ sender: Any)
    {
        videoPlayerTopView.isHidden = true
        videoPlayerMainView.isHidden = true
        
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
        //        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        //
        //
        //        videoPlayer.videoURLs = [firstVideoURL!]
        
        
        videoPlayer.videoPlayerControls.stop()
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.proImage.layer.cornerRadius = self.proImage.frame.size.width / 2
        self.proImage.layer.masksToBounds = true
        
        
        self.navProImage.layer.cornerRadius = self.navProImage.frame.size.width / 2
        self.navProImage.layer.masksToBounds = true
        
//        self.navProView.layer.cornerRadius = self.navProView.frame.size.width / 2
//
//        self.navProView.layer.borderWidth = 1.5
//        self.navProView.layer.borderColor = UIColor(hexString: "#0074BB").cgColor
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        NotificationCenter.default.post(name: Notification.Name("disableSwipeGesture"), object: nil)

    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func reportAtn(_ sender: Any)
    {
        if self.followerID == ""
        {
//            self.view.makeToast("Check Internet Connection")
        }
        else
        {
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Unfollow", style: .destructive, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.unFollowUser(postId: self.followerID)
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
            })
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }
    }
    
    
    func unFollowUser(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "followerId":postId
        ]
        
        print(params)
        let url = BASE_URL + UNFOLLOW
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            //                            self.loadFeeds()
                            
                            //                            self.navigationController?.popViewController(animated: true)
                            
                            self.loadProfile()
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func blockUser(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "userId":postId
        ]
        
        print(params)
        let url = BASE_URL + BLOCKUSER
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            //                            self.loadFeeds()
                            
                            //                            self.navigationController?.popViewController(animated: true)
                            
                            
                            NotificationCenter.default.post(name: Notification.Name("backToBlockUser"), object: nil)

                            
                            
                            self.navigationController?.popViewController(animated: true)
                            
                            
//                            self.loadProfile()
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func moreBtn(_ sender: Any)
    {
        if self.followerID == ""
        {
            //            self.view.makeToast("Check Internet Connection")
        }
        else
        {
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Block", style: .destructive, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                self.blockUser(postId: self.followerID)
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
            })
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
        }

    }
    
    
    @IBAction func gridBtn(_ sender: Any)
    {
        
        if menuStatus == 0
        {
            menuStatus = 0
        }
        else
        {
            
            menuStatus = 0
                                    
            gridLine.isHidden = false
            tblLine.isHidden = true
            favLine.isHidden = true
            
            self.gridMainView.isHidden = false
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = true
            
            self.loadProfile()
            
        }
        
    }
    
    @IBAction func tblBtn(_ sender: Any)
    {
        if menuStatus == 1
        {
            menuStatus = 1
        }
        else
        {
                        
            menuStatus = 1
            
            gridLine.isHidden = true
            tblLine.isHidden = false
            favLine.isHidden = true
            
            self.gridMainView.isHidden = true
            self.listMainView.isHidden = false
            self.userTaggedView.isHidden = true
            

            
            
            self.loadProfile()
            
            
        }
    }
    
    
    @IBAction func favBtn(_ sender: Any)
    {
        if menuStatus == 2
        {
            menuStatus = 2
        }
        else
        {
            menuStatus = 2
            
            gridLine.isHidden = true
            tblLine.isHidden = true
            favLine.isHidden = false
            
            self.gridMainView.isHidden = true
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = false
            
            self.loadTaggedPost()
        }
    }

    
    func loadTaggedPost()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "userId":userID,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + SEARCHUSERTAGGEDPOSTS
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                
                                
                                let feedPosts = myProfileArray[0]as! NSDictionary
                                
                                
                                
                                let follow = feedPosts["id"] as! Int
                                
                                self.followerID = String(describing: follow)
                                
                                self.receiverID = self.followerID

                                
                                let imgString = feedPosts["profilePic"]as? String ?? ""
                                
                                
                                self.followerID = String(describing: follow)
                                
                                
                                self.userID = String(describing: follow)
                                
                                
                                let privacy_settings = feedPosts["privacySettings"]as? String ?? ""
                                
                                
                                let follow_status = feedPosts["user_follow_status"]as? String ?? ""
                                
                                
                                if follow_status == "false"
                                {
                                    self.followView.isHidden = false
                                    self.friendsView.isHidden = true
                                }
                                else
                                {
                                    self.followView.isHidden = true
                                    self.friendsView.isHidden = false
                                }
                                
                                
                                if privacy_settings == "disabled"
                                {
                                    self.privateView.isHidden = true
                                    self.publicView.isHidden = false
                                }
                                else
                                {
                                    self.privateView.isHidden = false
                                    self.publicView.isHidden = true
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 150 + 350)
                                    
                                }
                                
                                
                                self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                
                                self.navUserName.text = feedPosts["name"]as? String ?? ""
                                
                                self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                
                                var photoCnt = feedPosts["photocount"] as! Int
                                
                                self.postCount.text = String(describing: photoCnt)
                                
                                photoCnt = feedPosts["total_followers"] as! Int
                                
                                self.followersLbl.text = String(describing: photoCnt)
                                
                                photoCnt = feedPosts["total_followings"] as! Int
                                
                                self.followingLbl.text = String(describing: photoCnt)
                                
                                self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    //                                    var myProfileArray = Array<Any>()
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                    
                                    
                                    self.userTaggedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                                    
                                    self.taggedCollectionView.reloadData()

                                    let x : Float = Float(self.userTaggedPosts.count) / Float(3)
                                    
                                    print("X Ans = ",x)
                                    
                                    print("X = ",self.feedPosts.count)
                                    
                                    let roundVal = x.rounded(.up)
                                    print("Round = ",roundVal)
                                    
                                    let retVal = (126  * roundVal)
                                    
                                    let size : CGFloat = CGFloat(retVal)
                                    
                                    self.taggedCollectHeight.constant = size
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size + 350)
                                 
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
//                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                }
                                else
                                {
                                        self.feedPosts.removeAll()
                                    self.taggedCollectionView.reloadData()

                                    let retVal = 126
                                    
                                    let size : CGFloat = CGFloat(retVal)
                                    
                                    self.taggedCollectHeight.constant = size

                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size + 350)
                                    
                                }
                                //                                self.newsFeedTblView.reloadData()
                            }
                            else
                            {
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                alertController.addAction(action1)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
    }
    
    
    
    @IBAction func followBtn(_ sender: Any)
    {
        followUser()
    }
    
    
    @IBAction func messageBtn(_ sender: Any)
    {
        
        if receiverID == ""
        {
            
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "ChatViewController")as! ChatViewController
            
            vc.receiverId = self.receiverID
            
            vc.name = self.userNameLbl.text!
            
    //        vc.privacySettings = self.privacy
    //
    //        vc.businessStatus = self.busStatus
    //
    //        vc.proStrImage = proStrImage
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func unfollowBtn(_ sender: Any)
    {
        unFollowUser()
    }
    
    
    func followUser()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + FOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                               self.loadProfile()
                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {

            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func unFollowUser()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + UNFOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
                                self.loadProfile()
                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
    }
    
    
    func loadProfile()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "username":userName,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + CLICKUSER
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>

                                
                                let feedPosts = myProfileArray[0]as! NSDictionary
                                
                                
                                let follow = feedPosts["id"] as! Int
                                
                                self.followerID = String(describing: follow)
                                
                                let imgString = feedPosts["profilePic"]as? String ?? ""

                                
                                self.followerID = String(describing: follow)
                                
                                self.receiverID = self.followerID

                                
                                self.userID = String(describing: follow)
                                
                                
                                let privacy_settings = feedPosts["privacySettings"]as? String ?? ""
                                
                                
                                let follow_status = feedPosts["user_follow_status"]as? String ?? ""
                                
                                
                                if follow_status == "false"
                                {
                                    self.followView.isHidden = false
                                    self.friendsView.isHidden = true
                                }
                                else
                                {
                                    self.followView.isHidden = true
                                    self.friendsView.isHidden = false
                                }

                                
                                
                                if privacy_settings == "disabled"
                                {
                                    self.privateView.isHidden = true
                                    self.publicView.isHidden = false
                                }
                                else
                                {
                                    self.privateView.isHidden = false
                                    self.publicView.isHidden = true
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 150 + 350)
                                    
                                }
                                
                                
                                
                                
                                
                                self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                
                                
                                self.navUserName.text = feedPosts["name"]as? String ?? ""
                                
                                self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                
                                var photoCnt = feedPosts["photocount"] as! Int
                                
                                self.postCount.text = String(describing: photoCnt)
                                
                                photoCnt = feedPosts["total_followers"] as! Int
                                
                                self.followersLbl.text = String(describing: photoCnt)
                                
                                photoCnt = feedPosts["total_followings"] as! Int
                                
                                self.followingLbl.text = String(describing: photoCnt)
                                
                                self.bioLbl.text = feedPosts["bio"]as? String ?? ""

                                
                                
                                
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                    
                                    self.feedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    if self.menuStatus == 1
                                    {
                                        
                                        self.listTableView.reloadData()

//                                        let i = Int(self.listTableView.frame.size.height) * self.feedPosts.count

                                        
                                        
                                        
                                        

                                        
                                        
                                        let sizes : CGFloat = CGFloat(484 * self.feedPosts.count)
                                        
                                        
                                        self.listTableViewHeight.constant = sizes

                                        
                                        
                                        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: sizes + 350 + 20)

                                        
                                        print("Uber = ", self.listTableView.contentSize.height)

                                        
                                        
                                        
//                                        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: sizes + 350)
                                        
                                    }
                                    else
                                    {
                                        
                                        self.gridCollectionView.reloadData()

                                        let x : Float = Float(self.feedPosts.count) / Float(3)
                                        
                                        print("X Ans = ",x)
                                        
                                        print("X = ",self.feedPosts.count)
                                        
                                        let roundVal = x.rounded(.up)
                                        print("Round = ",roundVal)
                                        
                                        let retVal = (126  * roundVal)
                                        
                                        let size : CGFloat = CGFloat(retVal)
                                        
                                        self.gridCollectHeight.constant = size
                                        
                                        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size + 350)
                                        
                                    }
                                    
 
                                    
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
                                    
                                    let follow = feedPosts["id"] as! Int
                                    
                                    self.followerID = String(describing: follow)
                                    
                                    self.receiverID = self.followerID

                                    
                                    self.userID = String(describing: follow)
                                    
                                    
                                    let privacy_settings = feedPosts["privacySettings"]as? String ?? ""

                                    
                                    let follow_status = feedPosts["user_follow_status"]as? String ?? ""

                                    
                                    if follow_status == "false"
                                    {
                                        self.followView.isHidden = false
                                        self.friendsView.isHidden = true
                                    }
                                    else
                                    {
                                        self.followView.isHidden = true
                                        self.friendsView.isHidden = false
                                    }
                                    
                                    
                                    if privacy_settings == "disabled"
                                    {
                                        self.privateView.isHidden = true
                                        self.publicView.isHidden = false
                                    }
                                    else
                                    {
                                        self.privateView.isHidden = false
                                        self.publicView.isHidden = true
                                        
                                        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 150 + 350)

                                    }
                                    
                                    
                                    
//                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                }
                                else
                                {
                                    //                                    self.feedPosts.removeAll()
                                }
                                
                                //                                self.newsFeedTblView.reloadData()
                                
                                
                            }
                            else
                            {
                              
                                
                                if self.menuStatus == 1
                                {
                                    
                                    self.feedPosts.removeAll()
                                    self.listTableView.reloadData()
                                }
                                else
                                {
                                    self.feedPosts.removeAll()
                                    self.gridCollectionView.reloadData()
                                    
                                }
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {

            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0)
        {
            if (min > 1)
            {
                timeAgo = "\(min) mins ago"
            }
            else
            {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedPosts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
        
        cell.delegate = self
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        

        
        
        let imgString = feedPosts["profilePic"]as? String ?? ""
        
        
        
        cell.proImageView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
        
        
        if feedPosts.object(forKey: "user_liked") as! String == "true"
        {
            cell.likeBtn.setImage(UIImage(named: "heart_filled"), for: UIControlState())
            
        }
        else
        {
            cell.likeBtn.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        
        if feedPosts.object(forKey: "user_pinned") as! String == "true"
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            
        }
        else
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
        }
        
        
        
        cell.bookmartBtn.addTarget(self, action: #selector(ProfileViewController.bookmartAtn(sender:)), for: .touchUpInside)
        
        cell.bookmartBtn.tag = indexPath.row
        
        
        cell.likeBtn.addTarget(self, action: #selector(ProfileViewController.likeBtn(sender:)), for: .touchUpInside)
        
        cell.likeBtn.tag = indexPath.row
        
        
        
        
        
        let cmtStatus = feedPosts["commentingStatus"] as! Int
        
        let commentStatus = String(describing: cmtStatus)
        
        cell.commentBtn.addTarget(self, action: #selector(ProfileViewController.commentBtn(_:)), for: .touchUpInside)
        
        cell.commentBtn.tag = indexPath.row

        if commentStatus == "1"
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = true
        }
        else
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = false
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        cell.reportBtn.addTarget(self, action: #selector(ProfileViewController.reportBtn(_:)), for: .touchUpInside)
        
        cell.reportBtn.tag = indexPath.row
        
        
        
        cell.selectionStyle = .none
        
        //        let Userimg = feedPosts["name"]as? String ?? ""
        //        Cell.imgprofile.sd_setImage(with: URL(string: Userimg as! String), placeholderImage: UIImage(named: "placeu"))
        
        //        var dct = self.feedPosts[indexPath.row] as! Dictionary<A
        
        cell.userNameLabel.text = feedPosts["name"]as? String ?? ""
        
        let currentDate = feedPosts["createdAt"]as? String
        
        cell.postAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        cell.addressLabel.text = feedPosts["city"]as? String ?? ""
        
        let like = feedPosts["total_likes"] as! Int
        
        cell.likeLabel.text = String(describing: like)
        
        
        let comment = feedPosts["total_comments"] as! Int
        
        
        cell.commentLabel.text = String(describing: comment)
        
        
        let userName = feedPosts["name"]as? String ?? ""
        
        let patterns = "\\s" + userName + "\\b"
        
        
        let customType = ActiveType.custom(pattern: userName) //Looks for "are"
        
        //        let customType = ActiveType.custom(pattern: patterns)
        
        cell.desLabel.enabledTypes.append(customType)
        
        
        cell.desLabel.customColor[customType] = UIColor.black
        
        
        cell.desLabel.configureLinkAttribute = { (type, attributes, isSelected) in
            var atts = attributes
            switch type {
            case customType:
                atts[NSAttributedStringKey.font] = UIFont.boldSystemFont(ofSize: 16)
            default: ()
            }
            
            return atts
        }
        
        
        
        let caption = feedPosts["caption"]as? String ?? ""
        
        
        
        let wholeCaption = " " + userName + " " + caption
        
        
        print("wholeCaption = ",wholeCaption)
        
        print("patterns = ",patterns)
        
        cell.desLabel.text  = wholeCaption
        
        
        //        cell.desLabel.handleMentionTap { self.alert("Mention", message: $0) }
        //        cell.desLabel.handleHashtagTap { self.alert("Hashtag", message: $0) }
        
        
        
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
//        CellAnimator.animate(cell, withDuration: 1, animation: CellAnimator.AnimationType(rawValue: 5)!)
        
        let cell = cell as! FeedTableViewCell
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        cell.feedContentPosts  = (feedPosts["linkData"]as? Array)!
        
        
        cell.collectionVIew.reloadData()
        //        cell.collectionVIew.contentOffset = .zero
        
        
        
        let lastcell = self.feedPosts.count - 2
        if indexPath.row == lastcell
        {
            if Total_Page_Count >= Current_page {
                Current_page = Current_page + 1
                print("CurrentPage\(Current_page)")
                
                
                
                
                //                UPDATE_FEED_API()
                //                Notification_Bar_API()
            }
        }
        
    }
    
    
    @objc func bookmartAtn(sender: SparkButton)
    {
        var dict = NSMutableDictionary()
        
        
        dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
        
        
        print("Dict = %@",dict)
        
        if (dict.object(forKey: "user_pinned") as! String == "true")
        {
            sender.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
            sender.likeBounce(0.6)
            sender.animate()
            
            dict["user_pinned"] = "false"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinRemovePost(postId: id)
            
        }
        else
        {
            sender.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            sender.unLikeBounce(0.4)
            
            dict["user_pinned"] = "true"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinPost(postId: id)
        }
        
        /*
         let indexPath = IndexPath(item: sender.tag, section: 0)
         
         
         let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell*/
        
        self.feedPosts[sender.tag] = dict
        
        
        /*        let like = (dict["total_likes"] as! Int)
         
         cell.likeLabel.text = String(describing: like)
         */
        
        
        
    }
    
    
    @objc func viewProfileBtn(_ sender: AnyObject)
    {
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = dict["username"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func reportBtn(_ sender: AnyObject)
    {
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Report", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.report(postid: id, currentIndex: sender.tag)
        })
        
        let deleteAction = UIAlertAction(title: "Unfollow", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let pID = (dict["postedBy"] as! Int)
            
            let id = String(describing: pID)
            
            self.unFollowPost(postId: id)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    func unFollowPost(postId :String)
    {        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "followerId":postId
        ]
        
        print(params)
        let url = BASE_URL + UNFOLLOW
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
//                            self.loadFeeds()
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    func report(postid :String,currentIndex :Int)
    {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "It's inappropriate", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.listTableView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let deleteAction = UIAlertAction(title: "It's spam", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.listTableView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
    
    
    @objc func commentBtn(_ sender: AnyObject)
    {
        print("Comment Button Tag = ",sender.tag)
        
        
        currentTableIndex = sender.tag
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        vc.commentDelegate = self
        
        
        toBackComment = 1

        
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
        
        /*
         videoPlayerTopView.isHidden = false
         videoPlayerMainView.isHidden = false
         
         self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
         
         UIView.animate(withDuration: 2.0,
         delay: 0,
         usingSpringWithDamping: CGFloat(0.20),
         initialSpringVelocity: CGFloat(6.0),
         options: UIViewAnimationOptions.allowUserInteraction,
         animations: {
         self.videoPlayerMainView.transform = CGAffineTransform.identity
         },
         completion: { Void in()  }
         )
         */
    }
    
    
    @objc func likeBtn(sender: SparkButton)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            print("Like Button Tag = ",sender.tag)
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                sender.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                sender.setImage(UIImage(named: "heart_filled"), for: UIControlState())
                sender.unLikeBounce(0.4)
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            
            let indexPath = IndexPath(item: sender.tag, section: 0)
            
            
            let cell = listTableView.cellForRow(at: indexPath) as! FeedTableViewCell
            
            self.feedPosts[sender.tag] = dict
            
            
            let like = (dict["total_likes"] as! Int)
            
            cell.likeLabel.text = String(describing: like)
            
            
            
            
        }
    }
    
    
    func pinRemovePost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + REMOVEFROMPINBORAD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func pinPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + ADDTOPINBOARD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func reportPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId,
            "reason":"not allowed"
        ]
        
        print(params)
        let url = BASE_URL + REPORT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard #available(iOS 9.0, *) else {
            return nil
        }
        
        //        let indexPath = collectionview.indexPathForItem(at: collectionview.convert(location, from:view))
        //        if let indexPath = indexPath {
        //            let imageName = img[(indexPath as NSIndexPath).item]
        //            if let cell = collectionview.cellForItem(at: indexPath) {
        //                previewingContext.sourceRect = cell.frame
        //
        //                //                let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //                //                controller.imagenames = imageName
        //                //                return controller
        //            }
        //        }
        
        return nil
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        //        let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //        controller.imagenames = (viewControllerToCommit as! DetailsVC).imagenames
        //
        //        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    @objc func userLongPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = taggedCollectionView.indexPath(for: cell)
        {
            
            let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
            
            print("FeedPosts = %@",feedPosts);
            
            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            var imageName : String = ""
            
            
            if feedContentPosts.count > 1
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    imageName = linkData
                    
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    
                    imageName = linkData
                    
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }
            
            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            
            // you can set different frame for each peek view here
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            
            let cmtStatus = feedPosts["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            
            print("commentStatus = ",commentStatus)
            
            if commentStatus == "1"
            {
            
            
            
            if feedPosts.object(forKey: "user_liked") as! String == "true"
            {
                
                let options = [
                    PeekViewAction(title: "Un Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.userLikeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.userCommentBtn(row: indexPath.row)
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
            }
            else
            {
                
                let options = [
                    PeekViewAction(title: "Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.userLikeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.userCommentBtn(row: indexPath.row)
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
                
                
            }
            }
            else
            {
                
                
                
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.userLikeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.userCommentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                }
                else
                {
                    
                    let options = [
                        PeekViewAction(title: "Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.userLikeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.userCommentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                    
                }
            }
            
            
        }
    }
    
    @objc func longPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = gridCollectionView.indexPath(for: cell)
        {
            
            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
            
            print("FeedPosts = %@",feedPosts);
            
            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            var imageName : String = ""
            
            
            if feedContentPosts.count > 1
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    imageName = linkData
                    
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    
                    imageName = linkData
                    
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }
            
            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            
            // you can set different frame for each peek view here
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            let cmtStatus = feedPosts["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            
            print("commentStatus = ",commentStatus)
            
            if commentStatus == "1"
            {
            
            
            
            if feedPosts.object(forKey: "user_liked") as! String == "true"
            {
                
                let options = [
                    PeekViewAction(title: "Un Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
            }
            else
            {
                
                let options = [
                    PeekViewAction(title: "Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                        
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
                
                
            }
            }
            else
            {
                
                
                
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                }
                else
                {
                    
                    let options = [
                        PeekViewAction(title: "Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                            
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                    
                }
            }
            
            
        }
    }
    
    func commentBtn(row :Int)
    {
        currentTableIndex = row
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.feedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func likeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.feedPosts[row] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            
            
            
            
            self.feedPosts[row] = dict
            
        }
    }
    
    
    func userLikeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.userTaggedPosts[row] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            self.userTaggedPosts[row] = dict
            
        }
    }
    
    
    func userCommentBtn(row :Int)
    {
        currentTableIndex = row
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.userTaggedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    

}


extension UserDetailsViewController: UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
//        cell.alpha = 0
//        UIView.animate(withDuration: 0.8) {
//            cell.alpha = 1
//        }
//        
        
        if collectionView == gridCollectionView
        {
            if forceTouchAvailable == false {
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(_:)))
                gesture.minimumPressDuration = 0.5
                cell.addGestureRecognizer(gesture)
            }
        }
        else if collectionView == taggedCollectionView
        {
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(userLongPressCell(_:)))
            gesture.minimumPressDuration = 0.5
            cell.addGestureRecognizer(gesture)
            
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == gridCollectionView
        {
            return self.feedPosts.count
        }
        else
        {
            return self.userTaggedPosts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if gridCollectionView == collectionView
        {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectionViewCell", for: indexPath) as! GridCollectionViewCell
            
            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
            
            print("feedPosts = ",feedPosts)
            
            
            var feedContentPosts = Array<Any>()

            feedContentPosts = (feedPosts["linkData"]as? Array)!


            print("feedContentPosts = ",feedContentPosts)
        
            var linkData = NSDictionary()
            
            if feedContentPosts.count > 0
            {
                
                if feedContentPosts.count > 1
                {
                    cell.multipleCount.isHidden = false
                }
                else
                {
                    cell.multipleCount.isHidden = true
                }
                
                
                linkData = feedContentPosts[0]as! NSDictionary
                
            }
            else
            {
                cell.multipleCount.isHidden = true
            }
        
            
                    print("linkData = ",linkData)
            
            
            let type = linkData["type"]as? String ?? ""
            
            
            print("Type = ",type)
            
            
            if type == "image"
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
                
            }
            else if type == "video"
            {
                let linkData = linkData["thumbnail"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            else
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            
            return cell
            
            
        }
       else
        {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserTaggedCollectionViewCell", for: indexPath) as! UserTaggedCollectionViewCell
            
            let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
            
            
            print("feedPosts = ",feedPosts)
            
            
            var feedContentPosts = Array<Any>()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            
            
            if feedContentPosts.count > 0
            {
                
                if feedContentPosts.count > 1
                {
                    cell.multipleCount.isHidden = false
                }
                else
                {
                    cell.multipleCount.isHidden = true
                }
                
            }
            else
            {
                cell.multipleCount.isHidden = true
            }
            
            print("feedContentPosts = ",feedContentPosts)
            
            let linkData = feedContentPosts[0]as! NSDictionary
            
            
            //        print("linkData = ",linkData)
            
            
            let type = linkData["type"]as? String ?? ""
            
            
            
            print("Type = ",type)
            
            
            
            if type == "image"
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
                
            }
            else if type == "video"
            {
                let linkData = linkData["thumbnail"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            else
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            
            return cell
            
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        
        if gridCollectionView == collectionView
        {
            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
            
            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
            
            
            toBackComment = 1
            
            let like = feedPosts["id"] as! Int
            
            vc.detailPostId = String(describing: like)
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
            
            let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
            
            toBackComment = 1
            
            let like = feedPosts["id"] as! Int
            
            vc.detailPostId = String(describing: like)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        /*        if let _ = scrollView as? UITableView {
         
         let  height = scrollView.frame.size.height
         let contentYoffset = scrollView.contentOffset.y
         let distanceFromBottom = scrollView.contentSize.height - contentYoffset
         if distanceFromBottom < height {
         print(" you reached end of the table")
         
         
         self.topView.isHidden = true
         self.lineView.isHidden = true
         
         self.view.layoutIfNeeded()
         hghtConst.constant = 0
         
         
         
         UIView.animate(withDuration: 0.4, animations: {
         self.view.layoutIfNeeded()
         })
         
         }
         }
         else if let _ = scrollView as? UICollectionView
         {
         
         let  height = scrollView.frame.size.height
         let contentYoffset = scrollView.contentOffset.y
         let distanceFromBottom = scrollView.contentSize.height - contentYoffset
         if distanceFromBottom < height {
         print(" you reached end of the table")
         
         
         self.topView.isHidden = true
         self.lineView.isHidden = true
         
         self.view.layoutIfNeeded()
         hghtConst.constant = 0
         
         
         
         UIView.animate(withDuration: 0.4, animations: {
         self.view.layoutIfNeeded()
         })
         
         }
         }*/
        
    }
    
}

extension UserDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3 - 1, height: collectionView.frame.size.width / 3 - 1)
    }
    
}



extension UserDetailsViewController: FeedTableViewCellDelegate
{
    func goToCommentVC()
    {
        
    }
    
    func goToProfileUserVC() {
        
    }
    
    func playVideo(url :String)
    {
        
        
         videoPlayerTopView.isHidden = false
         videoPlayerMainView.isHidden = false
         
/*         self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
         
         UIView.animate(withDuration: 2.0,
         delay: 0,
         usingSpringWithDamping: CGFloat(0.20),
         initialSpringVelocity: CGFloat(6.0),
         options: UIViewAnimationOptions.allowUserInteraction,
         animations: {
         self.videoPlayerMainView.transform = CGAffineTransform.identity
         },
         completion:
         {
         Void in()
         
         let firstVideoURL  = URL(string: url)                 //returns a valid URL
         
         
         self.videoPlayer.videoURLs = [firstVideoURL!]
         
         
         self.videoPlayer.videoPlayerControls.play()
         
         
         })*/
        
        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        
        
        self.videoPlayer.videoURLs = [firstVideoURL!]
        
        
        self.videoPlayer.videoPlayerControls.play()
         
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
        
    }
    
}
