//
//  ChatListTableViewCell.swift
//  Snipofeed
//
//  Created by Pyramidions on 22/07/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var onlineStatus: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.height/2
        self.profilePic.clipsToBounds = true

        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
