//
//  ChatListViewController.swift
//  Snipofeed
//
//  Created by Pyramidions on 22/07/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import KRProgressHUD


class ChatListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
{
    
    
    var searchActive : Bool = false

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var msgList = Array<Any>()

    var filtered = Array<Any>()

    
    override func viewDidLoad() {
        super.viewDidLoad()


        tableView.separatorStyle = .none

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        NotificationCenter.default.post(name: Notification.Name("enableSwipeGesture"), object: nil)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.getChatsList()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    
    func getChatsList()
    {
        
        if let accesstoken = UserDefaults.standard.string(forKey: "accessToken")
        {
           
            let params: Parameters = [
                "accessToken": accesstoken
            ]
            
            print("Params = ",params)

            
            let url = BASE_URL + USERMSGLIST
            
            
            let Headers: HTTPHeaders = [
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            
            
            KRProgressHUD.show(withMessage: "Loading...")

            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                { response in
                    
                    KRProgressHUD.dismiss()
                    
                    //                    self.indicator.stopAnimating()
                    if(response.result.isSuccess)
                    {
                        if let json = response.result.value
                        {
                            print("CHAT SCREEN JSON: \(json)") // serialized json response
                            let jsonResponse = JSON(json)
                        
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                self.msgList = JSON.object(forKey: "msglist")as! Array<Any>
                                
                                self.tableView.reloadData()
                            }
                            else
                            {
                                self.showAlert(title: "Oops", msg: jsonResponse["message"].stringValue)
                            }
                        }
                    }
                    else{
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                        
                    }
            }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if(searchActive)
        {
            return filtered.count
        }
        else
        {
            return msgList.count
        }
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell", for: indexPath) as! ChatListTableViewCell
        
        var feedPosts = NSDictionary()
        
        if(searchActive)
        {
            feedPosts = self.filtered[indexPath.row]as! NSDictionary
        }
        else
        {
            feedPosts = self.msgList[indexPath.row]as! NSDictionary
        }

        let profilePic = feedPosts["profilePic"]as? String ?? ""
        
        let name = feedPosts["name"]as? String ?? ""

        cell.userNameLbl.text = name

        cell.selectionStyle = .none
        
        
        let status = feedPosts["onlineStatus"]as? String ?? ""
        
        if status == "1"
        {
            cell.onlineStatus.image = #imageLiteral(resourceName: "greenDot")
        }
        else
        {
            cell.onlineStatus.image = #imageLiteral(resourceName: "grayDot")
        }
      
        cell.profilePic.sd_setImage(with: URL(string: profilePic), placeholderImage: UIImage(named: "default-user-image"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        var feedPosts = NSDictionary()
        
        if(searchActive)
        {
            feedPosts = self.filtered[indexPath.row]as! NSDictionary
        }
        else
        {
            feedPosts = self.msgList[indexPath.row]as! NSDictionary
        }
        
        
        let id = feedPosts["id"] as! Int
        
        let receiverID = String(describing: id)
        
        let name = feedPosts["name"]as? String ?? ""

        
        
        if receiverID == ""
        {
            
        }
        else
        {
            
            
            
            self.searchBarCancelButtonClicked(self.searchBar)
            
            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "ChatViewController")as! ChatViewController            
            vc.receiverId = receiverID
            vc.name = name
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        NotificationCenter.default.post(name: Notification.Name("BackPageViewController"), object: nil)
    }
    
    
    @IBAction func addFollowers(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "ShowChatFollowersController")as! ShowChatFollowersController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableView.resignFirstResponder()
        self.searchBar.showsCancelButton = false
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        self.searchActive = true;
        self.searchBar.showsCancelButton = true
        
        
        filtered.removeAll()
        
        
        getSearchArrayContains(searchText)
        
        self.tableView.reloadData()
        
        }
    
    
    func getSearchArrayContains(_ text : String)
    {
        let predicate=NSPredicate(format: "name CONTAINS[cd] %@", text)
        let arr=(msgList as NSArray).filtered(using: predicate)
        filtered.removeAll()
     
        for i in 0..<arr.count
        {
            let result = arr[i] as! NSDictionary
            
            filtered.append(result)
        }
    }
}


