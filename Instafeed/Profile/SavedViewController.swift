//
//  SavedViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 14/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import KRProgressHUD
import SDWebImage
import Alamofire
import SwiftyJSON
import ActiveLabel

class SavedViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIViewControllerPreviewingDelegate
{
    @IBOutlet weak var savedCollectionView: UICollectionView!
            
    var pinnedPosts = Array<Any>()
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 1

    var forceTouchAvailable = false

    var toComeBack : Int = 0

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.savedCollectionView.delegate = self
        self.savedCollectionView.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)

        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        if toComeBack == 0
        {
            self.loadProfile()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func loadProfile()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + SHOWPINNED
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    //                                    var myProfileArray = Array<Any>()
                                    
                                    self.pinnedPosts = JSON.object(forKey: "pinnedPosts")as! Array<Any>
                                    
                                    
                                    self.savedCollectionView.reloadData()
                                    
                                    
                                }
                                else
                                {
                                    //                                    self.feedPosts.removeAll()
                                }
                                
                                //                                self.newsFeedTblView.reloadData()
                                
                                
                            }
                            else
                            {
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
//        cell.alpha = 0
//        UIView.animate(withDuration: 0.8) {
//            cell.alpha = 1
//        }
        
        if collectionView == savedCollectionView
        {
            if forceTouchAvailable == false {
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(_:)))
                gesture.minimumPressDuration = 0.5
                cell.addGestureRecognizer(gesture)
            }
        }
    }
    
    
    
    @objc func longPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = savedCollectionView.indexPath(for: cell)
        {
            
            let feedPosts = self.pinnedPosts[indexPath.row]as! NSDictionary
            
            print("FeedPosts = %@",feedPosts);
            
            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            var imageName : String = ""
            
            
            if feedContentPosts.count > 1
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    imageName = linkData
                    
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    
                    imageName = linkData
                    
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }
            
            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            
            // you can set different frame for each peek view here
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            let cmtStatus = feedPosts["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            
            print("commentStatus = ",commentStatus)
            
            if commentStatus == "1"
            {
            
            if feedPosts.object(forKey: "user_liked") as! String == "true"
            {
                
                let options = [
                    PeekViewAction(title: "Un Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
            }
            else
            {
                
                let options = [
                    PeekViewAction(title: "Like", style: .default),
                    PeekViewAction(title: "Comment", style: .default)]
                
                
                
                PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                    switch optionIndex
                    {
                    case 0:
                        print("Option 1 selected")
                        self.likeBtn(row: indexPath.row)
                        
                    case 1:
                        print("Option 2 selected")
                        self.commentBtn(row: indexPath.row)
                        
                    default:
                        break
                    }
                }, dismissHandler: {
                    print("Peekview dismissed!")
                })
                
                
            }
            
            }
            else
            {
                
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                }
                else
                {
                    
                    let options = [
                        PeekViewAction(title: "Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                            
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                    
                }
                
            }
            
        }
    }
    
    
    func commentBtn(row :Int)
    {
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.pinnedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
//        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func postLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    func likeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.pinnedPosts[row] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            
            self.pinnedPosts[row] = dict
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
            return self.pinnedPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
       
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SavedCollectionViewCell", for: indexPath) as! SavedCollectionViewCell
            
            let feedPosts = self.pinnedPosts[indexPath.row]as! NSDictionary
            
            
            print("feedPosts = ",feedPosts)
            
            
            var feedContentPosts = Array<Any>()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
        
        
            if feedContentPosts.count > 0
            {
            
            print("feedContentPosts = ",feedContentPosts)
            
            let linkData = feedContentPosts[0]as! NSDictionary
            
            
            //        print("linkData = ",linkData)
            
            
            let type = linkData["type"]as? String ?? ""
            
            
            
            print("Type = ",type)
            
            
            
            if type == "image"
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
                
            }
            else if type == "video"
            {
                let linkData = linkData["thumbnail"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
            else
            {
                let linkData = linkData["linkData"]as? String ?? ""
                
                cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                
            }
                
        }
        
        return cell

        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
            
            let feedPosts = self.pinnedPosts[indexPath.row]as! NSDictionary
        
            let like = feedPosts["id"] as! Int
        
            toComeBack = 1
        
            vc.detailPostId = String(describing: like)
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard #available(iOS 9.0, *) else {
            return nil
        }
        
        //        let indexPath = collectionview.indexPathForItem(at: collectionview.convert(location, from:view))
        //        if let indexPath = indexPath {
        //            let imageName = img[(indexPath as NSIndexPath).item]
        //            if let cell = collectionview.cellForItem(at: indexPath) {
        //                previewingContext.sourceRect = cell.frame
        //
        //                //                let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //                //                controller.imagenames = imageName
        //                //                return controller
        //            }
        //        }
        
        return nil
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        //        let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //        controller.imagenames = (viewControllerToCommit as! DetailsVC).imagenames
        //
        //        navigationController?.pushViewController(controller, animated: true)
    }
    
    
}

extension SavedViewController: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width / 3 - 1, height: collectionView.frame.size.width / 3 - 1)
    }
}



