//
//  GridCollectionViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 04/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var multipleCount: UIImageView!
}
