//
//  ProfileViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 04/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import KRProgressHUD
import SDWebImage
import Alamofire
import SwiftyJSON
import ActiveLabel
import ASPVideoPlayer


class ProfileViewController: UIViewController,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,commentCountChangeDelegate, UIViewControllerPreviewingDelegate
{
    
    
    var sharedInstance = BusinessSharedManager.sharedInstance

    
    var currentTableIndex : Int = 0
    
    var privacy : String = ""

    var busStatus : String = ""

    var forceTouchAvailable = false

    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var videoPlayerTopView: UIView!
    @IBOutlet weak var videoPlayerMainView: UIView!
    
    @IBOutlet weak var videoPlayer: ASPVideoPlayer!
    
    
    
    func commentCount()
    {
        var dict = NSMutableDictionary()
        
        
        let indexPath = IndexPath(item: currentTableIndex, section: 0)
        
        let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell
        
        
        dict = NSMutableDictionary(dictionary: self.feedPosts[currentTableIndex] as! NSDictionary)
        
        
        print("Dict = ",dict)
        
        
        dict["total_comments"] = (dict["total_comments"] as! Int + 1)
        
        
        let pID = (dict["total_comments"] as! Int)
        
        
        print("Dict = ",pID)
        
        
        let id = String(describing: pID)
        
        
        cell.commentLabel.text = String(describing: id)
        
        
        self.feedPosts[currentTableIndex] = dict
        
        
        
    }
    
    
    
    var menuStatus = 0
    
    
    
    @IBOutlet weak var userTaggedView: UIView!
    @IBOutlet weak var gridMainView: UIView!
    @IBOutlet weak var listMainView: UIView!
    
    
    @IBOutlet weak var proSingleView: UIView!
    @IBOutlet weak var proAllView: UIView!
    @IBOutlet weak var promoteView: UIView!
//    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var newsFeedTblView: UITableView!
    
    
    @IBOutlet var editView: [UIView]!
    
    
    
    
    @IBOutlet weak var gridLine: UIView!
    @IBOutlet weak var tblLine: UIView!
    @IBOutlet weak var favLine: UIView!
    @IBOutlet weak var bookView: UIView!

    var proStrImage: String = ""

    
    var feedPosts = Array<Any>()
    
    var userTaggedPosts = Array<Any>()

    
    @IBOutlet weak var navProView: UIView!
    @IBOutlet weak var navProImage: UIImageView!
    @IBOutlet weak var navUserName: UILabel!
    
    
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    
    @IBOutlet weak var bioLbl: UILabel!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var gridView: UIView!
    @IBOutlet weak var gridCollectionView: UICollectionView!
    
    @IBOutlet weak var userTaggedCollectionView: UICollectionView!
    
    @IBOutlet weak var hghtConst: NSLayoutConstraint!
    
    
    
    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(ProfileViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            
            return refreshControl
    }()
    
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 1

    var toBackComment : Int = 0

    
    var toBackDetail : Int = 0

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gridLine.isHidden = false
        tblLine.isHidden = true
        favLine.isHidden = true
        bookView.isHidden = true
        
        
        
        self.navUserName.text = ""
        
        self.userNameLbl.text = ""
        
        self.postCount.text =  "0"
        
        self.followersLbl.text = "0"
        
        self.followingLbl.text = "0"
        
        self.bioLbl.text = " - "
        
        
        
        newsFeedTblView.isScrollEnabled = false

        
        menuStatus = 0
       
        
        
        self.gridMainView.isHidden = false
        self.listMainView.isHidden = true
        self.userTaggedView.isHidden = true
        
        
        
        newsFeedTblView.estimatedRowHeight = 77
        newsFeedTblView.rowHeight = UITableViewAutomaticDimension
        
        
        
        
        self.scrollView.addSubview(self.refreshControl)
        
        
       
            
//            self.hghtConst.constant = 573
//
//            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)

       /*
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.up
        
        swipeRight.delegate = self
        
        self.swipeView.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        
        swipeDown.delegate = self
        self.swipeView.addGestureRecognizer(swipeDown)*/
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute:
            {
                self.pullToloadFeeds()
        })
        
        
//        self.refreshControl.endRefreshing()
        
//        self.pullToloadFeeds()
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
//        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 1700)
        
        if toBackComment == 1
        {
            toBackComment = 0
            toBackDetail = 0
        }
        else
        {
            
            self.hghtConst.constant = 573
            
            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
                        
            menuStatus = 0
        
            gridLine.isHidden = false
            tblLine.isHidden = true
            favLine.isHidden = true
            bookView.isHidden = true
            
            self.gridMainView.isHidden = false
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = true
            
            self.loadProfile()
            
        }

    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        
        NotificationCenter.default.post(name: Notification.Name("disableSwipeGesture"), object: nil)

        

        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        
//        let yourBackImage = UIImage(named: "back_1")
//        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
//        self.navigationController?.navigationBar.backItem?.title = ""
//        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        
    }
    
    
    /*
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                
                
                topView.isHidden = false
                self.lineView.isHidden = false
                
                
                view.layoutIfNeeded()
                hghtConst.constant = 230
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                
                
            case UISwipeGestureRecognizerDirection.left:
                 print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                
                
                

                self.topView.isHidden = true
                self.lineView.isHidden = true
                
                view.layoutIfNeeded()
                hghtConst.constant = 0
                
                
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                
                
          
                
            default:
                break
            }
        }
    }*/
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.proImage.layer.cornerRadius = self.proImage.frame.size.width / 2
        self.proImage.layer.masksToBounds = true
        
        
        self.navProImage.layer.cornerRadius = self.navProImage.frame.size.width / 2
        self.navProImage.layer.masksToBounds = true

        self.navProView.layer.cornerRadius = self.navProView.frame.size.width / 2
        self.navProView.layer.borderWidth = 1.5
        self.navProView.layer.borderColor = UIColor(hexString: "#0074BB").cgColor
        
        
        self.editView[0].layer.borderWidth = 1
        self.editView[0].layer.borderColor = UIColor.gray.cgColor

        self.editView[1].layer.borderWidth = 1
        self.editView[1].layer.borderColor = UIColor.gray.cgColor


        self.promoteView.layer.borderWidth = 1
        self.promoteView.layer.borderColor = UIColor.gray.cgColor

        
        
    }
    
    
    @IBAction func closeVideoPlayer(_ sender: Any)
    {
        videoPlayerTopView.isHidden = true
        videoPlayerMainView.isHidden = true
        
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
        //        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        //
        //
        //        videoPlayer.videoURLs = [firstVideoURL!]
        
        
        videoPlayer.videoPlayerControls.stop()
        
    }
    
        
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func moreBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "OptionsViewController")as! OptionsViewController
        
        vc.privacySettings = self.privacy

        vc.businessStatus = self.busStatus
        
        vc.proStrImage = proStrImage

        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func promoteBtn(_ sender: Any)
    {
        
        sharedInstance.clearData()
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "CreatePromoteNav")as! CreatePromoteNav
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    
    @IBAction func editBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "EditProfileViewController")as! EditProfileViewController
        Dvc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(Dvc, animated: true, completion: nil)
    }
    
    
    @IBAction func gridBtn(_ sender: Any)
    {
        
        if menuStatus == 0
        {
            menuStatus = 0
        }
        else
        {
            
            menuStatus = 0
            
            gridLine.isHidden = false
            tblLine.isHidden = true
            favLine.isHidden = true
            bookView.isHidden = true
            
            self.gridMainView.isHidden = false
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = true

            
//            self.gridCollectionView.reloadData()
            
            self.loadProfile()
            
//            self.gridCollectionView.reloadData()
        }

    }
    
    
    @IBAction func tblBtn(_ sender: Any)
    {
        if menuStatus == 1
        {
            menuStatus = 1
        }
        else
        {
            
            
          
            
            
            menuStatus = 1
        
            gridLine.isHidden = true
            tblLine.isHidden = false
            favLine.isHidden = true
            bookView.isHidden = true
            
            self.gridMainView.isHidden = true
            self.listMainView.isHidden = false
            self.userTaggedView.isHidden = true

//            self.newsFeedTblView.reloadData()

            
            
            self.loadProfile()

            
        }
    }
    
    
    @IBAction func favBtn(_ sender: Any)
    {
        if menuStatus == 2
        {
            menuStatus = 2
        }
        else
        {
            
            
        
            
            
            menuStatus = 2
        
            gridLine.isHidden = true
            tblLine.isHidden = true
            favLine.isHidden = false
            bookView.isHidden = true
            
            
            self.gridMainView.isHidden = true
            self.listMainView.isHidden = true
            self.userTaggedView.isHidden = false
            
            
            self.loadTaggedPost()
            
        }
    }
    
    @IBAction func bookBtn(_ sender: Any)
    {
        
        if menuStatus == 3
        {
            menuStatus = 3
        }
        else
        {
            
//            menuStatus = 0
//
//            loadProfile()
        
            gridLine.isHidden = true
            tblLine.isHidden = true
            favLine.isHidden = true
            bookView.isHidden = false
            
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "SavedViewController")as! SavedViewController
            
            toBackComment = 1

            self.navigationController?.pushViewController(vc, animated: true)            
        }
    }
    
    
    
    func loadTaggedPost()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,    
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + USERTAGGEDPOST
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    //                                    var myProfileArray = Array<Any>()
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                    
                                    
                                    self.userTaggedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                                    
                                    
                                    //                                    self.newsFeedTblView.reloadData()
                                    
                                    
                                    
                                    
                                        
                                        let x : Float = Float(self.userTaggedPosts.count) / Float(3)
                                        
                                        print("X Ans = ",x)
                                        
                                        print("X = ",self.feedPosts.count)
                                        
                                        let roundVal = x.rounded(.up)
                                        print("Round = ",roundVal)
                                        
//                                        let retVal = 230 + (126  * roundVal) + 60

                                    
                                        let retVal = (126  * roundVal) + 60

                                        
                                        let size : CGFloat = CGFloat(retVal)
                                        
                                        
//                                        if size > 573
//                                        {
                                    
                                            
                                            //                                        self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            //                                        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            
                                            self.view.layoutIfNeeded()
                                            
//                                        }
//                                        else
//                                        {
//                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
//
//                                        }
                                    
                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                    
                                    
                                    self.userTaggedCollectionView.reloadData()
                                    
                                    
                                    
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
                                    self.privacy = feedPosts["privacySettings"]as? String ?? ""

                                    self.busStatus = feedPosts["businessStatus"]as? String ?? ""
                                    
                                    if self.busStatus == "disabled"
                                    {
                                        self.proAllView.isHidden = true
                                        self.proSingleView.isHidden = false
                                    }
                                    else
                                    {
                                        self.proAllView.isHidden = false
                                        self.proSingleView.isHidden = true

                                    }

                                    
                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                }
                                else
                                {
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    self.hghtConst.constant = 126
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 126)

                                    
                                    
                                    //                                    self.feedPosts.removeAll()
                                }
                                
                                //                                self.newsFeedTblView.reloadData()
                                
                                
                            }
                            else
                            {
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    
    func loadProfile()
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + MYPROFILE
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
            {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    //                                    var myProfileArray = Array<Any>()
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                    
                                    self.feedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                                    
//                                    self.newsFeedTblView.reloadData()
                                    
                                    
                                    
                                    if self.menuStatus == 1
                                    {
                                        
                                        
//                                        let size : CGFloat = CGFloat((484 * self.feedPosts.count) + 290) + 60

                                        
                                        let size : CGFloat = CGFloat((484 * self.feedPosts.count)) + 60

                                        
                                        if size > 573
                                        {
                                            
                                        
//                                        self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                        
                                        
                                        self.hghtConst.constant = size
                                        
                                        self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                        
//                                        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)

                                        
                                        
                                        self.view.layoutIfNeeded()
                                        
                                        }
                                        else
                                        {
                                            
                                            let size : CGFloat = CGFloat((484 * self.feedPosts.count)) + 60

                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)

                                            
//                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
//
//                                            self.hghtConst.constant = 573

                                        }
                                        
                                        self.newsFeedTblView.reloadData()


                                    }
                                    else
                                    {
                                        
                                        
                                        let x : Float = Float(self.feedPosts.count) / Float(3)

                                        print("X Ans = ",x)
                                        
                                        print("X = ",self.feedPosts.count)
                                        
                                        let roundVal = x.rounded(.up)
                                        print("Round = ",roundVal)
                                        
//                                        let retVal = 230 + (126  * roundVal) + 60
                                        
                                        let retVal = (126  * roundVal) + 60

                                        
                                        let size : CGFloat = CGFloat(retVal)
                                        
                                        
                                        if size > 573
                                        {
                                            
                                            //                                        self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            //                                        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            
                                            self.view.layoutIfNeeded()
                                            
                                        }
                                        else
                                        {
                                            
                                            let retVal = (126  * roundVal) + 60
                                            
                                            
                                            let size : CGFloat = CGFloat(retVal)

                                            
                                            self.hghtConst.constant = size

                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            


                                            self.view.layoutIfNeeded()

                                        }
                                        
                                        
                                        
                                        self.gridCollectionView.reloadData()
                                    }
                                    
                                    
                                    
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
                                    self.proStrImage = imgString
                                    
                                    
                                    self.privacy = feedPosts["privacySettings"]as? String ?? ""

                                    self.busStatus = feedPosts["businessStatus"]as? String ?? ""

                                    if self.busStatus == "disabled"
                                    {
                                        self.proAllView.isHidden = true
                                        self.proSingleView.isHidden = false
                                    }
                                    else
                                    {
                                        self.proAllView.isHidden = false
                                        self.proSingleView.isHidden = true
                                        
                                    }

                                    
                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                }
                                else
                                {
                                    
                                    
                                    
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>

                                    
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
                                    self.proStrImage = imgString
                                    
                                    
                                    self.privacy = feedPosts["privacySettings"]as? String ?? ""
                                    
                                    self.busStatus = feedPosts["businessStatus"]as? String ?? ""
                                    
                                    if self.busStatus == "disabled"
                                    {
                                        self.proAllView.isHidden = true
                                        self.proSingleView.isHidden = false
                                    }
                                    else
                                    {
                                        self.proAllView.isHidden = false
                                        self.proSingleView.isHidden = true
                                        
                                    }
                                    
                                    
                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                    
                                    
                                    
                                    
                                    self.hghtConst.constant = 573
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)

                                    //                                    self.feedPosts.removeAll()
                                }
                                
                                //                                self.newsFeedTblView.reloadData()
                                
                                
                            }
                            else
                            {
                                
                                
                                if self.menuStatus == 1
                                {
                                    
                                    self.feedPosts.removeAll()
                                    self.newsFeedTblView.reloadData()
                                }
                                else
                                {
                                    self.feedPosts.removeAll()
                                    self.gridCollectionView.reloadData()

                                }
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func dateDiff(dateStr:String) -> String
    {
        let f:DateFormatter = DateFormatter()
        f.timeZone = NSTimeZone.local
        f.dateFormat = "yyyy-M-dd'T'HH:mm:ss.SSSZZZ"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedPosts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell") as! FeedTableViewCell
        
        cell.delegate = self
        
        //        let feedPosts = self.feedPosts[indexPath.row]as! Dictionary<String, Any>
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        let hashArray = feedPosts["caption_tags"]as? NSArray

        
        let imgString = feedPosts["profilePic"]as? String ?? ""
        
        
        
        cell.proImageView.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
  
        
        if feedPosts.object(forKey: "user_liked") as! String == "true"
        {
            cell.likeBtn.setImage(UIImage(named: "heart_filled"), for: UIControlState())
            
        }
        else
        {
            cell.likeBtn.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
        }
        
        
        if feedPosts.object(forKey: "user_pinned") as! String == "true"
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            
        }
        else
        {
            cell.bookmartBtn.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
        }
        
 
        
        cell.bookmartBtn.addTarget(self, action: #selector(ProfileViewController.bookmartAtn(sender:)), for: .touchUpInside)
        
        cell.bookmartBtn.tag = indexPath.row
        
        
        cell.likeBtn.addTarget(self, action: #selector(ProfileViewController.likeBtn(sender:)), for: .touchUpInside)
        
        cell.likeBtn.tag = indexPath.row
        
        
        
        let cmtStatus = feedPosts["commentingStatus"] as! Int
        
        let commentStatus = String(describing: cmtStatus)
        
        cell.commentBtn.addTarget(self, action: #selector(ProfileViewController.commentBtn(_:)), for: .touchUpInside)
        
        cell.commentBtn.tag = indexPath.row

/*        if commentStatus == "1"
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = true
        }
        else
        {
            cell.yesCommentView.isHidden = true
            cell.noCommentView.isHidden = false
        }*/
        
        
        if commentStatus == "1"
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = true
        }
        else
        {
            cell.yesCommentView.isHidden = false
            cell.noCommentView.isHidden = false
        }
        
        
        cell.reportBtn.addTarget(self, action: #selector(ProfileViewController.reportBtn(_:)), for: .touchUpInside)
        
        cell.reportBtn.tag = indexPath.row
        
        
        
        cell.selectionStyle = .none
        
        //        let Userimg = feedPosts["name"]as? String ?? ""
        //        Cell.imgprofile.sd_setImage(with: URL(string: Userimg as! String), placeholderImage: UIImage(named: "placeu"))
        
        //        var dct = self.feedPosts[indexPath.row] as! Dictionary<A
        
        cell.userNameLabel.text = feedPosts["name"]as? String ?? ""
        
        let currentDate = feedPosts["createdAt"]as? String
        
        cell.postAgoLabel.text =  self.dateDiff(dateStr: currentDate!)
        
        cell.addressLabel.text = feedPosts["city"]as? String ?? ""
        
        let like = feedPosts["total_likes"] as! Int
        
        cell.likeLabel.text = String(describing: like)
        
        
        let comment = feedPosts["total_comments"] as! Int
        
        
        cell.commentLabel.text = String(describing: comment)
 
        
        let userName = feedPosts["name"]as? String ?? ""
        
        let patterns = "\\s" + userName + "\\b"
        
        
        let customType = ActiveType.custom(pattern: userName) //Looks for "are"
        
        //        let customType = ActiveType.custom(pattern: patterns)
        
        cell.desLabel.enabledTypes.append(customType)
        
        
        cell.desLabel.customColor[customType] = UIColor.black
        
        
        cell.desLabel.configureLinkAttribute = { (type, attributes, isSelected) in
            var atts = attributes
            switch type {
            case customType:
                atts[NSAttributedStringKey.font] = UIFont.boldSystemFont(ofSize: 16)
            default: ()
            }
            
            return atts
        }
        
        
        
        let caption = feedPosts["caption"]as? String ?? ""
        
        
        
        let wholeCaption = " " + userName + " " + caption
        
        
        print("wholeCaption = ",wholeCaption)
        
        print("patterns = ",patterns)
        
        cell.desLabel.text  = wholeCaption
        
        
//        cell.desLabel.handleMentionTap { self.alert("Mention", message: $0) }
//        cell.desLabel.handleHashtagTap { self.alert("Hashtag", message: $0) }
        
        
        cell.desLabel.handleMentionTap
        {
                self.viewProfile(userName: $0)
        }
        
        
        
        cell.desLabel.handleHashtagTap
        {
                self.alert("Hashtag", message: $0, hash: hashArray!)
        }
        
        return cell
    }
    
    
    func viewProfile(userName :String)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        
        toBackComment = 1

        vc.userName = userName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func alert(_ title: String, message: String, hash: NSArray)
    {
        print("Hash Tag = ",hash)
        
        var hashTag : String = "#" + message
        
        var flag : Bool = false
        
        var currentID = 0
        
        for var i in 0..<hash.count
        {
            let people = hash[i] as! NSDictionary
            let tagName = people["tagName"]as? String ?? ""
            
            if tagName == hashTag
            {
                flag = true
                
                currentID = i;
                
                break;
            }
        }
        
        
        if flag
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "HashViewController")as! HashViewController
            
            let people = hash[currentID] as! NSDictionary
            
            
            let cmtStatus = people["tagId"] as! Int
            
            toBackComment = 1

            
            let commentStatus = String(describing: cmtStatus)
            
            vc.userName = commentStatus
            
            vc.tgName = hashTag
            
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "HashViewController")as! HashViewController
            
            
            toBackComment = 1

            
            vc.userName = "0"
            
            vc.tgName = hashTag
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //        guard let tableViewCell = cell as? FeedTableViewCell else { return }
        
        //        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
//        CellAnimator.animate(cell, withDuration: 1, animation: CellAnimator.AnimationType(rawValue: 5)!)
        
        let cell = cell as! FeedTableViewCell
        
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        cell.feedContentPosts  = (feedPosts["linkData"]as? Array)!
        
        
        cell.collectionVIew.reloadData()
        //        cell.collectionVIew.contentOffset = .zero

        let lastcell = self.feedPosts.count - 2
        if indexPath.row == lastcell
        {
            if Total_Page_Count >= Current_page
            {
                Current_page = Current_page + 1
                print("CurrentPage\(Current_page)")
                
                //                UPDATE_FEED_API()
                //                Notification_Bar_API()
            }
        }
        
    }
    
    @objc func bookmartAtn(sender: SparkButton)
    {
        var dict = NSMutableDictionary()
        
        
        dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
        
        
        print("Dict = %@",dict)
        
        
        if (dict.object(forKey: "user_pinned") as! String == "true")
        {
            sender.setImage(UIImage(named: "bookmark_un_filled"), for: UIControlState())
            sender.likeBounce(0.6)
            sender.animate()
            
            dict["user_pinned"] = "false"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinRemovePost(postId: id)
            
        }
        else
        {
            sender.setImage(UIImage(named: "bookmark_filled"), for: UIControlState())
            sender.unLikeBounce(0.4)
            
            dict["user_pinned"] = "true"
            
            let pID = (dict["id"] as! Int)
            
            let id = String(describing: pID)
            
            pinPost(postId: id)
        }
        
        /*
         let indexPath = IndexPath(item: sender.tag, section: 0)
         
         
         let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell*/
        
        self.feedPosts[sender.tag] = dict
        
        
        /*        let like = (dict["total_likes"] as! Int)
         
         cell.likeLabel.text = String(describing: like)
         */
        
    }
    
    
    @objc func reportBtn(_ sender: AnyObject)
    {
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Report", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.report(postid: id, currentIndex: sender.tag)
            
        })
        
        let deleteAction = UIAlertAction(title: "Unfollow", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            let pID = (dict["postedBy"] as! Int)
            
            let id = String(describing: pID)
            
            
            self.unFollowPost(postId: id)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    func unFollowPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "followerId":postId
        ]
        
        print(params)
        let url = BASE_URL + UNFOLLOW
        
        let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            self.loadProfile()
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    func report(postid :String,currentIndex :Int)
    {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "It's inappropriate", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.newsFeedTblView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let deleteAction = UIAlertAction(title: "It's spam", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            
            if Reachability.isConnectedToNetwork()
            {
                self.reportPost(postId: postid)
                
                let indexPath = IndexPath(item: currentIndex, section: 0)
                
                self.feedPosts.remove(at: currentIndex)
                
                self.newsFeedTblView.deleteRows(at: [indexPath], with: .fade)
            }
            else
            {
                self.showAlert(title: "Oops", msg: "No Internet Connection")
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    @objc func commentBtn(_ sender: AnyObject)
    {
        print("Comment Button Tag = ",sender.tag)
        
        currentTableIndex = sender.tag
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        vc.commentDelegate = self
        
        let dict = self.feedPosts[sender.tag] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String
        
        vc.proPic = profilePic
        
        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
         videoPlayerTopView.isHidden = false
         videoPlayerMainView.isHidden = false
         
         self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
         
         UIView.animate(withDuration: 2.0,
         delay: 0,
         usingSpringWithDamping: CGFloat(0.20),
         initialSpringVelocity: CGFloat(6.0),
         options: UIViewAnimationOptions.allowUserInteraction,
         animations: {
         self.videoPlayerMainView.transform = CGAffineTransform.identity
         },
         completion: { Void in()  }
         )
         */
    }
    
    
    @objc func likeBtn(sender: SparkButton)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            print("Like Button Tag = ",sender.tag)
            
            var dict = NSMutableDictionary()
            
            dict = NSMutableDictionary(dictionary: self.feedPosts[sender.tag] as! NSDictionary)
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                sender.setImage(UIImage(named: "heart_un_filled"), for: UIControlState())
                sender.likeBounce(0.6)
                sender.animate()
                
                dict["user_liked"] = "false"
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                postUnLike(postId: id)
                
            }
            else
            {
                sender.setImage(UIImage(named: "heart_filled"), for: UIControlState())
                sender.unLikeBounce(0.4)
                
                dict["user_liked"] = "true"
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                let pID = (dict["id"] as! Int)
                
                let id = String(describing: pID)
                
                postLike(postId: id)
            }
            
            
            let indexPath = IndexPath(item: sender.tag, section: 0)
            
            
            let cell = newsFeedTblView.cellForRow(at: indexPath) as! FeedTableViewCell
            
            self.feedPosts[sender.tag] = dict
            
            
            let like = (dict["total_likes"] as! Int)
            
            cell.likeLabel.text = String(describing: like)
            
            
            
            
        }
    }
    
    
    func pinRemovePost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + REMOVEFROMPINBORAD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    func pinPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + ADDTOPINBOARD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func postUnLike(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId
        ]
        
        print(params)
        let url = BASE_URL + POSTUNLIKE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    func reportPost(postId :String)
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "postId":postId,
            "reason":"not allowed"
        ]
        
        print(params)
        let url = BASE_URL + REPORT
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                        }
                        else
                        {
                            
                        }
                    }
                }
                else
                {
                    
                }
        }
    }
    
    
    
    
    /*
     func UPDATE_FEED_API()
     {
     if Reachability.isConnectedToNetwork(){
     
     
     
     let accessToken = UserDefaults.standard.string(forKey: "accessToken")
     
     
     
     
     let params: Parameters = [
     "accessToken": accessToken!,
     "page":Current_page
     ]
     
     print(params)
     let url = BASE_URL + FEEDS
     
     let Headers: HTTPHeaders = [
     
     "Content-Type":"application/x-www-form-urlencoded"
     ]
     
     print(url)
     
     
     
     Alamofire.request(url, method: .post, parameters: params, headers: Headers)
     .responseJSON
     {
     response in
     
     KRProgressHUD.dismiss()
     
     
     if(response.result.isSuccess)
     {
     
     
     if let json = response.result.value
     {
     print("Account =  \(json)")
     let jsonResponse = JSON(json)
     
     if(jsonResponse["error"].stringValue == "false" )
     {
     let JSON = response.result.value as! NSDictionary
     
     
     
     
     
     
     if (jsonResponse["success"].stringValue == "true" )
     {
     
     let JSON = response.result.value as! NSDictionary
     
     
     let newFeed = JSON.object(forKey: "NewsPosts")as! Array<Any>
     
     
     if newFeed.count > 0
     {
     for var i in 0..<newFeed.count
     {
     let dict = newFeed[i] as! NSDictionary
     self.feedPosts.append(dict)
     }
     
     }
     
     
     }
     else
     {
     self.feedPosts.removeAll()
     }
     
     self.newsFeedTblView.reloadData()
     
     
     
     
     
     
     /*        self.TVfeed.reloadData()
     
     
     
     
     self.feedPosts = JSON.object(forKey: "NewsPosts")as! Array<Any> */
     
     
     self.newsFeedTblView.reloadData()
     }
     else
     {
     /*                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
     
     let action1 = UIAlertAction(title: "Ok", style: .default)
     {
     (action:UIAlertAction) in
     }
     
     
     alertController.addAction(action1)
     
     self.present(alertController, animated: true, completion: nil)
     
     */
     }
     }
     }
     else
     {
     print(response.error.debugDescription)
     //                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
     }
     }
     
     
     /*
     
     Alamofire.request(url, method: .post, parameters: parameters, headers: Headers)
     .responseJSON {[unowned self] response in
     print(response)
     //                self.stopAnimating()
     if response.response == nil || response.result.isFailure  {
     
     let appearance = ToastView.appearance()
     appearance.backgroundColor = .lightGray
     appearance.textColor = .black
     appearance.font = .boldSystemFont(ofSize: 16)
     appearance.textInsets = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
     appearance.bottomOffsetPortrait = 100
     appearance.cornerRadius = 20
     }
     else{
     if let result = response.result.value
     {
     let JSON = result as! NSDictionary
     print(JSON)
     let errorString:String = JSON.value(forKey: "error") as! String
     
     if errorString == "false"
     {
     let success = JSON.value(forKey: "success")as! String
     if success == "true"{
     let NewFeeds = JSON.value(forKey: "NewsPosts")as! Array<Any>
     self.Total_Page_Count = JSON.object(forKey: "pageCount")as! Int
     print(self.Total_Page_Count)
     if NewFeeds.count > 0
     {
     for var i in 0..<NewFeeds.count
     {
     let dict = NewFeeds[i] as! Dictionary<String,Any>
     self.feedPosts.append(dict)
     }
     
     }
     self.TVfeed.reloadData()
     }
     else {
     let message = JSON.value(forKey: "message")as! String
     
     }
     
     }
     else
     {
     let Message = JSON.value(forKey: "message")as! String
     if Message == "Invalid Access Token" {
     let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
     let Dvc = StoaryBoard.instantiateViewController(withIdentifier: "SignInVC")as! SignInVC
     self.present(Dvc, animated: true, completion: nil)
     Toast(text: "This account has been logged into another device", duration: 3).show()
     }
     //                            self.stopAnimating()
     }
     
     }
     
     }
     }*/
     }
     else{
     //            Toast(text: "Check Internet Connenction", duration: 3).show()
     }
     }*/
    
    
    
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        //        guard let tableViewCell = cell as? FeedTableViewCell else { return }
    }
    
    
    
     
     func pullToloadFeeds()
     {
        
        
        self.feedPosts.removeAll()
        self.newsFeedTblView.reloadData()
   
        self.gridCollectionView.reloadData()

        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
//            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + MYPROFILE
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
//                    KRProgressHUD.dismiss()
                    
                    
                    self.refreshControl.endRefreshing()

                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                if (jsonResponse["success"].stringValue == "true" )
                                {
                                    
                                    let JSON = response.result.value as! NSDictionary
                                    
                                    //                                    var myProfileArray = Array<Any>()
                                    
                                    let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                                    
                                    self.feedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                                    
                                    //                                    self.newsFeedTblView.reloadData()
                                    
                                    
                                    
                                    if self.menuStatus == 1
                                    {
                                        
                                        
                                        //                                        let size : CGFloat = CGFloat((484 * self.feedPosts.count) + 290) + 60
                                        
                                        
                                        let size : CGFloat = CGFloat((484 * self.feedPosts.count)) + 60
                                        
                                        
                                        if size > 573
                                        {
                                            
                                            
                                            //                                        self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            //                                        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            
                                            self.view.layoutIfNeeded()
                                            
                                        }
                                        else
                                        {
                                            
                                            let size : CGFloat = CGFloat((484 * self.feedPosts.count)) + 60
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            
                                            //                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
                                            //
                                            //                                            self.hghtConst.constant = 573
                                            
                                        }
                                        
                                        self.newsFeedTblView.reloadData()
                                        
                                        
                                    }
                                    else
                                    {
                                        
                                        
                                        let x : Float = Float(self.feedPosts.count) / Float(3)
                                        
                                        print("X Ans = ",x)
                                        
                                        print("X = ",self.feedPosts.count)
                                        
                                        let roundVal = x.rounded(.up)
                                        print("Round = ",roundVal)
                                        
                                        //                                        let retVal = 230 + (126  * roundVal) + 60
                                        
                                        let retVal = (126  * roundVal) + 60
                                        
                                        
                                        let size : CGFloat = CGFloat(retVal)
                                        
                                        
                                        if size > 573
                                        {
                                            
                                            //                                        self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            //                                        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: size)
                                            
                                            
                                            
                                            self.view.layoutIfNeeded()
                                            
                                        }
                                        else
                                        {
                                            
                                            let retVal = (126  * roundVal) + 60 + 20
                                            
                                            
                                            let size : CGFloat = CGFloat(retVal)
                                            
                                            
                                            self.hghtConst.constant = size
                                            
                                            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: size)
                                            
                                            
                                            
                                            self.view.layoutIfNeeded()
                                            
                                        }
                                        
                                        
                                        
                                        self.gridCollectionView.reloadData()
                                    }
                                    
                                    
                                    
                                    let feedPosts = myProfileArray[0]as! NSDictionary
                                    
                                    let imgString = feedPosts["profilePic"]as? String ?? ""
                                    
                                    self.proStrImage = imgString
                                    
                                    
                                    self.privacy = feedPosts["privacySettings"]as? String ?? ""
                                    
                                    self.busStatus = feedPosts["businessStatus"]as? String ?? ""
                                    
                                    if self.busStatus == "disabled"
                                    {
                                        self.proAllView.isHidden = true
                                        self.proSingleView.isHidden = false
                                    }
                                    else
                                    {
                                        self.proAllView.isHidden = false
                                        self.proSingleView.isHidden = true
                                        
                                    }
                                    
                                    
                                    self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                                    
                                    
                                    self.navUserName.text = feedPosts["name"]as? String ?? ""
                                    
                                    self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                                    
                                    var photoCnt = feedPosts["photocount"] as! Int
                                    
                                    self.postCount.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followers"] as! Int
                                    
                                    self.followersLbl.text = String(describing: photoCnt)
                                    
                                    photoCnt = feedPosts["total_followings"] as! Int
                                    
                                    self.followingLbl.text = String(describing: photoCnt)
                                    
                                    self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                                    
                                }
                                else
                                {
                                    
                                    self.hghtConst.constant = 573
                                    
                                    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 573)
                                    
                                    //                                    self.feedPosts.removeAll()
                                }
                                
                                //                                self.newsFeedTblView.reloadData()
                                
                                
                            }
                            else
                            {
                                
                                
                                if self.menuStatus == 1
                                {
                                    
                                    self.feedPosts.removeAll()
                                    self.newsFeedTblView.reloadData()
                                }
                                else
                                {
                                    self.feedPosts.removeAll()
                                    self.gridCollectionView.reloadData()
                                    
                                }
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.newsFeedTblView.reloadData()

            self.userTaggedCollectionView.reloadData()
            
            self.gridCollectionView.reloadData()

            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
    }
     /*{
     
         let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
         let params: Parameters = [
         "accessToken": accessToken!,
         "page":1
         ]
        
         print(params)
         let url = BASE_URL + MYPROFILE
        
         let Headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded"
         ]
        
         Alamofire.request(url, method: .post, parameters: params, headers: Headers)
         .responseJSON
         {
         response in
            
         //                self.newsFeedTblView.cr.endHeaderRefresh()
         
         self.refreshControl.endRefreshing()
         
         if(response.result.isSuccess)
         {
             if let json = response.result.value
             {
             print("Account =  \(json)")
             let jsonResponse = JSON(json)
             
             if(jsonResponse["error"].stringValue == "false" )
             {
             
             if (jsonResponse["success"].stringValue == "true" )
             {
                
                let JSON = response.result.value as! NSDictionary
                
                //                                    var myProfileArray = Array<Any>()
                
                let myProfileArray = JSON.object(forKey: "profile")as! Array<Any>
                
                self.feedPosts = JSON.object(forKey: "myposts")as! Array<Any>
                
                self.newsFeedTblView.reloadData()
                
    //            self.gridCollectionView.reloadData()
                
                let feedPosts = myProfileArray[0]as! NSDictionary
                
                let imgString = feedPosts["profilePic"]as? String ?? ""
                
                self.navProImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                
                self.proImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
                
                self.navUserName.text = feedPosts["name"]as? String ?? ""
                
                self.userNameLbl.text = feedPosts["name"]as? String ?? ""
                
                var photoCnt = feedPosts["photocount"] as! Int
                
                self.postCount.text = String(describing: photoCnt)
                
                photoCnt = feedPosts["total_followers"] as! Int
                
                self.followersLbl.text = String(describing: photoCnt)
                
                photoCnt = feedPosts["total_followings"] as! Int
                
                self.followingLbl.text = String(describing: photoCnt)
                
                self.bioLbl.text = feedPosts["bio"]as? String ?? ""
                
             }
             else
             {
             self.feedPosts.removeAll()
             }
             
             self.newsFeedTblView.reloadData()
             
             }
             else
             {
             let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
             
             let action1 = UIAlertAction(title: "Ok", style: .default)
             {
             (action:UIAlertAction) in
             }
                
             alertController.addAction(action1)
             
             self.present(alertController, animated: true, completion: nil)
             
             }
             }
         }
         else
         {
         print(response.error.debugDescription)
         self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
         }
         }
     }*/
    
    
    @objc func userLongPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = userTaggedCollectionView.indexPath(for: cell)
        {
            
            let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
            
            print("FeedPosts = %@",feedPosts);
            
            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()
            
            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            var imageName : String = ""
            
            
            if feedContentPosts.count > 0
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    imageName = linkData
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    imageName = linkData
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }
            
            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            
            // you can set different frame for each peek view here
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            
            let cmtStatus = feedPosts["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
            
            print("commentStatus = ",commentStatus)
            
            if commentStatus == "1"
            {
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default),
                        PeekViewAction(title: "Comment", style: .default)]
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler:
                        {
                            optionIndex in
                            switch optionIndex
                            {
                            case 0:
                                print("Option 1 selected")
                                self.userLikeBtn(row: indexPath.row)
                                
                            case 1:
                                print("Option 2 selected")
                                self.userCommentBtn(row: indexPath.row)
                                
                            default:
                                break
                                
                            }
                    },
                                                 dismissHandler:
                        {
                            print("Peekview dismissed!")
                    })
                }
                else
                {
                    let options = [
                        PeekViewAction(title: "Like", style: .default),
                        PeekViewAction(title: "Comment", style: .default)]
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.userLikeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.userCommentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                }
            }
            else
            {
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default)]
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler:
                        {
                            optionIndex in
                            switch optionIndex
                            {
                            case 0:
                                print("Option 1 selected")
                                self.userLikeBtn(row: indexPath.row)
                                
                            case 1:
                                print("Option 2 selected")
                                self.userCommentBtn(row: indexPath.row)
                                
                            default:
                                break
                                
                            }
                    },
                                                 dismissHandler:
                        {
                            print("Peekview dismissed!")
                    })
                }
                else
                {
                    let options = [
                        PeekViewAction(title: "Like", style: .default)]
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.userLikeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.userCommentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                }
            }
            
        }
    }
    
    @objc func longPressCell(_ gestureRecognizer: UILongPressGestureRecognizer)
    {
        
        if let cell = gestureRecognizer.view as? UICollectionViewCell, let indexPath = gridCollectionView.indexPath(for: cell)
        {

            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary

            print("FeedPosts = %@",feedPosts);
            
            var feedContentPosts = Array<Any>()
            
            var linkData = NSDictionary()

            feedContentPosts = (feedPosts["linkData"]as? Array)!
            
            var imageName : String = ""

            
            if feedContentPosts.count > 0
            {
                linkData = feedContentPosts[0]as! NSDictionary
                
                let type = linkData["type"]as? String ?? ""
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    imageName = linkData
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    imageName = linkData
                }
            }
            else
            {
                imageName = "default-thumbnail"
            }

            
            let proPic = feedPosts["profilePic"]as? String ?? ""
            let name = feedPosts["name"]as? String ?? ""
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
            controller.imagenames = imageName
            controller.proPic = proPic
            controller.proName = name
            
            // you can set different frame for each peek view here
            
            let frame = CGRect(x: 15, y: (screenHeight - 300)/2, width: screenWidth - 30, height: 300)
            
            
            
            let cmtStatus = feedPosts["commentingStatus"] as! Int
            
            let commentStatus = String(describing: cmtStatus)
            
           
            print("commentStatus = ",commentStatus)
            
            if commentStatus == "1"
            {
                
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default),
                        PeekViewAction(title: "Comment", style: .default)]
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                }
                else
                {
                    
                    let options = [
                        PeekViewAction(title: "Like", style: .default),
                        PeekViewAction(title: "Comment", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                            
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                    
                }

            }
            else
            {
                
                if feedPosts.object(forKey: "user_liked") as! String == "true"
                {
                    
                    let options = [
                        PeekViewAction(title: "Un Like", style: .default)]
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                }
                else
                {
                    
                    let options = [
                        PeekViewAction(title: "Like", style: .default)]
                    
                    
                    
                    PeekView().viewForController(parentViewController: self, contentViewController: controller, expectedContentViewFrame: frame, fromGesture: gestureRecognizer, shouldHideStatusBar: true, menuOptions: options, completionHandler: { optionIndex in
                        switch optionIndex
                        {
                        case 0:
                            print("Option 1 selected")
                            self.likeBtn(row: indexPath.row)
                            
                        case 1:
                            print("Option 2 selected")
                            self.commentBtn(row: indexPath.row)
                            
                        default:
                            break
                        }
                    }, dismissHandler: {
                        print("Peekview dismissed!")
                    })
                    
                    
                }
                
            }
            
            
          
            
            
 
        }
    }
    
    func commentBtn(row :Int)
    {
        currentTableIndex = row
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.feedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func likeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {

            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.feedPosts[row] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            
            
            
            
            self.feedPosts[row] = dict
            
        }
    }
    
    
    func userLikeBtn(row :Int)
    {
        
        if !Reachability.isConnectedToNetwork()
        {
            let alertController = UIAlertController(title: "Oops", message: "No Internet Connection", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default)
            {
                (action:UIAlertAction) in
            }
            
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            
            
            
            var dict = NSMutableDictionary()
            
            
            dict = NSMutableDictionary(dictionary: self.userTaggedPosts[row] as! NSDictionary)
            
            //        dict = self.feedPosts[sender.tag]as! NSDictionary
            
            
            
            print("Dict = %@",dict)
            
            
            
            
            
            
            if (dict.object(forKey: "user_liked") as! String == "true")
            {
                
                
                dict["user_liked"] = "false"
                
                //            let like =  (dict["total_likes"] as! Int - 1)
                
                
                let like = (dict["total_likes"] as! Int - 1)
                
                dict["total_likes"] = like
                
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postUnLike(postId: id)
                
                
                
                
            }
            else
            {
                
                
                dict["user_liked"] = "true"
                
                //            let like =  (dict["total_likes"] as! Int + 1)
                
                
                dict["total_likes"] = (dict["total_likes"] as! Int + 1)
                
                
                let pID = (dict["id"] as! Int)
                
                
                let id = String(describing: pID)
                
                
                postLike(postId: id)
            }
            
            self.userTaggedPosts[row] = dict
            
        }
    }
    
    
    func userCommentBtn(row :Int)
    {
        currentTableIndex = row
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
        
        
        let dict = self.userTaggedPosts[row] as! NSDictionary
        
        let pID = (dict["id"] as! Int)
        
        let id = String(describing: pID)
        
        vc.postID = id
        
        let profilePic = dict["profilePic"] as! String ?? ""
        
        vc.proPic = profilePic
        
        toBackComment = 1
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard #available(iOS 9.0, *) else {
            return nil
        }
        
//        let indexPath = collectionview.indexPathForItem(at: collectionview.convert(location, from:view))
//        if let indexPath = indexPath {
//            let imageName = img[(indexPath as NSIndexPath).item]
//            if let cell = collectionview.cellForItem(at: indexPath) {
//                previewingContext.sourceRect = cell.frame
//
//                //                let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
//                //                controller.imagenames = imageName
//                //                return controller
//            }
//        }
        
        return nil
        
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        //        let controller = storyboard?.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        //        controller.imagenames = (viewControllerToCommit as! DetailsVC).imagenames
        //
        //        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func followingBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "ProfileFollowingViewController")as! ProfileFollowingViewController
        self.navigationController?.pushViewController(vc, animated: true)        
    }
    
    
    @IBAction func followersBtn(_ sender: Any)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "FollowersViewController")as! FollowersViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}


extension ProfileViewController: UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
//        cell.alpha = 0
//        UIView.animate(withDuration: 0.8) {
//            cell.alpha = 1
//        }
        
        
        if collectionView == gridCollectionView
        {
            if forceTouchAvailable == false {
                let gesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressCell(_:)))
                gesture.minimumPressDuration = 0.5
                cell.addGestureRecognizer(gesture)
            }
        }
        else if collectionView == userTaggedCollectionView
        {
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(userLongPressCell(_:)))
            gesture.minimumPressDuration = 0.5
            cell.addGestureRecognizer(gesture)

        }
        
    }
    
    /*
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        

        if userTaggedPosts.count > 0
        {
            self.userTaggedCollectionView.backgroundView = nil
        }
        else
        {
//            var noDataLabel: UILabel = UILabel(frame: CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height))
//            noDataLabel.text = "No Data Available"
//            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
//            noDataLabel.textAlignment = NSTextAlignment.Center
            
            
            let view = Bundle.main.loadNibNamed("EmptyTable", owner: self, options: nil)
            
            self.userTaggedCollectionView.backgroundView = view
        }
        
        
        return 0
        
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == userTaggedCollectionView
        {
            

            
            
            return self.userTaggedPosts.count
        }
        else
        {        
            return self.feedPosts.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
            if gridCollectionView == collectionView
            {
            
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectionViewCell", for: indexPath) as! GridCollectionViewCell
                
                let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
                
                
                print("feedPosts = ",feedPosts)
                
                
                var feedContentPosts = Array<Any>()
                
                feedContentPosts = (feedPosts["linkData"]as? Array)!
                
                
                print("feedContentPosts = ",feedContentPosts)
                
                var linkData = NSDictionary()
                
                if feedContentPosts.count > 0
                {
                    cell.multipleCount.isHidden = false
                    
                    linkData = feedContentPosts[0]as! NSDictionary
                    
                    if feedContentPosts.count > 1
                    {
                        cell.multipleCount.isHidden = false
                    }
                    else
                    {
                        cell.multipleCount.isHidden = true
                    }
                    
                }
                else
                {
                    cell.multipleCount.isHidden = true
                }
                
                
        //        print("linkData = ",linkData)


                let type = linkData["type"]as? String ?? ""
                
                
                
                print("Type = ",type)
                
                
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    

                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    
                }
                else
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    
                }
                
                return cell

                
            }
            else
            {
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserTaggedCollectionViewCell", for: indexPath) as! UserTaggedCollectionViewCell
                
                let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
                
                
                print("feedPosts = ",feedPosts)
                
                
                var feedContentPosts = Array<Any>()
                
                feedContentPosts = (feedPosts["linkData"]as? Array)!
                
                
                
                if feedContentPosts.count > 0
                {
                    cell.multipleCount.isHidden = false
                    
                    if feedContentPosts.count > 1
                    {
                        cell.multipleCount.isHidden = false
                    }
                    else
                    {
                        cell.multipleCount.isHidden = true

                    }
                }
                else
                {
                    cell.multipleCount.isHidden = true
                }
                
                print("feedContentPosts = ",feedContentPosts)
                
                let linkData = feedContentPosts[0]as! NSDictionary
                
                
                //        print("linkData = ",linkData)
                
                
                let type = linkData["type"]as? String ?? ""
                
                
                
                print("Type = ",type)
                
                
                
                if type == "image"
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    
                    
                }
                else if type == "video"
                {
                    let linkData = linkData["thumbnail"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    
                }
                else
                {
                    let linkData = linkData["linkData"]as? String ?? ""
                    
                    cell.feedImage.sd_setImage(with: URL(string: linkData), placeholderImage: UIImage(named: "default-thumbnail"))
                    
                }
                
                return cell
                
                
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if gridCollectionView == collectionView
        {
        
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController

            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary

            
            toBackComment = 1
            
            let like = feedPosts["id"] as! Int
            
            vc.detailPostId = String(describing: like)
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
            
            let feedPosts = self.userTaggedPosts[indexPath.row]as! NSDictionary
            
            toBackComment = 1

            let like = feedPosts["id"] as! Int
            
            vc.detailPostId = String(describing: like)
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
/*        if let _ = scrollView as? UITableView {
            
            let  height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                print(" you reached end of the table")
                
                
                self.topView.isHidden = true
                self.lineView.isHidden = true
                
                self.view.layoutIfNeeded()
                hghtConst.constant = 0
                
                
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                
            }
        }
        else if let _ = scrollView as? UICollectionView
        {
            
            let  height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                print(" you reached end of the table")
                
                
                self.topView.isHidden = true
                self.lineView.isHidden = true
                
                self.view.layoutIfNeeded()
                hghtConst.constant = 0
                
                
                
                UIView.animate(withDuration: 0.4, animations: {
                    self.view.layoutIfNeeded()
                })
                
            }
        }*/
        
    }
    
}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3 - 1, height: collectionView.frame.size.width / 3 - 1)
    }
    
    
    
    
    
    
    
}


extension UIColor
{
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension ProfileViewController: FeedTableViewCellDelegate
{
    func goToCommentVC()
    {
        
    }
    
    func goToProfileUserVC() {
        
    }
    
    func playVideo(url :String)
    {
        
     
        videoPlayerTopView.isHidden = false
        videoPlayerMainView.isHidden = false
        
/*        self.videoPlayerMainView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.videoPlayerMainView.transform = CGAffineTransform.identity
        },
                       completion:
            {
                Void in()
                
                let firstVideoURL  = URL(string: url)                 //returns a valid URL
                
                
                self.videoPlayer.videoURLs = [firstVideoURL!]
                
                
                self.videoPlayer.videoPlayerControls.play()
                
                
        })*/
        
        let firstVideoURL  = URL(string: url)                 //returns a valid URL
        
        
        self.videoPlayer.videoURLs = [firstVideoURL!]
        
        
        self.videoPlayer.videoPlayerControls.play()
        
 
        
        //        var videoUrl: String = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        
        
        
    }
    
}



