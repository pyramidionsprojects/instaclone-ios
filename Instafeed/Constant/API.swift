//
//  APIUrl.swift
//  KOLLOKI
//
//  Created by admin on 19/02/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

let BASE_URL = "http://139.59.43.169:3012/"
let SOCIAL_LOGIN = "facebookLogin"
let CREATE_ACCOTNT = "createAccount"
let LOG_IN = "login"
let DEVICE_TOKEN = "device_token"
let CHECKNAME = "checkName"
let GETOTP = "getOTP"
let CHECKOTP = "checkOTP"
let ENTERNEWPASSWORD = "enterNewPassword"
let PROFILEPICUPLOAD = "profilePicUpload"
let NEWPOST = "newnewpost"//"newpost"
let FEEDS = "feeds"
let POSTLIKE = "postlike"
let POSTUNLIKE = "postunlike"
let ADDTOPINBOARD = "addToPinboard"
let REMOVEFROMPINBORAD = "removeFromPinboard"
let SHOWCOMMENTS = "showComments"
let REPORT = "report"
let COMMENTLIKE = "commentlike"
let COMMENTUNLIKE = "commentunlike"
let COMMENT = "comment"
let UNFOLLOW = "unfollow"
let HASHTAGSEARCH = "hashTagSearch"
let TAGUSERSEARCH = "tagUserSearch"
let MYPROFILE = "myProfile"
let GETEDITPROFILE = "geteditProfile"
let EDITPROFILE = "editProfile"
let SEARCHPLACES = "searchPlaces"
let SEARCHUSER = "searchUser"
let USERTAGGEDPOST = "userTagedPosts"
let SHOWPINNED = "showPinned"
let EDITPASSWORD = "editPassword"
let LOGOUT = "logout"
let VIEWPOST = "viewpost"
let CLICKUSER = "clickUser"
let FOLLOW = "follow"
let SEARCHUSERTAGGEDPOSTS = "searchUserTagedPosts"
let NOTIFY = "notify"
let DEFAULTSEARCHDATA = "defaultSearchData"
let PRIVACYSETTING = "privacySetting"
let TOPSEARCH = "topsearch"
let CLICKTAG = "clicktag"
let BLOCKUSER = "blockUser"
let LIKEREPLY = "likereply"
let REPLYUNLIKE = "replyunlike"
let REPLYES = "replyes"
let FOLLOWINGNOTIFY = "followingNotify"
let SWICTHTOBUSINESSUSER = "switchToBusinessUser"
let PROMOTE = "promote"
let PLACESPOST = "placesPost"
let FOLLOWINGFEED = "followingfeed"
let SHOWFOLLOWING = "showFollowing"
let SHOWFOLLOWERS = "showFollowers"
let DELETEPOST = "deletePost"
let TURNOFFCOMMENTS = "turnOffComments"
let TURNONCOMMENTS = "turnOnComments"
let CREATESTORIES = "createStories"
let SHOWSTORIES = "showStories"
let CHECKNAME1 = "checkName1"
let STORYVIEWS = "storyViews"
let DELETESTORIES = "deleteStories"
let CHATLIST = "chatlist"
let USERMSGLIST = "usermsglist"
let LISTFOLFOLLOWING = "listfolfollowing"







// MARK: AWS DATA

let AWS_S3_POOL_ID = "ap-northeast-2:11b9ab57-891c-439e-9435-e09f37496aaa"
let BUCKET_NAME = "zoechats"

let IMAGES_BUCKET = "https://s3.ap-northeast-2.amazonaws.com/zoechats/"


// MARK: USERDEFAULTS
let isLoggedIn = "isLoggedIn"
let locationValue = "locationValue"
let username = "username"
let name = "name"
let currentLat = "currentLat"
let currentLong = "currentLong"
let userID = "userID"
let propic = "propic"


