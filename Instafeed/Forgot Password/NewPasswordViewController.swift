//
//  NewPasswordViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 26/04/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD


class NewPasswordViewController: UIViewController,NVActivityIndicatorViewable {

    
    var email : String = ""
    var indicator: NVActivityIndicatorView!

    @IBOutlet weak var centerAlignNewPwd: NSLayoutConstraint!
    @IBOutlet weak var centerAlignConfPwd: NSLayoutConstraint!
    @IBOutlet weak var centerAlignNext: NSLayoutConstraint!
    
    @IBOutlet weak var newPwdTxt: UITextField!
    @IBOutlet weak var confPwdTxt: UITextField!

    @IBOutlet weak var nextView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let redColor = UIColor(red: 236.0 / 255.0, green: 76.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
        let frame = CGRect.init(x: (self.view.frame.size.width/2)-25, y: (self.view.frame.size.height/2)-25, width: 50, height: 50)
        indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin, color: redColor, padding: 0)
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        centerAlignNewPwd.constant += view.bounds.width
        centerAlignConfPwd.constant += view.bounds.width
        centerAlignNext.constant += view.bounds.width
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseOut, animations:
            {
                self.centerAlignNewPwd.constant -= self.view.bounds.width
                self.view.layoutIfNeeded()
        },
                       completion:
            {
                (finished: Bool) in
                
        })
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: .curveEaseOut, animations:
            {
                self.centerAlignConfPwd.constant -= self.view.bounds.width
                self.view.layoutIfNeeded()
        }, completion: nil)
        
        
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: .curveEaseOut, animations:
        {
                self.centerAlignNext.constant -= self.view.bounds.width
                self.view.layoutIfNeeded()

        }, completion: nil)
        
        
    }
    
    
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        self.nextView.layer.borderWidth = 1
        self.nextView.layer.borderColor = UIColor(red:85/255, green:166/255, blue:218/255, alpha: 1).cgColor
    }
    
    
    @IBAction func nextBtn(_ sender: Any)
    {
        if newPwdTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter password", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else if confPwdTxt.text! == ""
        {
            let alertController = UIAlertController(title: "Oops", message: "Please enter confirm password", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else if confPwdTxt.text! != newPwdTxt.text!
        {
            let alertController = UIAlertController(title: "Oops", message: "Password not match", preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action1)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            sendNewPWD()
        }
        
    }
    
    
    
    func sendNewPWD()
    {
//        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Loading...")

        
        let params: Parameters = [
            "email": email,
            "password": newPwdTxt.text!
        ]
        
        print(params)
        let url = BASE_URL + ENTERNEWPASSWORD
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
//                self.indicator.stopAnimating()

                
                KRProgressHUD.dismiss()

                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("OTP Response =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
//                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()

                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

}
