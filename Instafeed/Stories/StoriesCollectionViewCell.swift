//
//  StoriesCollectionViewCell.swift
//  Snipofeed
//
//  Created by Pyramidions on 04/07/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class StoriesCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var storiesImg: UIImageView!
    @IBOutlet weak var meImg: UIImageView!
    @IBOutlet weak var roundView: UIView!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        storiesImg.layer.cornerRadius = storiesImg.frame.size.width / 2
        storiesImg.layer.masksToBounds = true
        
        
        roundView.layer.cornerRadius = roundView.frame.size.width / 2
        roundView.layer.masksToBounds = true
        roundView.layer.borderWidth = 2
        roundView.layer.borderColor = UIColor(hexString: "#0074BB").cgColor

    }
    
}
