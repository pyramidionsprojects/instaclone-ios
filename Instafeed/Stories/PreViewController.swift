//
//  PreViewController.swift
//  InstagramStories
//
//  Created by mac05 on 05/10/17.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
import KRProgressHUD
import SDWebImage


class PreViewController: UIViewController, SegmentedProgressBarDelegate
{

    
    
    var flag = false
    
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var lblView: UIView!
    @IBOutlet weak var typeTextLbl: UILabel!
    
    var playVideoOrLoadImageIndex = 0
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    var pageIndex : Int = 0
//    var items = [[String: Any]]()
    
    var items = Array<Any>()

    var item = Array<Any>()

//  var item = [[String : String]]()
    var SPB: SegmentedProgressBar!
    var player: AVPlayer!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let _ = DownloadManager.shared.activate()

        
        if pageIndex == 0
        {
            deleteView.isHidden = false
        }
        else
        {
            deleteView.isHidden = true
        }
        
        
        let feedPosts = items[pageIndex]as! NSDictionary
        
        
        print("feedPosts = ",feedPosts)
        
        
        item  = (feedPosts["data"]as? Array)!

        
        self.userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.height / 2;
        
        let name = feedPosts["name"]as? String ?? ""

        lblUserName.text = name
        
        
        let userImage = feedPosts["userImage"]as? String ?? ""

        
        self.userProfileImage.sd_setImage(with: URL(string: userImage), placeholderImage: UIImage(named: "default-user-image"))

        
        
   
//        userProfileImage.image = UIImage(named: items[pageIndex]["pro-image"] as! String)
//        lblUserName.text = items[pageIndex]["name"] as? String
//        item = self.items[pageIndex]["items"] as! [[String : String]]
//        
        SPB = SegmentedProgressBar(numberOfSegments: self.item.count, duration: 5)
        if #available(iOS 11.0, *) {
            SPB.frame = CGRect(x: 18, y: UIApplication.shared.statusBarFrame.height + 5, width: view.frame.width - 35, height: 3)
        } else {
            // Fallback on earlier versions
            SPB.frame = CGRect(x: 18, y: 15, width: view.frame.width - 35, height: 3)
        }
        
        SPB.delegate = self
        SPB.topColor = UIColor.white
        SPB.bottomColor = UIColor.white.withAlphaComponent(0.25)
        SPB.padding = 2
        SPB.isPaused = true
        SPB.currentAnimationIndex = 0
        view.addSubview(SPB)
        view.bringSubview(toFront: SPB)
        
        let tapGestureImage = UITapGestureRecognizer(target: self, action: #selector(self.tapOn(_:)))
        tapGestureImage.numberOfTapsRequired = 1
        tapGestureImage.numberOfTouchesRequired = 1
        imagePreview.addGestureRecognizer(tapGestureImage)
        
        let tapGestureVideo = UITapGestureRecognizer(target: self, action: #selector(self.tapOn(_:)))
        tapGestureVideo.numberOfTapsRequired = 1
        tapGestureVideo.numberOfTouchesRequired = 1
        videoView.addGestureRecognizer(tapGestureVideo)
        
        
        let lblViewType = UITapGestureRecognizer(target: self, action: #selector(self.tapOn(_:)))
        lblViewType.numberOfTapsRequired = 1
        lblViewType.numberOfTouchesRequired = 1
        lblView.addGestureRecognizer(lblViewType)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        UIView.animate(withDuration: 0.8) {
            self.view.transform = .identity
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.async {
            self.SPB.currentAnimationIndex = 0
            self.SPB.startAnimation()
            
            
            
            
            self.playVideoOrLoadImage(index: 0)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async
        {
            self.SPB.currentAnimationIndex = 0
            self.SPB.isPaused = true
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - SegmentedProgressBarDelegate
    //1
    func segmentedProgressBarChangedIndex(index: Int) {
        playVideoOrLoadImage(index: index)
    }
    
    //2
    func segmentedProgressBarFinished() {
        if pageIndex == (self.items.count - 1)
        {
            
            if flag
            {
                NotificationCenter.default.post(name: Notification.Name("backToBlockUser"), object: nil)
                
                self.dismiss(animated: true, completion: nil)

            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        else {
            _ = ContentViewControllerVC.goNextPage(fowardTo: pageIndex + 1)
        }
    }
    
    @objc func tapOn(_ sender: UITapGestureRecognizer)
    {
        SPB.skip()
    }
    
    /*
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if (keyPath == "status")
        {
            if player.status == .readyToPlay
            {
                
                print("Started")
                
                self.SPB.isPaused = false

                
                self.player.play()
                
                
            }
            else if player.status == .failed
            {
                
            }
        }
    }
*/
    
    //MARK: - Play or show image
    func playVideoOrLoadImage(index: NSInteger)
    {
        
        playVideoOrLoadImageIndex = index
        
        self.lblView.isHidden = true
        
        let feedPosts = item[index]as! NSDictionary
        
        let imgString = feedPosts["type"]as? String ?? ""
        
        if imgString == "image"
        {
            
            self.SPB.duration = 5
            self.imagePreview.isHidden = false
            self.videoView.isHidden = true
            
            let img = feedPosts["storyURL"]as? String ?? ""
            
            self.imagePreview.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: ""))
            
//            self.imagePreview.image = UIImage(named: item[index]["item"]!)
        }
        else if imgString == "video"
        {
            
            let moviePath = feedPosts["storyURL"]as? String ?? ""
            
        
                self.imagePreview.isHidden = true
                self.videoView.isHidden = false

            
            let url = URL(string: moviePath)
            
            
            let destinationFilename : String = (url?.lastPathComponent)!

            
            
            let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
            let documentDirectoryPath:String = path[0]
            let fileManager = FileManager()
            
            let imagesDirectoryPath = documentDirectoryPath.appending("/Snipofeed")
            
            let destinationURLForFile = URL(fileURLWithPath: imagesDirectoryPath.appendingFormat("/" + destinationFilename))

            
            print("Got destinationURLForFile = ",destinationURLForFile)
            
          
            
            if fileManager.fileExists(atPath: destinationURLForFile.path)
            {
//                let url = URL(string: destinationURLForFile.path)
                
                let url = NSURL.fileURL(withPath: destinationURLForFile.path)
//                self.player = AVPlayer(url: url)

                
                
                self.player = AVPlayer(url: url)
                
                let videoLayer = AVPlayerLayer(player: self.player)
                videoLayer.frame = view.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.videoView.layer.addSublayer(videoLayer)
                
                let asset = AVAsset(url: url)
                let duration = asset.duration
                let durationTime = CMTimeGetSeconds(duration)
                
                
                self.SPB.duration = durationTime
                
                self.player.play()
                
            }
            else
            {
                
                let task = DownloadManager.shared.activate().downloadTask(with: url!)
                
                task.resume()
                
                self.player = AVPlayer(url: url!)
                
                let videoLayer = AVPlayerLayer(player: self.player)
                videoLayer.frame = view.bounds
                videoLayer.videoGravity = .resizeAspectFill
                self.videoView.layer.addSublayer(videoLayer)
                
                let asset = AVAsset(url: url!)
                let duration = asset.duration
                let durationTime = CMTimeGetSeconds(duration)
                
                
                self.SPB.duration = durationTime
                
                self.player.play()

            }
            
            
            
            
            
            
            
            
          
            
//            player.currentItem!.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions(), context: nil)


            
                
//                self.SPB.duration = durationTime
//                self.player.play()
//            }
        }
        else
        {
            
            self.lblView.isHidden = false
            
            let bgimage = feedPosts["bgimage"]as? String ?? ""

            let count = "I" + String(bgimage)
            
            let image = UIImage(named:count)
            
            self.imagePreview.image = image
            
            let storyDescription = feedPosts["storyDescription"]as? String ?? ""

            self.typeTextLbl.text = storyDescription
        }
        
        
        
        if pageIndex == 0
        {
            
        }
        else
        {
            let status = feedPosts["status"] as? String ?? ""
            
            let tempID = feedPosts["id"] as! Int

            let id = String(describing: tempID)

            if status == "false"
            {
                self.viewStory(id: id)
            }
            else
            {
                
            }
        }
        
    }
    
    //MARK: - Button actions
    @IBAction func close(_ sender: Any)
    {
        if flag
        {
            NotificationCenter.default.post(name: Notification.Name("backToBlockUser"), object: nil)
            self.dismiss(animated: true, completion: nil)
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteStory(_ sender: Any)
    {
        self.SPB.isPaused = true

        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete Story", style: .destructive, handler:
        {
            (alert: UIAlertAction!) -> Void in

            let feedPosts = self.item[self.playVideoOrLoadImageIndex]as! NSDictionary
            
            let tempID = feedPosts["id"] as! Int
            
            let id = String(describing: tempID)
            
            self.deleteStory(id: id)
            
//            self.deletePost(postid: id, currentIndex: sender.tag)
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.SPB.isPaused = false
        })
            
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func deleteStory(id :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")

            let params: Parameters = [
                "accessToken": accessToken!,
                "storyId":id
            ]
            
            print(params)
            let url = BASE_URL + DELETESTORIES
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                NotificationCenter.default.post(name: Notification.Name("backToBlockUser"), object: nil)
                                self.dismiss(animated: true, completion: nil)
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        
                    }
            }
            
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func viewStory(id :String)
    {
        if Reachability.isConnectedToNetwork()
        {
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "storyId":id
            ]
            
            print(params)
            let url = BASE_URL + STORYVIEWS
            
            let Headers: HTTPHeaders = [
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    if(response.result.isSuccess)
                    {
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                self.flag = true
                                
//                                NotificationCenter.default.post(name: Notification.Name("backToBlockUser"), object: nil)
//
//                                self.dismiss(animated: true, completion: nil)
                                
                            }
                            else
                            {
                                
                            }
                        }
                    }
                    else
                    {
                        
                    }
            }
        }
        else
        {
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
    }
    
}
