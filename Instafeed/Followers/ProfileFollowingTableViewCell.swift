//
//  YouTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 17/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class ProfileFollowingTableViewCell: UITableViewCell {

    
//    @IBOutlet weak var viewProfileBtn: UIButton!
//    @IBOutlet weak var viewPostBtn: UIButton!
    
    
    @IBOutlet weak var viewFollowerBtn: UIButton!
    
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var followView: UIView!

//    @IBOutlet weak var proImg: UIImageView!
//    @IBOutlet weak var likeImgView: UIImageView!
    
//    @IBOutlet weak var commentDisplayLbl: UILabel!
    
    
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var unfollowBtn: UIButton!
    
    
    @IBOutlet weak var followProImg: UIImageView!
    
    @IBOutlet weak var followLbl: UILabel!
    
    @IBOutlet weak var followUserNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        proImg.layer.cornerRadius = proImg.frame.size.width / 2
//        proImg.layer.masksToBounds = true

        followProImg.layer.cornerRadius = followProImg.frame.size.width / 2
        followProImg.layer.masksToBounds = true

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
