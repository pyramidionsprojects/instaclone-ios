//
//  YouViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 10/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage
import ActiveLabel
import UIEmptyState

class FollowersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UIEmptyStateDelegate, UIEmptyStateDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 0
    
    var fIndex : Int = 0
    
    var feedPosts = Array<Any>()
    
    lazy var refreshControl: UIRefreshControl =
    {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(NewsFeedViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            return refreshControl
    }()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none

//        self.tableView.addSubview(self.refreshControl)
        
        
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self

        
        
//        tableView.estimatedRowHeight = 77
//        tableView.rowHeight = UITableViewAutomaticDimension

//        loadFeeds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        loadFeeds()
    }
    
    public func reloadEmptyState() {
        self.reloadEmptyStateForTableView(self.tableView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
//    self.emptyStateImageSize = CGSize(width: 60, height: 60)

    
    var emptyStateImageSize: CGSize?
    {
        return CGSize(width: 60, height: 60)
    }
    
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "notification")
    }
    
    var emptyStateTitle: NSAttributedString {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 22)]
        return NSAttributedString(string: "No Followers", attributes: attrs)
    }
    
    var emptyStateButtonTitle: NSAttributedString? {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.white,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]
        return NSAttributedString(string: "Refresh", attributes: attrs)
    }
    
    var emptyStateButtonSize: CGSize? {
        return CGSize(width: 80, height: 30)
    }
    
    
    // MARK: - Empty State Delegate
    
    func emptyStateViewWillShow(view: UIView) {
        guard let emptyView = view as? UIEmptyStateView else { return }
        // Some custom button stuff
        emptyView.button.layer.cornerRadius = 5
        emptyView.button.layer.borderWidth = 1
        emptyView.button.layer.borderColor = UIColor.black.cgColor
        emptyView.button.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func emptyStatebuttonWasTapped(button: UIButton)
    {
        self.pullToRefresh()
    }

    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
//        self.pullToRefresh()
    }
    
    
    func pullToRefresh()
    {
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!
        ]
        
        print(params)
        let url = BASE_URL + SHOWFOLLOWERS
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                self.refreshControl.endRefreshing()
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
                            self.feedPosts = JSON.object(forKey: "following")as! Array<Any>

                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()
                            
                            
                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                
                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()
                                
                                self.reloadEmptyState()
                                
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()
                    
                }
        }
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func loadFeeds()
    {
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!
        ]
        
        print(params)
        let url = BASE_URL + SHOWFOLLOWERS
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
                            
                            
                            self.feedPosts = JSON.object(forKey: "following")as! Array<Any>
                            

                            
                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()

                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                

                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()
                                
                                self.reloadEmptyState()

                                
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()

                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.feedPosts.count
    }
    
    @objc func followAtn(_ sender: AnyObject)
    {
        
        fIndex = sender.tag
        
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary

        let pID = (feedPosts["id"] as! Int)
        
        let id = String(describing: pID)

        
        self.followUser(followerID: id)
        
    }

    @objc func unFollowAtn(_ sender: AnyObject)
    {
        
        fIndex = sender.tag

        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let pID = (feedPosts["id"] as! Int)
        
        let id = String(describing: pID)
        
        
        self.unFollowUser(followerID: id)
    }
    
    
    @objc func viewProfileAtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary

        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = feedPosts["name"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
       
    }
    
    @objc func viewPostAtn(_ sender: AnyObject)
    {
        
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
        
        let like = feedPosts["postId"] as! Int
        
        vc.detailPostId = String(describing: like)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func viewFollowerAtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = feedPosts["username"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowersTableViewCell", for: indexPath) as! FollowersTableViewCell
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        print("List = ",feedPosts)
        
        cell.selectionStyle = .none

        
        cell.followBtn.addTarget(self, action: #selector(FollowersViewController.followAtn(_:)), for: .touchUpInside)
        
        cell.followBtn.tag = indexPath.row


        cell.unfollowBtn.addTarget(self, action: #selector(FollowersViewController.unFollowAtn(_:)), for: .touchUpInside)
        
        cell.unfollowBtn.tag = indexPath.row

        
        
//        cell.viewProfileBtn.addTarget(self, action: #selector(FollowersViewController.viewProfileAtn(_:)), for: .touchUpInside)
//        cell.viewProfileBtn.tag = indexPath.row

//        cell.viewPostBtn.addTarget(self, action: #selector(FollowersViewController.viewPostAtn(_:)), for: .touchUpInside)
//        cell.viewPostBtn.tag = indexPath.row

        cell.viewFollowerBtn.addTarget(self, action: #selector(FollowersViewController.viewFollowerAtn(_:)), for: .touchUpInside)
        cell.viewFollowerBtn.tag = indexPath.row

        

        let name = feedPosts["name"]as? String ?? ""

        cell.followLbl.text = name

        let username = feedPosts["username"]as? String ?? ""
        
        cell.followUserNameLbl.text = username


            let type = feedPosts["user_follow_status"]as? String ?? ""
            
            if type == "false"
            {
                cell.followBtn.isHidden = false
                cell.unfollowBtn.isHidden = true
            }
            else
            {
                cell.followBtn.isHidden = true
                cell.unfollowBtn.isHidden = false
            }
            
            let imgString = feedPosts["profilePic"]as? String ?? ""
            
            cell.followProImg.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
            

        
        return cell
    }
    
    
    
    
    
    func followUser(followerID: String)
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + FOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
//                                self.loadProfile()
                                
                                var dict = NSMutableDictionary()
                                
                                let indexPath = IndexPath(item: self.fIndex, section: 0)
                                
                                let cell = self.tableView.cellForRow(at: indexPath) as! FollowersTableViewCell
                                

                                dict = NSMutableDictionary(dictionary: self.feedPosts[self.fIndex] as! NSDictionary)
                                
                                
                                print("Dict = ",dict)
                                
                                
                                dict["user_follow_status"] = "true"
                                
                                
                                self.feedPosts[self.fIndex] = dict
                                
                                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                                
                                
                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func unFollowUser(followerID: String)
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + UNFOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
//                                self.loadProfile()
                                
                                
                                var dict = NSMutableDictionary()
                                
                                let indexPath = IndexPath(item: self.fIndex, section: 0)
                                
                                let cell = self.tableView.cellForRow(at: indexPath) as! FollowersTableViewCell
                                
                                
                                dict = NSMutableDictionary(dictionary: self.feedPosts[self.fIndex] as! NSDictionary)
                                
                                
                                print("Dict = ",dict)
                                
                                
                                dict["user_follow_status"] = "false"
                                
                                
                                self.feedPosts[self.fIndex] = dict
                                
                                
                                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)

                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
//        return 87.5
        
        return 75
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
