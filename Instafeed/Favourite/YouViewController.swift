//
//  YouViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 10/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage
import ActiveLabel
import UIEmptyState

class YouViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UIEmptyStateDelegate, UIEmptyStateDataSource
{
    @IBOutlet weak var tableView: UITableView!
    
    var Total_Page_Count : Int = 0
    var Current_page : Int = 0
    
    var fIndex : Int = 0
    
    var feedPosts = Array<Any>()
    
    lazy var refreshControl: UIRefreshControl =
    {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(NewsFeedViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            return refreshControl
    }()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none

        self.tableView.addSubview(self.refreshControl)
        
        
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self

        
        
//        tableView.estimatedRowHeight = 77
//        tableView.rowHeight = UITableViewAutomaticDimension

//        loadFeeds()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadFeeds()
    }
    
    
    
    
    public func reloadEmptyState() {
        self.reloadEmptyStateForTableView(self.tableView)
    }
    
    
//    self.emptyStateImageSize = CGSize(width: 60, height: 60)

    
    var emptyStateImageSize: CGSize?
    {
        return CGSize(width: 60, height: 60)
    }
    
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "notification")
    }
    
    var emptyStateTitle: NSAttributedString {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 22)]
        return NSAttributedString(string: "No Notification", attributes: attrs)
    }
    
    var emptyStateButtonTitle: NSAttributedString? {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.white,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]
        return NSAttributedString(string: "Refresh", attributes: attrs)
    }
    
    var emptyStateButtonSize: CGSize? {
        return CGSize(width: 80, height: 30)
    }
    
    
    // MARK: - Empty State Delegate
    
    func emptyStateViewWillShow(view: UIView) {
        guard let emptyView = view as? UIEmptyStateView else { return }
        // Some custom button stuff
        emptyView.button.layer.cornerRadius = 5
        emptyView.button.layer.borderWidth = 1
        emptyView.button.layer.borderColor = UIColor.black.cgColor
        emptyView.button.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func emptyStatebuttonWasTapped(button: UIButton)
    {
        self.pullToRefresh()
    }

    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        self.pullToRefresh()
    }
    
    func pullToRefresh()
    {
        Current_page = 1
        
//        KRProgressHUD.show(withMessage: "Loading...")

        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "page":Current_page
        ]
        
        print(params)
        let url = BASE_URL + NOTIFY
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                self.refreshControl.endRefreshing()
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
                            //                            if (jsonResponse["success"].stringValue == "true" )
                            //                            {
                            
                            //                                let JSON = response.result.value as! NSDictionary
                            
                            
                            self.feedPosts = JSON.object(forKey: "notify")as! Array<Any>
                            
                            
                            self.Total_Page_Count =  jsonResponse["pageCount"].intValue

                            
                            
                            
                            
                            //                            }
                            //                            else
                            //                            {
                            //                                self.feedPosts.removeAll()
                            //                            }
                            
                            
                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()

                            
                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                

                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()
                                
                                self.reloadEmptyState()

                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()

                }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func loadFeeds()
    {
        Current_page = 1
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "page":Current_page
        ]
        
        print(params)
        let url = BASE_URL + NOTIFY
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
                            //                            if (jsonResponse["success"].stringValue == "true" )
                            //                            {
                            
                            //                                let JSON = response.result.value as! NSDictionary
                            
                            
                            self.feedPosts = JSON.object(forKey: "notify")as! Array<Any>
                            
                            self.Total_Page_Count = jsonResponse["pageCount"].intValue

                            
                            //                            }
                            //                            else
                            //                            {
                            //                                self.feedPosts.removeAll()
                            //                            }
                            
                            
                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()

                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                

                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()
                                
                                self.reloadEmptyState()

                                
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()

                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func dateFromMilliseconds(milliSeconds: CLongLong) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(milliSeconds)/1000)
    }
    
    func dateDiff(dateStr:String) -> String {
        print(dateStr)
        let f:DateFormatter = DateFormatter()
        f.timeZone = TimeZone(abbreviation: "UTC")
        f.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        print(startDate)
        print(endDate!)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.feedPosts.count
    }
    
    @objc func followAtn(_ sender: AnyObject)
    {
        
        fIndex = sender.tag
        
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary

        let pID = (feedPosts["follower_Id"] as! Int)
        
        let id = String(describing: pID)

        
        self.followUser(followerID: id)
        
    }

    @objc func unFollowAtn(_ sender: AnyObject)
    {
        
        fIndex = sender.tag

        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let pID = (feedPosts["follower_Id"] as! Int)
        
        let id = String(describing: pID)
        
        
        self.unFollowUser(followerID: id)
    }
    
    
    @objc func viewProfileAtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary

        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = feedPosts["name"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
       
    }
    
    @objc func viewPostAtn(_ sender: AnyObject)
    {
        
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
        
        let like = feedPosts["postId"] as! Int
        
        vc.detailPostId = String(describing: like)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func viewFollowerAtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
        vc.userName = feedPosts["name"]as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YouTableViewCell", for: indexPath) as! YouTableViewCell
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        
        print("List = ",feedPosts)
        
        cell.selectionStyle = .none

//        cell.likeBtn.addTarget(self, action: #selector(NewsFeedViewController.likeBtn(sender:)), for: .touchUpInside)

        
        
        
        cell.followBtn.addTarget(self, action: #selector(YouViewController.followAtn(_:)), for: .touchUpInside)
        
        cell.followBtn.tag = indexPath.row


        cell.unfollowBtn.addTarget(self, action: #selector(YouViewController.unFollowAtn(_:)), for: .touchUpInside)
        
        cell.unfollowBtn.tag = indexPath.row

        
        
        cell.viewProfileBtn.addTarget(self, action: #selector(YouViewController.viewProfileAtn(_:)), for: .touchUpInside)
        cell.viewProfileBtn.tag = indexPath.row

        cell.viewPostBtn.addTarget(self, action: #selector(YouViewController.viewPostAtn(_:)), for: .touchUpInside)
        cell.viewPostBtn.tag = indexPath.row

        cell.viewFollowerBtn.addTarget(self, action: #selector(YouViewController.viewFollowerAtn(_:)), for: .touchUpInside)
        cell.viewFollowerBtn.tag = indexPath.row

        
        
        
        let type = feedPosts["notification_type"]as? String ?? ""
        
        if type == "follow"
        {
            cell.likeView.isHidden = true
            cell.followView.isHidden = false

            let type = feedPosts["follow_status"]as? String ?? ""
            
            if type == "false"
            {
                cell.followBtn.isHidden = false
                cell.unfollowBtn.isHidden = true
            }
            else
            {
                cell.followBtn.isHidden = true
                cell.unfollowBtn.isHidden = false
            }
            
            let imgString = feedPosts["userImage"]as? String ?? ""
            
            cell.followProImg.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
            
            
            
            let name = feedPosts["name"]as? String ?? ""
            let value = feedPosts["notification"]as? String ?? ""
            
            let time =  feedPosts.value(forKey: "time")as! CLongLong
            let sec = time * 1000
            let data = dateFromMilliseconds(milliSeconds: time * 1000)
            print(data)
            let date = " " + dateDiff(dateStr: String(describing: data))
            
            
            let attributedStringColor = [NSAttributedStringKey.foregroundColor : UIColor.black];
            
            let attributedString = NSAttributedString(string: name, attributes: attributedStringColor)
            
            let result = NSMutableAttributedString()
            
            
            let notVal = " " + value
            
            
            let attributedString1 = NSAttributedString(string: notVal, attributes: nil)
            
            
            let attributedStringColor1 = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
            
            let attributedString2 = NSAttributedString(string: date, attributes: attributedStringColor1)
            
            
            
            result.append(attributedString1)
            result.append(attributedString)
            result.append(attributedString2)
            print(result)
            
            cell.followLbl.attributedText! = result
            
            
        }
        else
        {
            cell.likeView.isHidden = false
            cell.followView.isHidden = true
            
            
            let imgString = feedPosts["userImage"]as? String ?? ""
            
            cell.proImg.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))
            
            let proImg = feedPosts["linkData"]as? String ?? ""
            
            cell.likeImgView.sd_setImage(with: URL(string: proImg), placeholderImage: UIImage(named: "default-thumbnail"))
            
            let name = feedPosts["name"]as? String ?? ""
            let value = feedPosts["notification"]as? String ?? ""
            
            let time =  feedPosts.value(forKey: "time")as! CLongLong
            let sec = time * 1000
            let data = dateFromMilliseconds(milliSeconds: time * 1000)
            print(data)
            let date = " " + dateDiff(dateStr: String(describing: data))
            
            let attributedStringColor = [NSAttributedStringKey.foregroundColor : UIColor.black];
            
            let attributedString = NSAttributedString(string: name, attributes: attributedStringColor)
            
            let result = NSMutableAttributedString()
            
            
            let notVal = " " + value
            
            
            let attributedString1 = NSAttributedString(string: notVal, attributes: nil)
            
            
            let attributedStringColor1 = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
            
            let attributedString2 = NSAttributedString(string: date, attributes: attributedStringColor1)
            
            
            
            result.append(attributedString1)
            result.append(attributedString)
            result.append(attributedString2)
            print(result)
            
            cell.commentDisplayLbl.attributedText! = result
        }
        
        
        
        return cell
    }
    
    
    
    
    
    func followUser(followerID: String)
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + FOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
//                                self.loadProfile()
                                
                                var dict = NSMutableDictionary()
                                
                                let indexPath = IndexPath(item: self.fIndex, section: 0)
                                
                                let cell = self.tableView.cellForRow(at: indexPath) as! YouTableViewCell
                                

                                dict = NSMutableDictionary(dictionary: self.feedPosts[self.fIndex] as! NSDictionary)
                                
                                
                                print("Dict = ",dict)
                                
                                
                                dict["follow_status"] = "true"
                                
                                
                                self.feedPosts[self.fIndex] = dict
                                
                                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                                
                                
                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func unFollowUser(followerID: String)
    {
        
        if Reachability.isConnectedToNetwork()
        {
            
            Current_page = 1
            
            KRProgressHUD.show(withMessage: "Loading...")
            
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            
            
            let params: Parameters = [
                "accessToken": accessToken!,
                "followerId":followerID
            ]
            
            print(params)
            let url = BASE_URL + UNFOLLOW
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                
//                                self.loadProfile()
                                
                                
                                var dict = NSMutableDictionary()
                                
                                let indexPath = IndexPath(item: self.fIndex, section: 0)
                                
                                let cell = self.tableView.cellForRow(at: indexPath) as! YouTableViewCell
                                
                                
                                dict = NSMutableDictionary(dictionary: self.feedPosts[self.fIndex] as! NSDictionary)
                                
                                
                                print("Dict = ",dict)
                                
                                
                                dict["follow_status"] = "false"
                                
                                
                                self.feedPosts[self.fIndex] = dict
                                
                                
                                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)

                                
                            }
                            else
                            {
                                
                                let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                                
                                let action1 = UIAlertAction(title: "Ok", style: .default)
                                {
                                    (action:UIAlertAction) in
                                }
                                
                                
                                alertController.addAction(action1)
                                
                                self.present(alertController, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                    else
                    {
                        print(response.error.debugDescription)
                        self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    }
                    
            }
        }
        else
        {
            
            self.showAlert(title: "Oops", msg: "No Internet Connection")
        }
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        print("self.feedPosts.count = ",self.feedPosts.count)

        print("Total_Page_Count = ",Total_Page_Count)

        print("Current_page = ",Current_page)

        
        let lastcell = self.feedPosts.count - 1
    
        if indexPath.row == lastcell
        {
            if Total_Page_Count > Current_page
            {
                Current_page = Current_page + 1
                print("CurrentPage\(Current_page)")
                
                UPDATE_FEED_API()
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
//        return 87.5
        
        return UITableViewAutomaticDimension
    }
    
    
    
    func UPDATE_FEED_API()
    {
        if Reachability.isConnectedToNetwork(){
            
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")
            

            let params: Parameters = [
                "accessToken": accessToken!,
                "page":Current_page
            ]
            
            print(params)
            let url = BASE_URL + NOTIFY
            
            let Headers: HTTPHeaders = [
                
                "Content-Type":"application/x-www-form-urlencoded"
            ]
            
            print(url)
            
            
            
            Alamofire.request(url, method: .post, parameters: params, headers: Headers)
                .responseJSON
                {
                    response in
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if(response.result.isSuccess)
                    {
                        
                        
                        if let json = response.result.value
                        {
                            print("Account =  \(json)")
                            let jsonResponse = JSON(json)
                            
                            if(jsonResponse["error"].stringValue == "false" )
                            {
                                let JSON = response.result.value as! NSDictionary
                                
                                
                                    
                                let newFeed = JSON.object(forKey: "notify")as! Array<Any>
                                    
                                    
                                    if newFeed.count > 0
                                    {
                                        for var i in 0..<newFeed.count
                                        {
                                            let dict = newFeed[i] as! NSDictionary
                                            self.feedPosts.append(dict)
                                        }
                                        
                                    }
                                
                                }
                                else
                                {
                                    self.feedPosts.removeAll()
                                }
                                
                                self.tableView.reloadData()
                            }
                            else
                            {
                                
                            }
                        }
            }
        }
    }
    

}
