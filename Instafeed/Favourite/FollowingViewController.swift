//
//  FollowingViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 10/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import KRProgressHUD
import SDWebImage
import ActiveLabel
import UIEmptyState


class FollowingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIEmptyStateDelegate, UIEmptyStateDataSource
{
    
    @IBOutlet weak var tableView: UITableView!
    
    var feedPosts = Array<Any>()
    
    lazy var refreshControl: UIRefreshControl =
        {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action: #selector(FollowingViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor(hexString: "#0074BB")
            refreshControl.attributedTitle = NSAttributedString(string: "Fetching...")
            
            return refreshControl
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
        
        

        
//        self.reloadEmptyState()

        tableView.separatorStyle = .none
        
        self.tableView.addSubview(self.refreshControl)

        tableView.estimatedRowHeight = 77
        tableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    var emptyStateImageSize: CGSize?
    {
        return CGSize(width: 60, height: 60)
    }

    
    public func reloadEmptyState() {
        self.reloadEmptyStateForTableView(self.tableView)
    }
    
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "notification")
    }
    
    var emptyStateTitle: NSAttributedString {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 22)]
        return NSAttributedString(string: "No Notification", attributes: attrs)
    }
    
    var emptyStateButtonTitle: NSAttributedString? {
        let attrs = [NSAttributedStringKey.foregroundColor: UIColor.white,
                     NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]
        return NSAttributedString(string: "Refresh", attributes: attrs)
    }
    
    var emptyStateButtonSize: CGSize? {
        return CGSize(width: 80, height: 30)
    }
    
    
    // MARK: - Empty State Delegate
    
    func emptyStateViewWillShow(view: UIView) {
        guard let emptyView = view as? UIEmptyStateView else { return }
        // Some custom button stuff
        emptyView.button.layer.cornerRadius = 5
        emptyView.button.layer.borderWidth = 1
        emptyView.button.layer.borderColor = UIColor.black.cgColor
        emptyView.button.layer.backgroundColor = UIColor.black.cgColor
    }
    
    func emptyStatebuttonWasTapped(button: UIButton)
    {
        loadFeeds()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.post(name: Notification.Name("disableSwipeGesture"), object: nil)

        loadFeeds()
        
        
    }
    
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl)
    {
        self.pullToRefresh()
    }
    
    func pullToRefresh()
    {

        feedPosts.removeAll()
        
        self.tableView.reloadData()

        
        print("Data = ",self.feedPosts)

        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!
        ]
        
        print(params)
        let url = BASE_URL + FOLLOWINGNOTIFY

        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                self.refreshControl.endRefreshing()
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                            
                            
                            
                            self.feedPosts = JSON.object(forKey: "followNotify")as! Array<Any>
                            
                            
                            
                            print("Data = ",self.feedPosts.count)
                            
                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()

                            
                        }
                        else
                        {
                            
                            self.feedPosts.removeAll()
                            self.tableView.reloadData()


                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()
                                
                                self.reloadEmptyState()

                            }
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    

                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()

                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    

    
    func loadFeeds()
    {
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        let params: Parameters = [
            "accessToken": accessToken!
        ]
        
        print(params)

        let url = BASE_URL + FOLLOWINGNOTIFY
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                KRProgressHUD.dismiss()
                
                
                if(response.result.isSuccess)
                {
                    
                    
                    if let json = response.result.value
                    {
                        print("Account =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            let JSON = response.result.value as! NSDictionary
                            
                                                        
                            self.feedPosts = JSON.object(forKey: "followNotify")as! Array<Any>
                            
                            self.tableView.reloadData()
                            
                            self.reloadEmptyState()

                            
                        }
                        else
                        {
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                                
                                self.feedPosts.removeAll()
                                
                                self.tableView.reloadData()

                                self.reloadEmptyState()

                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                    
                    self.feedPosts.removeAll()
                    
                    self.tableView.reloadData()
                    
                    self.reloadEmptyState()

                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.feedPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowingTableViewCell", for: indexPath) as! FollowingTableViewCell
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        cell.delegate = self

        
        cell.selectionStyle = .none
        
        
        let notification_type = feedPosts["notification_type"]as? String ?? ""
        
        let time =  feedPosts.value(forKey: "time")as! CLongLong
        let sec = time * 1000
        let data = dateFromMilliseconds(milliSeconds: time * 1000)
        print(data)
        let date = " " + dateDiff(dateStr: String(describing: data))
        

        let notification_1 = feedPosts["notification_1"]as? String ?? ""
        let notification_2 = feedPosts["notification_2"]as? String ?? ""
        let notification_3 = feedPosts["notification_3"]as? String ?? ""

        
        
        var myMutableString = NSMutableAttributedString()
        
        myMutableString = NSMutableAttributedString(string: notification_1, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Medium", size: 15.0)!])
        
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:notification_1.count))
        

        
        var myMutableString2 = NSMutableAttributedString()
        
        myMutableString2 = NSMutableAttributedString(string: notification_2, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Regular", size: 15.0)!])
        
        myMutableString2.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:notification_2.count))

        
        
        
        myMutableString.append(myMutableString2)
        
        
        
        
        
        
        var myMutableString1 = NSMutableAttributedString()
        
        myMutableString1 = NSMutableAttributedString(string: notification_3, attributes: [NSAttributedStringKey.font:UIFont(name: "Poppins-Medium", size: 15.0)!])
        
        myMutableString1.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:notification_3.count))

        myMutableString.append(myMutableString1)

        
        let attributedString1 = NSAttributedString(string: date, attributes: nil)
        
        
        let attributedStringColor1 = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        
        let attributedString2 = NSAttributedString(string: date, attributes: attributedStringColor1)
        

        myMutableString.append(attributedString2)
        
        cell.commentsLabel.attributedText! = myMutableString
        
        
        var feedContentPosts = Array<Any>()

        feedContentPosts  = (feedPosts["data"]as? Array)!

        if feedContentPosts.count > 0
        {
            let feedPosts = feedContentPosts[0]as! NSDictionary

/*
            let imgString = feedPosts["follfromImage"]as? String ?? ""
            
            cell.likeImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-user-image"))

            
            let imgStrings = feedPosts["folltoImage"]as? String ?? ""

            
            cell.proImage.sd_setImage(with: URL(string: imgStrings), placeholderImage: UIImage(named: "default-user-image"))
*/
            
            if notification_type == "followers_like"
            {
                let like = feedPosts["linkData"]as? String ?? ""
                cell.likeImage.sd_setImage(with: URL(string: like), placeholderImage: UIImage(named: "default-thumbnail"))

                let pro = feedPosts["userImage"]as? String ?? ""

                cell.proImage.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-user-image"))

            }
            else if notification_type == "followers_followers"
            {
                
                let pro = feedPosts["folltoImage"]as? String ?? ""

                
                cell.proImage.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-user-image"))

                
                let imgString = feedPosts["follfromImage"]as? String ?? ""

                
                cell.likeImage.sd_setImage(with: URL(string: imgString), placeholderImage: UIImage(named: "default-thumbnail"))
            }
            else if notification_type == "followers_comment"
            {
                let pro = feedPosts["followerImage"]as? String ?? ""
                cell.likeImage.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-thumbnail"))
                
                cell.proImage.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-user-image"))
            }
        }
        
        if feedContentPosts.count == 1 || feedContentPosts.count == 0
        {
            cell.likeImage.isHidden = false
        }
        else
        {
            cell.likeImage.isHidden = true
        }
        
        cell.viewProfileBtn.addTarget(self, action: #selector(FollowingViewController.viewProfileAtn(_:)), for: .touchUpInside)
        cell.viewProfileBtn.tag = indexPath.row


        cell.viewFollowersBtn.addTarget(self, action: #selector(FollowingViewController.viewFollowersBtn(_:)), for: .touchUpInside)
        cell.viewFollowersBtn.tag = indexPath.row

        
        return cell
    }
    

    @objc func viewFollowersBtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary
        
        let notification_type = feedPosts["notification_type"]as? String ?? ""
        
        var feedContentPosts = Array<Any>()
        
        feedContentPosts  = (feedPosts["data"]as? Array)!
        
        if feedContentPosts.count > 0
        {
            let feedPosts = feedContentPosts[0]as! NSDictionary
            
            if notification_type == "followers_like"
            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
                let like = feedPosts["postId"] as! Int
                vc.detailPostId = String(describing: like)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if notification_type == "followers_followers"
            {
                let userName = feedPosts["name"]as? String ?? ""
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
                vc.userName = userName
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if notification_type == "followers_comment"
            {
                let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = StoaryBoard.instantiateViewController(withIdentifier: "CommentViewController")as! CommentViewController
                let pID = (feedPosts["postId"] as! Int)
                let id = String(describing: pID)
                vc.postID = id
                let profilePic = feedPosts["linkData"] as! String
                vc.proPic = profilePic
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    
    @objc func viewProfileAtn(_ sender: AnyObject)
    {
        let feedPosts = self.feedPosts[sender.tag]as! NSDictionary

        
        print("feedPosts = ",feedPosts)
        
        let userName = feedPosts["name"]as? String ?? ""

        print("userName = ",userName)

        let proStatus : String = UserDefaults.standard.object(forKey: username) as! String
        

        print("proStatus = ",proStatus)

        
        if proStatus == userName
        {
            NotificationCenter.default.post(name: Notification.Name("changeTabbarItem"), object: nil)
        }
        else
        {
            let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = StoaryBoard.instantiateViewController(withIdentifier: "UserDetailsViewController")as! UserDetailsViewController
            vc.userName = userName
            self.navigationController?.pushViewController(vc, animated: true)

        }
        
    }
    
    
    
    func dateFromMilliseconds(milliSeconds: CLongLong) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(milliSeconds)/1000)
    }
    
    func dateDiff(dateStr:String) -> String {
        print(dateStr)
        let f:DateFormatter = DateFormatter()
        f.timeZone = TimeZone(abbreviation: "UTC")
        f.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        
        let now = f.string(from: NSDate() as Date)
        let startDate = f.date(from: dateStr)
        let endDate = f.date(from: now)
        print(startDate)
        print(endDate!)
        let calendar: Calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: startDate!, to: endDate!)
        
        
        let weeks = Int(components.month!)
        let days = Int(components.day!)
        let hours = Int(components.hour!)
        let min = Int(components.minute!)
        let sec = Int(components.second!)
        
        var timeAgo = ""
        if sec == 0 {
            timeAgo = "a moment ago"
        }else if (sec > 0){
            if (sec > 1) {
                timeAgo = "\(sec) secs ago"
            } else {
                timeAgo = "\(sec) secs ago"
            }
        }
        
        if (min > 0){
            if (min > 1) {
                timeAgo = "\(min) mins ago"
            } else {
                timeAgo = "\(min) mins ago"
            }
        }
        
        if(hours > 0){
            if (hours > 1) {
                timeAgo = "\(hours) hrs ago"
            } else {
                timeAgo = "\(hours) hrs ago"
            }
        }
        
        if (days > 0) {
            if (days > 1) {
                timeAgo = "\(days) days ago"
            } else {
                timeAgo = "\(days) day ago"
            }
        }
        
        if(weeks > 0){
            if (weeks > 1) {
                timeAgo = "\(weeks) weeks ago"
            } else {
                timeAgo = "\(weeks) week ago"
            }
        }
        
        //    print("timeAgo is===> \(timeAgo)")
        return timeAgo;
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
        let cell = cell as! FollowingTableViewCell
        
        let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
        
        var feedContentPosts = Array<Any>()

        feedContentPosts  = (feedPosts["data"]as? Array)!
        

        cell.loadDelegate(row: indexPath.row)
        
        print("willDisplay feedContentPosts = ",feedContentPosts.count);
        
 
        cell.feedContentPosts  = feedContentPosts

        cell.notification_type = feedPosts["notification_type"]as? String ?? ""

        
        cell.collectionView.delegate = cell
        cell.collectionView.dataSource = cell

        
        cell.collectionView.reloadData()
//        cell.collectionView.collectionViewLayout.invalidateLayout()

    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        
       
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
            let feedPosts = self.feedPosts[indexPath.row]as! NSDictionary
            
            var feedContentPosts = Array<Any>()
            
            feedContentPosts  = (feedPosts["data"]as? Array)!

            if feedContentPosts.count > 1
            {
        
                let x : Float = Float(feedContentPosts.count) / Float(4)
                
                let roundVal = x.rounded(.up)
                print("Round = ",roundVal)
                
                let retVal = (62.5  * roundVal) + 87.5
                
                let size : CGFloat = CGFloat(retVal)
                
                return size

            }
            else
            {
//                return 82.5
                return UITableViewAutomaticDimension
            }

    }
    
}

extension FollowingViewController: FollowingTableViewCellDelegate
{
    func goToPost(linkData :String)
    {
        let StoaryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = StoaryBoard.instantiateViewController(withIdentifier: "DetailViewController")as! DetailViewController
        
        vc.cameFrom = "1"
        
        vc.detailPostId = linkData
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
