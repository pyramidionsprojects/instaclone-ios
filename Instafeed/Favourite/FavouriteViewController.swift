//
//  FavouriteViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 10/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController,CAPSPageMenuDelegate {

    
    @IBOutlet weak var topView: UIView!
    
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        
        let stoaryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let Dvc = stoaryboard.instantiateViewController(withIdentifier: "FollowingViewController")as! FollowingViewController
        Dvc.title = "FOLLOWING"
        controllerArray.append(Dvc)
        
        let pvc = stoaryboard.instantiateViewController(withIdentifier: "YouViewController")as! YouViewController
        pvc.title = "YOU"
        controllerArray.append(pvc)
        
        print(controllerArray)
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        let width = screenSize.width / 2
        
        let parameters: [CAPSPageMenuOption] = [
            //            .scrollMenuBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            //            .viewBackgroundColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1.0)),
            //            .selectionIndicatorColor(UIColor.orange),
            //            .addBottomMenuHairline(false),
            //            .menuItemFont(UIFont(name: "HelveticaNeue", size: 10.0)!),
            //            .menuHeight(50.0),
            ////            .selectionIndicatorHeight(0.0),
            //            .menuItemWidthBasedOnTitleTextWidth(true),
            //            .selectedMenuItemLabelColor(UIColor.orange),
            .menuItemWidth(width),
            .menuItemFont(UIFont(name: "HelveticaNeue", size: 15.0)!),
            .scrollMenuBackgroundColor(UIColor(hexString: "#FAFAFA"))
        ]
        
        
        let setrame =  topView.bounds
        
        
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: setrame, pageMenuOptions: parameters)
        
        
        
        pageMenu?.delegate = self
        
        self.addChildViewController(pageMenu!)
        self.topView.addSubview(pageMenu!.view)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Show the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

/*
extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
*/
