//
//  FollowingTableViewCell.swift
//  Instafeed
//
//  Created by Pyramidions on 08/06/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit

protocol FollowingTableViewCellDelegate
{
    func goToPost(linkData :String)
}

class FollowingTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate
{

    var feedContentPosts = Array<Any>()

    var postArray = Array<Any>()

    
    var delegate: FollowingTableViewCellDelegate?

   
    
    @IBOutlet weak var viewProfileBtn: UIButton!
    
    
    @IBOutlet weak var viewFollowersBtn: UIButton!
    
    
    var notification_type : String = ""
    
    @IBOutlet weak var proImage: UIImageView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        

    }
    
    
    func loadDelegate(row :Int)
    {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.tag = row
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        proImage.layer.cornerRadius = proImage.frame.size.width / 2
        proImage.layer.masksToBounds = true

//        collectionView.delegate = self
//        collectionView.dataSource = self
        
        
//        let flowLayout = UICollectionViewFlowLayout.init()
//        flowLayout.itemSize = CGSize(width: collectionView.frame.size.width / 4 - 10, height: collectionView.frame.size.width / 4 - 10)
//        flowLayout.scrollDirection = .horizontal
//        flowLayout.minimumLineSpacing = 2
//        flowLayout.minimumInteritemSpacing = 0
//        
//        self.collectionView?.collectionViewLayout = flowLayout
        
        
        
        print("collectionView = ",collectionView.frame)
        
        
    }
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.feedContentPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowingCollectionViewCell", for: indexPath) as! FollowingCollectionViewCell
        
        
        print("Collection Count = ",self.feedContentPosts.count)
        
        
        let feedPosts = self.feedContentPosts[indexPath.row]as! NSDictionary
        
        
        print("feedPosts = ",feedPosts);
        
        
        
        if notification_type == "followers_like"
        {
            let pro = feedPosts["linkData"]as? String ?? ""
            cell.likeImages.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-thumbnail"))
        }
        else if notification_type == "followers_followers"
        {
            let pro = feedPosts["folltoImage"]as? String ?? ""
            cell.likeImages.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-thumbnail"))
        }
        else if notification_type == "followers_comment"
        {
            let pro = feedPosts["linkData"]as? String ?? ""
            cell.likeImages.sd_setImage(with: URL(string: pro), placeholderImage: UIImage(named: "default-thumbnail"))
        }

        
        
        
        return cell
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {

        postArray.removeAll()
        
         for index in 0..<self.feedContentPosts.count
         {
            let feedPosts = self.feedContentPosts[index]as! NSDictionary
            
            let pid = feedPosts["postId"] as! Int
            
            let postid = String(describing: pid)
            
            let dict : NSDictionary = ["id":postid]
            
            postArray.append(dict)
            
         }
        
        
        if postArray.count > 0
        {
            let data = try? JSONSerialization.data(withJSONObject: self.postArray, options: [])
            let linkDataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            delegate?.goToPost(linkData: linkDataString! as String)
        }
        

    }
    
    
    
    
}


extension FollowingTableViewCell: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 4 - 10, height: collectionView.frame.size.width / 4 - 10)
    }
}


/*

extension FollowingTableViewCell
{
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.itemSize = CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        
        self.collectionView?.collectionViewLayout = flowLayout
        
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
//        self.collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
    
}

*/
