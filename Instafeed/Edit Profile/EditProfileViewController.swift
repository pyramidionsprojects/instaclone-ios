//
//  EditProfileViewController.swift
//  Instafeed
//
//  Created by Pyramidions on 11/05/18.
//  Copyright © 2018 Pyramidions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import AWSS3
import AWSCore
import KRProgressHUD
import SDWebImage
import JGProgressHUD
import McPicker


class EditProfileViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate
{

    var gender : String = "0"
        
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var websiteTxt: UITextField!
    @IBOutlet weak var bioTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var genderTxt: UITextField!
    
    
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var maleImg: UIImageView!
    @IBOutlet weak var femaleImg: UIImageView!
    
    
    let data: [[String]] = [
        ["Male", "Female"]
    ]

    
    
    var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var ProfileImgURL : String = ""
    var indicator: NVActivityIndicatorView!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
//        self.maleImg.isHidden = true
//        self.femaleImg.isHidden = true
        
        
        self.getDetails()

    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
/*        genderView.layer.shadowColor = UIColor.darkGray.cgColor
        genderView.layer.shadowOpacity = 1
        genderView.layer.shadowOffset = CGSize.zero
        genderView.layer.shadowRadius = 2*/
        
        profileImgView.layer.cornerRadius = self.profileImgView.frame.size.width / 2
        profileImgView.clipsToBounds = true
        
        
    }
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any)
    {
//        updateData()
        
        updateUserName()
    }
    
    
    @IBAction func changeProfile(_ sender: Any)
    {
        
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel)
        {
            action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Choose Gallery", style: .default)
        {
            action -> Void in
            
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            
            
            /*
             The sourceType property wants a value of the enum named        UIImagePickerControllerSourceType, which gives 3 options:
             
             UIImagePickerControllerSourceType.PhotoLibrary
             UIImagePickerControllerSourceType.Camera
             UIImagePickerControllerSourceType.SavedPhotosAlbum
             
             */
            self.present(self.imagePicker, animated: true, completion: nil)
            
            
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Choose Camera", style: .default) { action -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    @IBAction func male(_ sender: Any)
    {
        gender = "male"
        
        self.maleImg.isHidden = false
        self.femaleImg.isHidden = true

    }
    
    @IBAction func female(_ sender: Any)
    {
        gender = "female"
        
        self.maleImg.isHidden = true
        self.femaleImg.isHidden = false

    }
    
    
    func uploadImageAWS()
    {
        
        let hud = JGProgressHUD(style: .dark)
        hud.show(in: self.view)

        
        let chosenImage = self.profileImgView.image
        
        let imgData = UIImageJPEGRepresentation(chosenImage!, 0.5)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddHHmmssSSS"
        var strFileName = formatter.string(from: Date())
        strFileName = strFileName + ".png"
        
        
        //            self.indicator.startAnimating()
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock =
            {
                (task, progress) in DispatchQueue.main.async(execute:
                    {
                        print(progress)
                })
        }
        
        completionHandler =
            { (task, error) -> Void in
                DispatchQueue.main.async(execute:
                    {
                })
        }
        
        let  transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(imgData,
                                   bucket: BUCKET_NAME,
                                   key: strFileName,
                                   contentType: "image/png",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith
            {
                (task) -> AnyObject! in
                if let error = task.error
                {
                    print("Error: \(error.localizedDescription)")
//                    KRProgressHUD.dismiss()
                    
                    hud.dismiss(afterDelay: 3.0)

                    
                }
                
                if let _ = task.result
                {
                    self.ProfileImgURL = ""
                    
                    self.ProfileImgURL = IMAGES_BUCKET + strFileName
                    
                    print("Send Image = ",self.ProfileImgURL)

                    
                    
                    
                    hud.dismiss(afterDelay: 3.0)

//                    KRProgressHUD.dismiss()
                }
                
                return nil
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.profileImgView.image = pickedImage
            
            ProfileImgURL = ""
            
//            KRProgressHUD.show(withMessage: "Loading...")

            
            self.uploadImageAWS()
            
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        
        
        dismiss(animated: true, completion:nil)
    }
    
    
    func updateData()
    {
        //        self.indicator.startAnimating()
        
        //        KRProgressHUD.dismiss()
        
        
//        KRProgressHUD.show(withMessage: "Loading...")

        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "profilePic":self.ProfileImgURL,
            "name":self.nameTxt.text!,
            "website":self.websiteTxt.text!,
            "bio":self.bioTxt.text!,
            "countryCode":"+91",
            "mobileNumber":self.phoneTxt.text!,
            "gender":self.gender
            ]
        
        print(params)
        let url = BASE_URL + EDITPROFILE
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    
                    
                    KRProgressHUD.dismiss()
                    
                    
                    if let json = response.result.value
                    {
                        print("Upload Image =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                        else
                        {
                            
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func getDetails()
    {
        
        
        KRProgressHUD.show(withMessage: "Loading...")

        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!
            ]
        
        print(params)
        let url = BASE_URL + GETEDITPROFILE
        
        let Headers: HTTPHeaders =
            [
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    if let json = response.result.value
                    {

                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            print("Get Profile Details = ",jsonResponse)
                            
                            self.nameTxt.text = jsonResponse["userDetails"][0]["name"].stringValue
                            self.userNameTxt.text = jsonResponse["userDetails"][0]["username"].stringValue
                            self.ProfileImgURL =  jsonResponse["userDetails"][0]["profilePic"].stringValue

                            self.profileImgView.sd_setImage(with: URL(string: self.ProfileImgURL), placeholderImage: UIImage(named: "default-user-image"))

                            self.websiteTxt.text = jsonResponse["userDetails"][0]["website"].stringValue
                            self.bioTxt.text = jsonResponse["userDetails"][0]["bio"].stringValue
                            self.emailTxt.text = jsonResponse["userDetails"][0]["email"].stringValue
                            self.phoneTxt.text = jsonResponse["userDetails"][0]["mobileNumber"].stringValue

                            self.gender = jsonResponse["userDetails"][0]["gender"].stringValue
                            
                            
                            if self.gender == "Male"
                            {
//                                self.maleImg.isHidden = false
//                                self.femaleImg.isHidden = true
                                
                                self.genderTxt.text = "Male"
                                
                                self.gender = "Male"

                                
                            }
                            else if self.gender == "Female"
                            {
//                                self.maleImg.isHidden = true
//                                self.femaleImg.isHidden = false
                                
                                self.genderTxt.text = "Female"
                                
                                self.gender = "Female"

                            }
                            else
                            {
//                                self.maleImg.isHidden = true
//                                self.femaleImg.isHidden = true
                                
                                self.genderTxt.text = "Not Specified"

                                self.gender = "Not Specified"


                            }
                            

//                            @IBOutlet weak var websiteTxt: UITextField!
//                            @IBOutlet weak var bioTxt: UITextField!
//                            @IBOutlet weak var emailTxt: UITextField!
//                            @IBOutlet weak var phoneTxt: UITextField!
//
                            
                            
                            
                            
                        }
                        else
                        {
                            
                            print("jsonResponse = ",jsonResponse)
                            
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    
    
    
    @IBAction func genderBtn(_ sender: Any)
    {
        McPicker.show(data: data, doneHandler: { [weak self] (selections: [Int:String]) in
            if let name = selections[0] {
//                self?.label.text = name
            }
            },
            cancelHandler:
            {
                print("Canceled Default Picker")
        }, selectionChangedHandler: { (selections: [Int:String], componentThatChanged: Int) in
            let newSelection = selections[componentThatChanged] ?? "Failed to get new selection!"
            print("Component \(componentThatChanged) changed value to \(newSelection)")
            
            self.genderTxt.text = newSelection
            
            self.gender = newSelection
            
        })
    }
    
    
    func updateUserName()
    {
        //        self.indicator.startAnimating()
        
        KRProgressHUD.show(withMessage: "Loading...")
        
        
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")
        
        
        let params: Parameters = [
            "accessToken": accessToken!,
            "username":self.userNameTxt.text!,
            ]
        
        print(params)
        let url = BASE_URL + CHECKNAME
        
        let Headers: HTTPHeaders = [
            
            "Content-Type":"application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: Headers)
            .responseJSON
            {
                response in
                
                if(response.result.isSuccess)
                {
                    
                    
                    
//                    KRProgressHUD.dismiss()
                    
                    
                    if let json = response.result.value
                    {
                        print("Change User Name Response =  \(json)")
                        let jsonResponse = JSON(json)
                        
                        if(jsonResponse["error"].stringValue == "false" )
                        {
                            UserDefaults.standard.set(jsonResponse["username"].stringValue, forKey: username)
                            self.updateData()
                        }
                        else
                        {
                                                        
                            KRProgressHUD.dismiss()
                            
                            let alertController = UIAlertController(title: "Oops", message: jsonResponse["message"].stringValue, preferredStyle: .alert)
                            
                            let action1 = UIAlertAction(title: "Ok", style: .default)
                            {
                                (action:UIAlertAction) in
                            }
                            
                            
                            alertController.addAction(action1)
                            
                            self.present(alertController, animated: true, completion: nil)
                            
                            
                        }
                    }
                }
                else
                {
                    //                    self.indicator.stopAnimating()
                    
                    KRProgressHUD.dismiss()
                    
                    
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
        }
    }
    

}
